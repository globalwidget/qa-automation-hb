package scenarios.headerAndFooter;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class HeaderAndFooters extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	pages.HeaderAndFooters headerAndFooters;
	Cart_AllProducts cart_AllProducts;
	MyCart mycart;
	private boolean status = false;

	String password;

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		headerAndFooters = new pages.HeaderAndFooters(obj);
		mycart = new MyCart(obj);
	}

	/*
	 * TestCaseid : General Description : Verify all the header menus are navigating
	 * to the respective pages
	 */
	public void verifyHeader() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			headerAndFooters.clickEmailUs();
			headerAndFooters.verifyNavigation("emailUs");
			headerAndFooters.Verifypresence_CallUs(retrieve("phoneNumber"));
			headerAndFooters.Verify_PrivacyPolicy();
			if (GOR.loggedIn == true) {
				headerAndFooters.clickAccount();
				myaccount.clickOnLogout();
			}
			headerAndFooters.clickAccount();
			login_register.login(retrieve("loginUsername"), retrieve("loginPassword"));
			headerAndFooters.clickAccount();
			myaccount.clickOnLogout();
			headerAndFooters.clickOnMyCart();
			headerAndFooters.verifyNavigation("cart");
			headerAndFooters.verifyNavigation("emptyCart");
			headerAndFooters.goTo_Shop(1, 0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickViewCart();
			mycart.clickProceedToCheckOut();
			headerAndFooters.verifyNavigation("checkout");
			headerAndFooters.goTo_Home();
			headerAndFooters.verifyNavigation("homeBanner");
			headerAndFooters.goTo_Shop(0, 0);
			headerAndFooters.verifyNavigation("shop");
			headerAndFooters.goTo_Shop(1, 0);
			headerAndFooters.verifyNavigation("AllProducts");
			headerAndFooters.goTo_Shop(2, 0);
			headerAndFooters.verifyNavigation("NewProductReleases");
//			GOR.OfferPopUpHandled = false;
//			homePage.closeOfferPopup();
			headerAndFooters.goTo_Shop(3, 0);
			headerAndFooters.verifyNavigation("cbdEdibles");
			headerAndFooters.goTo_Shop(3, 1);
			headerAndFooters.verifyNavigation("Gummies");
//			GOR.OfferPopUpHandled = false;
//			homePage.closeOfferPopup();
			headerAndFooters.goTo_Shop(3, 2);
			headerAndFooters.verifyNavigation("Capsules");
//			headerAndFooters.goTo_Shop(3, 3);
//			headerAndFooters.verifyNavigation("Syrups");
			headerAndFooters.goTo_Shop(3, 3);
			headerAndFooters.verifyNavigation("Lollipops");
//			GOR.OfferPopUpHandled = false;
//			homePage.closeOfferPopup();
			headerAndFooters.goTo_Shop(3, 4);
			headerAndFooters.verifyNavigation("maxChill");
			headerAndFooters.goTo_Shop(4, 0);
			headerAndFooters.verifyNavigation("cbdOil");
//			headerAndFooters.goTo_Shop(5, 0);
//			headerAndFooters.verifyNavigation("cbdVape");
//			headerAndFooters.goTo_Shop(6, 0);
//			headerAndFooters.verifyNavigation("cbdTopicals");
//			headerAndFooters.goTo_Shop(6, 1);
//			headerAndFooters.verifyNavigation("cbdPainFreeze");
//			headerAndFooters.goTo_Shop(6, 2);
//			headerAndFooters.verifyNavigation("cbdLotion");
//			headerAndFooters.goTo_Shop(6, 3);
//			headerAndFooters.verifyNavigation("cbdBubbleBombs");
//			headerAndFooters.goTo_Shop(6, 4);
//			headerAndFooters.verifyNavigation("cbdHeatReliefSpray");
//			headerAndFooters.goTo_Shop(6, 5);
//			headerAndFooters.verifyNavigation("cbdPatches");
//			headerAndFooters.goTo_Shop(6, 6);
//			headerAndFooters.verifyNavigation("cbdEssentialOil");
//			headerAndFooters.goTo_Shop(6, 7);
//			headerAndFooters.verifyNavigation("cbdLipBalm");
//			headerAndFooters.goTo_Shop(6, 8);
//			headerAndFooters.verifyNavigation("cbdBeardProducts");
//			headerAndFooters.goTo_Shop(6, 9);
//			headerAndFooters.verifyNavigation("cbdTattooOinment");
			headerAndFooters.goTo_Shop(7, 0);
			headerAndFooters.verifyNavigation("cbdBundles");
			headerAndFooters.goTo_Shop(8, 0);
			headerAndFooters.verifyNavigation("petCBDproducts");
			headerAndFooters.goTo_Shop(8, 1);
			headerAndFooters.verifyNavigation("petCBDoil");
			headerAndFooters.goTo_Shop(8, 2);
			headerAndFooters.verifyNavigation("petCBDdogtreats");
//			headerAndFooters.goTo_Shop(8, 3);
//			headerAndFooters.verifyNavigation("petCBDshampoo");
//			headerAndFooters.goTo_Shop(8, 4);
//			headerAndFooters.verifyNavigation("petCBDpawButter");
			headerAndFooters.goTo_Shop(9, 0);
			headerAndFooters.verifyNavigation("apparel");
			headerAndFooters.goTo_Shop(10, 0);
			headerAndFooters.verifyNavigation("subscriptions");

			headerAndFooters.goTo_Learn(0);
			headerAndFooters.verifyNavigation("CBDfacts");
//			headerAndFooters.goTo_Learn(1);
//			headerAndFooters.verifyNavigation("productFinderQuiz");
			headerAndFooters.goTo_Learn(2);
			headerAndFooters.verifyNavigation("blog");
			headerAndFooters.goTo_Learn(3);
			headerAndFooters.verifyNavigation("ourStory");
			headerAndFooters.goTo_Learn(6);
			headerAndFooters.verifyNavigation("intheNews");
			headerAndFooters.goTo_Learn(4);
			headerAndFooters.verifyNavigation("CBDfacts");
			headerAndFooters.goTo_Learn(5);
			headerAndFooters.verifyNavigation("benefitsOfCBDoil");
			headerAndFooters.goTo_Learn(7);
			headerAndFooters.verifyNavigation("thirdPartLabTesting");
			headerAndFooters.goTo_Learn(8);
			headerAndFooters.verifyNavigation("faq");
			headerAndFooters.goTo_Learn(9);
			headerAndFooters.verifyNavigation("rewards");

			headerAndFooters.goTo_Wholesale(0);
			headerAndFooters.verifyNavigation("becomeDistributor");
			headerAndFooters.goTo_Wholesale(1);
			headerAndFooters.verifyNavigation("becomeDistributor");
			headerAndFooters.goTo_Wholesale(2);
			headerAndFooters.verifyNavigation("productLiability");
		//	headerAndFooters.goTo_Wholesale(3);
		//	headerAndFooters.verifyNavigation("legalSummary");
			headerAndFooters.goTo_Wholesale(4);
			headerAndFooters.verifyNavigation("distributorTerms");
			headerAndFooters.goTo_Wholesale(5);
			headerAndFooters.verifyNavigation("distributorBlog");

			headerAndFooters.goTo_Contact();
			headerAndFooters.verifyNavigation("contact");
			headerAndFooters.goTo_delta8();
			headerAndFooters.verifyNavigation("delta8");
//			headerAndFooters.goTo_Rewards();
//			headerAndFooters.verifyNavigation("rewards");

		} catch (Exception e) {
			testStepFailed("Error message for Login using Invalid Credentials could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General Description : Verify all the footer menus are navigating
	 * to the respective pages
	 */
	public void verifyFooter() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			headerAndFooters.navigateFooter("allCBDProduct");
			headerAndFooters.verifyNavigation("AllProducts");
			headerAndFooters.navigateFooter("cbdEdibles");
			headerAndFooters.verifyNavigation("cbdEdibles");
//			headerAndFooters.navigateFooter("cbdVape");
//			headerAndFooters.verifyNavigation("cbdVape");
			headerAndFooters.navigateFooter("cbdOils");
			headerAndFooters.verifyNavigation("cbdOils");
			headerAndFooters.navigateFooter("cbdTopicals");
			headerAndFooters.verifyNavigation("cbdTopicals");
			headerAndFooters.navigateFooter("cbdPetProducts");
			headerAndFooters.verifyNavigation("petCBDoil");
			headerAndFooters.navigateFooter("hempBombsApparel");
			headerAndFooters.verifyNavigation("apparel");

			headerAndFooters.navigateFooter("facebook");
			headerAndFooters.verifyNavigation("facebook");
			headerAndFooters.navigateFooter("instagram");
			headerAndFooters.verifyNavigation("instagram");
			headerAndFooters.navigateFooter("youtube");
			headerAndFooters.verifyNavigation("youtube");
			headerAndFooters.navigateFooter("twitter");
			headerAndFooters.verifyNavigation("twitter");

			headerAndFooters.navigateFooter("homeFooter");
			headerAndFooters.verifyNavigation("homeBanner");
			headerAndFooters.navigateFooter("distributors_storeOwners");
			headerAndFooters.verifyNavigation("distributors_storeOwners");
			headerAndFooters.navigateFooter("affiliates");
			headerAndFooters.verifyNavigation("affiliate");
			headerAndFooters.navigateFooter("seniorDiscount");
			headerAndFooters.verifyNavigation("seniorDiscount");
			headerAndFooters.navigateFooter("veteran_activeDuty");
			headerAndFooters.verifyNavigation("veteran_activeDuty");
			headerAndFooters.navigateFooter("firstResponders");
			headerAndFooters.verifyNavigation("firstResponders");
			headerAndFooters.navigateFooter("contactFooter");
			headerAndFooters.verifyNavigation("contact");
			headerAndFooters.navigateFooter("siteMap");
			headerAndFooters.verifyNavigation("siteMap");
			headerAndFooters.navigateFooter("ccpa");
			headerAndFooters.verifyNavigation("ccpa");

		} catch (Exception e) {
			testStepFailed("Error message for Login using Invalid Credentials could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
