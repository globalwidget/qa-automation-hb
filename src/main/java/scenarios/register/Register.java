package scenarios.register;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;

public class Register extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	private boolean status = false;

	String password;

	public Register(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
	}

	/*
	 * TestCaseid : Register Description : To validate register with invalid
	 * credentials and existing credentials
	 */
	public void register_invalidCredentials() {
		try {
			password = retrieve("securityPassword");
			String invalidUsername = retrieve("invalidUsername");
			String invalidPassword = retrieve("invalidPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			homePage.goToLogin_Register();
			login_register.register(invalidUsername, invalidPassword);
			login_register.verifyInvalidPasswordRestriction_Resgister();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	public void register_existingCredentials() {

		try {
			password = retrieve("securityPassword");
			String existingUser_Username = retrieve("loginUsername");
			String existingUser_Password = retrieve("loginPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			login_register.register(existingUser_Username, existingUser_Password);
			login_register.verifyErrorMessage_Register();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || login_register.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;

	}

	/*
	 * TestCaseid : Register Description : To validate register with valid
	 * credentials
	 */

	public void register() {
		try {
			myaccount = new MyAccount(obj);
			password = retrieve("securityPassword");
			String registerUsername = retrieve("registerUsername");
			String registerPassword = retrieve("registerPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			homePage.goToLogin_Register();
			login_register.register(registerUsername, registerPassword);
			myaccount.verifyPresenceOfLogout();
			myaccount.clickOnLogout();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;

	}

}
