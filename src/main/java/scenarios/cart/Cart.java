package scenarios.cart;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.Checkout;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;
import pages.Rewards;

public class Cart extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	Cart_AllProducts cart_AllProducts;
	MyCart myCart;
	HeaderAndFooters headerAndfooter;
	Rewards rewards;
	Checkout checkout;
	private boolean status = false;
	HashMap<String, String> billingdata;
	HashMap<String, String> shippingData;

	String password;

	public Cart(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		myCart = new MyCart(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		rewards = new Rewards(obj);
		checkout = new Checkout(obj);
	}

	/*
	 * TestCaseid : Cart Description : To validate adding a product to the cart.
	 */
	public void addProductToCart(int numberOfproducts) {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String productName;
			String secondProductName = null;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();

			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.selectFirstProduct();
			productName = cart_AllProducts.getProductTitle();
			cart_AllProducts.clickAddToCart();
			if (numberOfproducts == 2) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectSecondProduct();
				secondProductName = cart_AllProducts.getProductTitle();
				cart_AllProducts.clickAddToCart();

			}
			cart_AllProducts.clickViewCart();
			myCart.verifyPresenceOfExpectedProduct(productName);
			if (numberOfproducts == 2) {
				myCart.verifyPresenceOfExpectedProduct(secondProductName);

			}

		} catch (Exception e) {
			testStepFailed("Product could not be added to the cart");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : To validate removing a product to the cart.
	 */
	public void verifyRemoveProductFromCart() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String productRemoved;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			productRemoved = myCart.clickOnRemove();
			myCart.verifyExpectedMessageAlert(productRemoved, "remove", "", 1);

		} catch (Exception e) {
			testStepFailed("Removal of a product from the cart could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to update the
	 * quantity of the products in the cart page
	 */
	public void verifyUpdateQuantityInCart() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");

			headerAndfooter.goTo_Home();
			waitTime(3);
			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCart.updateCartQuantity();
			myCart.clickUpdateCart();
			waitTime(5);
			myCart.verifyExpectedMessageAlert("Cart updated", "", "", 1);
		} catch (Exception e) {
			testStepFailed("Updating the quantity of the product from the cart could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to apply the valid
	 * coupon code in the cart page
	 */
	public void verifyValidCouponEntry() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String validCouponCode = retrieve("validCouponCode");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCart.enterCouponCode(validCouponCode);
			myCart.clickApplyCoupon();
			myCart.verifyExpectedMessageAlert("Coupon Code", "has been applied to your order", "", 1);
			myCart.removeCoupon();
		} catch (Exception e) {
			testStepFailed("Valid Coupon entry could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to apply the valid
	 * coupon code in the cart page
	 */
	public void verifyInvalidValidCouponEntry() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String invalidCouponCode = retrieve("invalidCouponCode");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCart.enterCouponCode(invalidCouponCode);
			myCart.clickApplyCoupon();
			myCart.verifyExpectedMessageAlert_InvalidCoupon("Coupon ", "does not exist!", invalidCouponCode);
		} catch (Exception e) {
			testStepFailed("Invalid Coupon entry could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_079 Description : To Verify if the user is able to
	 * login in cart page to see the redemptions and the available points.
	 */
	public void loginFromMyCart() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				myaccount.clickOnLogout();
			} else if (GOR.loggedIn == true) {
				headerAndfooter.goToMyAccount();
				myaccount.clickOnLogout();
				GOR.loggedIn = false;
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCart.login(userId, userPassword);
			myCart.checkPointsRedemptionSection();
			myCart.fetchPoints();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myaccount.testFailure || myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_063 Description : To Verify if the user is able to
	 * see the current points above the rewards choice drop down.
	 */
	public void checkCurrentPointsDisplay() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");
			int rewardsPoints;
			int myCartPoints;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewardsPoints = rewards.fetchPoints();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCartPoints = myCart.fetchPoints();
			myCart.comparePoints(rewardsPoints, myCartPoints);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_055 Description : To Verify if the user is able to
	 * see the redemptions drop down options in the cart page.
	 */
	public void checkRedemtptionsDropdown() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCart.verifypointsredemptionDropdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_070 Description : To Verify if the user is not able
	 * to apply both coupon code and reward redemptions at the same time in cart for
	 * the same order.
	 */
	public void verifyCouponRewardFeature() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCart.enterCouponCode("web15");
			myCart.clickApplyCoupon();
			myCart.verifyExpectedMessageAlert("prefer to redeem rewards points", "please remove this coupon",
					"choose your reward", 1);
			myCart.checkPointsRedemptionSectionAbsence();
			myCart.removeCoupon();
			myCart.checkPointsRedemptionSection();
			myCart.chooseReward(0);
			myCart.clickRedeem();
			myCart.verifyExpectedMessageAlert("rewards points discount has been applied to your order",
					"If you would prefer to use a coupon code, please remove the reward points",
					"Your rewards points discount code will be saved in your rewards history for future use", 1);
			GOR.rewardsApplied_150Points = true;
			myCart.checkCouponSectionIsDisabled();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_056 Description : To Verify selecting the reward 150
	 * points from the drop down is applying the $15 discount.
	 */
	public void verify150pointsRewardAmount() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			if (GOR.rewardsApplied_150Points == false) {
				myCart.chooseReward(0);
				myCart.clickRedeem();
			}
			myCart.rewardsAmountApplied("15.00", "150");

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_061 Description : To Verify the sub total is getting
	 * updated respectively based on the rewards discount applied.
	 */
	public void verifySubtotalAfterPointsRedemption() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			if (GOR.rewardsApplied_150Points == false) {
				myCart.chooseReward(0);
				myCart.clickRedeem();
			}
			myCart.checkTotalAmount();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_065 Description : To Verify if the user is able to
	 * remove the applied redemptions.
	 */
	public void verifyRemoveRewardsApplied() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			if (GOR.rewardsApplied_150Points == false) {
				myCart.chooseReward(0);
				myCart.clickRedeem();
			}
			myCart.clickRemove_Rewards();
			myCart.verifyExpectedMessageAlert("Coupon", "removed", "", 1);

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_SR_062 Description : To Verify rewards points drop down
	 * is unselectable if the user is not having enough points to redeem.
	 */
	public void verifyUnselectableDropdownForLackOfPoints() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			if (GOR.rewardsApplied_150Points == false) {
				myCart.chooseReward(0);
				myCart.clickRedeem();
				myCart.clickRemove_Rewards();
			}
			myCart.verifyNotEnoughPoints();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || login_register.testFailure
				|| myCart.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_PS_012 Description : To Verify if the user is able to
	 * choose the different subscription plan in the cart page for any specific
	 * products.
	 */
	public void verifyDifferentSubscriptionSelection() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == true) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
			}
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("3 months");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.selectSubscription("month");
			myCart.clickUpdateCart();
			myCart.verifyExpectedMessageAlert("Cart updated", "", "", 1);
			myCart.verifySubscriptionSelectedIsPerMonth();

			testStepInfo("A different subscription was selected successfully in the cart page");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || myCart.testFailure
				|| cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart_HB_PS_013 Description : To Verify the percentage offer is
	 * getting applied to the products while choosing the respective subscription.
	 */
	public void verifyPercentageOffer() {
		try {
			String amount;
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			if (GOR.productAdded == true) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
			}
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();

			amount = myCart.selectSubscription("month");
			myCart.verifySubtotal(amount);

			amount = myCart.selectSubscription("2Months");
			myCart.clickUpdateCart();
			myCart.verifyExpectedMessageAlert("Cart updated", "", "", 1);
			myCart.verifySubtotal(amount);

			amount = myCart.selectSubscription("3Months");
			myCart.clickUpdateCart();
			myCart.verifyExpectedMessageAlert("Cart updated", "", "", 1);
			myCart.verifySubtotal(amount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || myCart.testFailure
				|| cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_020 Description : To Verify the sales tax are getting
	 * calculated for the subscription order.
	 */
	public void verifySalesTaxForSubscriptionOrder() {
		try {
			password = retrieve("securityPassword");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes", retrieve("orderNotes"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == true) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
			}
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(true);
			checkout.fillDetails(billingdata, shippingData);
			checkout.verifySalesTax_SubscriptionProduct();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || myCart.testFailure
				|| cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_021 Description : To Verify all the datas under recurring
	 * totals are calculated and displayed properly.
	 */
	public void verifyRecurringTotalsSection() {
		try {
			password = retrieve("securityPassword");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes", retrieve("orderNotes"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.productAdded == true) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
			}
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(true);
			checkout.fillDetails(billingdata, shippingData);
			checkout.verifyRecurringTotals();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndfooter.testFailure || myCart.testFailure
				|| cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
