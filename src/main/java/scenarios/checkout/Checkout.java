package scenarios.checkout;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class Checkout extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	Cart_AllProducts cart_AllProducts;
	MyCart myCart;
	pages.Checkout checkout;
	HeaderAndFooters headerAndfooter;

	private boolean status = false;
	HashMap<String, String> billingdata;
	HashMap<String, String> shippingData;
	String Username;
	String Password;
	String cardNumber_data;
	String expirationMonth_data;
	String expirationYear_data;
	String cardSecurityCode_data;
	String detailsSaved;
	String shipDifferentAddress;
	String password;

	public Checkout(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		myCart = new MyCart(obj);
		checkout = new pages.Checkout(obj);
		headerAndfooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is not able to
	 * place the order with any invalid details in the checkout page.
	 */
	public void checkout_InvalidDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");
			detailsSaved = retrieve("detailsSaved");
			String invalidEmailAddress = retrieve("invalidEmailAddress");
			String invalidBillingPostCode = retrieve("invalidBillingPostCode");
			String invalidShippingPostCode = retrieve("invalidShippingPostCode");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();
			GOR.shipDifferentAddress = true;

			if (detailsSaved.equalsIgnoreCase("no")) {
				billingdata.put("firstName_data", retrieve("firstName_Billing"));
				billingdata.put("lastName_data", retrieve("lastName_Billing"));
				billingdata.put("companyName_data", retrieve("companyName_Billing"));
				billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingdata.put("billingCountry_data", retrieve("country_Billing"));
				billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingdata.put("city_data", retrieve("city_Billing"));
				billingdata.put("state_data", retrieve("state_Billing"));

				shippingData.put("firstName_data", retrieve("firstName_shipping"));
				shippingData.put("lastName_data", retrieve("lastName_shipping"));
				shippingData.put("companyName_data", retrieve("companyName_shipping"));
				shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
				shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
				shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
				shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
				shippingData.put("city_data", retrieve("city_shipping"));
				shippingData.put("state_data", retrieve("state_shipping"));

			}
			billingdata.put("postCode_data", invalidBillingPostCode);
			shippingData.put("postCode_data", invalidShippingPostCode);
			billingdata.put("email_data", invalidEmailAddress);

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			if (GOR.loggedIn == true) {
				headerAndfooter.goToMyAccount();
				myaccount.clickOnLogout();
			}
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.ClearDetails();
			checkout.clickPlaceOrder();
			checkout.VerifyEmptyCardDetailsErrors();
			checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
					cardSecurityCode_data);
			checkout.clickPlaceOrder();
			checkout.VerifyEmptyFieldErrors();
			checkout.Login(Username, Password);
			checkout.fillDetails(billingdata, shippingData);
			if (GOR.completeCheckoutWithSavedCard == false) {
				checkout.clickOnUseNewCardIfPresent();
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
			}
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifyRelevantMessage("Invalid Email");
			billingdata.replace("email_data", retrieve("email"));
			checkout.fillDetails(billingdata, shippingData);
			checkout.clickPlaceOrder();
			checkout.verifyRelevantMessage("Invalid Billing Post Code");
			checkout.verifyRelevantMessage("Invalid Shipping Post Code");

		} catch (Exception e) {
			testStepFailed("Placing Order with Invalid Details could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is able to place
	 * the order with any invalid card details in the checkout page.
	 */
	public void checkout_InvalidCreditCardDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");
			detailsSaved = retrieve("detailsSaved");
			String cardNumber_Wronglength = retrieve("cardNumberWrongLength");
			String expirationYear_invalidData = retrieve("expirationYearInvalidData");
			String cardSecurityCode_invalidData = retrieve("cardSecurityCodeInvalidData");

			billingdata = new HashMap<String, String>();
			GOR.shipDifferentAddress = false;
			if (detailsSaved.equalsIgnoreCase("yes") == false) {
				billingdata.put("firstName_data", retrieve("firstName_Billing"));
				billingdata.put("lastName_data", retrieve("lastName_Billing"));
				billingdata.put("companyName_data", retrieve("companyName_Billing"));
				billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingdata.put("billingCountry_data", retrieve("country_Billing"));
				billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingdata.put("postCode_data", retrieve("postCode_Billing"));
				billingdata.put("city_data", retrieve("city_Billing"));
				billingdata.put("state_data", retrieve("state_Billing"));
				billingdata.put("email_data", retrieve("emailAddress"));

			}

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMyCart();
				myCart.clickProceedToCheckOut();
			}
			checkout.clickShipDifferentAddress(false);
			checkout.fillDetails(billingdata, shippingData);
			if (GOR.completeCheckoutWithSavedCard == true) {
				checkout.clickOnUseNewCardIfPresent();
			}
			checkout.fillDetailsforCreditCard(cardNumber_Wronglength, expirationMonth_data, expirationYear_invalidData,
					cardSecurityCode_invalidData);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifyRelevantMessage("Wrong Length Card Number");
			checkout.verifyRelevantMessage("Invalid Card Number");
			checkout.verifyRelevantMessage("Invalid Security Code");
			checkout.verifyRelevantMessage("Invalid Expiration Data");
			checkout.verifyRelevantMessage("Invalid Card Number");

		} catch (Exception e) {
			testStepFailed("Placing Order with invalid card details could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is able to place
	 * the order with valid details in the checkout page.
	 */
	public void checkout_ValidDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes", retrieve("orderNotes"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMyCart();
				myCart.clickProceedToCheckOut();
			}
			checkout.clickShipDifferentAddress(true);
			checkout.fillDetails(billingdata, shippingData);
			if (GOR.completeCheckoutWithSavedCard == false)
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();

		} catch (Exception e) {
			testStepFailed("Placing Order with Valid Details could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the details saved in the
	 * account is displayed in the checkout page
	 */
	public void checkout_VerifySavedDetails() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			HashMap<String, String> billingData_Saved = new HashMap<String, String>();
			HashMap<String, String> shippingData_Saved = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			} else {
				headerAndfooter.goToMyAccount();
				myaccount.clickOnLogout();
				login_register.login(Username, Password);
			}
			headerAndfooter.goToMyAccount();
			myaccount.clickOnAddresses();
			billingData_Saved = myaccount.getSavedBilllingDetails();
			myaccount.clickOnAddresses();
			shippingData_Saved = myaccount.getSavedShippingDetails();
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
			} else {
				headerAndfooter.clickOnMyCart();
				myCart.clickProceedToCheckOut();
			}
			checkout.clickShipDifferentAddress(true);
			checkout.checkSavedDetails(billingData_Saved, shippingData_Saved);
		} catch (Exception e) {
			testStepFailed(
					"Details saved in the account for checkout are reflected in the checkout page could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_020 Description : To Verify the sales tax are getting
	 * calculated for subscription order.
	 */
	public void checkout_verifySalesTax_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes", retrieve("orderNotes"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(true);
			GOR.shipDifferentAddress = true;
			checkout.fillDetails(billingdata, shippingData);
		} catch (Exception e) {
			testStepFailed("Filling billing and shipping details for a subscription product could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_022 Description : To Verify if the user is able to fill
	 * the billing and shipping address.
	 */
	public void checkout_fillDetails_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes", retrieve("orderNotes"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(true);
			checkout.fillDetails(billingdata, shippingData);
		} catch (Exception e) {
			testStepFailed("Filling billing and shipping details for a subscription product could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_031 Description : To Verify if the user is not able to
	 * place the order with invalid CC details.
	 */
	public void checkout_InvalidCreditCardDetails_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");
			detailsSaved = retrieve("detailsSaved");
			String cardNumber_Wronglength = retrieve("cardNumberWrongLength");
			String expirationYear_invalidData = retrieve("expirationYearInvalidData");
			String cardSecurityCode_invalidData = retrieve("cardSecurityCodeInvalidData");

			billingdata = new HashMap<String, String>();
			GOR.shipDifferentAddress = false;
			if (detailsSaved.equalsIgnoreCase("yes") == false) {
				billingdata.put("firstName_data", retrieve("firstName_Billing"));
				billingdata.put("lastName_data", retrieve("lastName_Billing"));
				billingdata.put("companyName_data", retrieve("companyName_Billing"));
				billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingdata.put("billingCountry_data", retrieve("country_Billing"));
				billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingdata.put("postCode_data", retrieve("postCode_Billing"));
				billingdata.put("city_data", retrieve("city_Billing"));
				billingdata.put("state_data", retrieve("state_Billing"));
				billingdata.put("email_data", retrieve("emailAddress"));

			}

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(false);
			checkout.fillDetails(billingdata, shippingData);
			if (GOR.completeCheckoutWithSavedCard == true)
				checkout.clickOnUseNewCardIfPresent();
			checkout.fillDetailsforCreditCard(cardNumber_Wronglength, expirationMonth_data, expirationYear_invalidData,
					cardSecurityCode_invalidData);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifyRelevantMessage("Wrong Length Card Number");
			checkout.verifyRelevantMessage("Invalid Card Number");
			checkout.verifyRelevantMessage("Invalid Security Code");
			checkout.verifyRelevantMessage("Invalid Expiration Data");
			checkout.verifyRelevantMessage("Invalid Card Number");

		} catch (Exception e) {
			testStepFailed("Placing Order with invalid card details for a subscription product could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_030 Description : To Verify application is forcing to
	 * create account in checkout page for guest users when they try to place their
	 * order with subscription.
	 */
	public void checkout_VerifyCreateAccountPassword_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");

			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes", retrieve("orderNotes"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == true) {
				headerAndfooter.goToMyAccount();
				myaccount.clickOnLogout();
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(true);
			GOR.shipDifferentAddress = true;
			checkout.fillDetails(billingdata, shippingData);
			if (GOR.completeCheckoutWithSavedCard == false)
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifyCreateAccountPasswordAndClearField();
			checkout.verifyRelevantMessage("Create Account Password");

		} catch (Exception e) {
			testStepFailed(
					"Forcing to create account in checkout page for guest users when they try to place their order with subscription could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is able to ship the
	 * product to different place other than the billing address for a subscription
	 * product.
	 */
	public void checkout_ShipDifferentAddress_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = true;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			shippingData.put("firstName_data", retrieve("firstName_shipping"));
			shippingData.put("lastName_data", retrieve("lastName_shipping"));
			shippingData.put("companyName_data", retrieve("companyName_shipping"));
			shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
			shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
			shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
			shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
			shippingData.put("postCode_data", retrieve("postCode_shipping"));
			shippingData.put("city_data", retrieve("city_shipping"));
			shippingData.put("state_data", retrieve("state_shipping"));
			shippingData.put("orderNotes", retrieve("orderNotes"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(true);
			checkout.fillDetails(billingdata, shippingData);
			if (GOR.completeCheckoutWithSavedCard == false)
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();
			GOR.orderNumberMainOrder = checkout.verifyRelevantMessage("Subscription Product Purchase Success");
			testStepInfo(
					"Placing Order to ship the product to different place other than the billing address for a subscription product was verified");
			GOR.orderNumberMainOrder = checkout.verifySuccessMessage();
			GOR.orderNumberSubscriptionOrder = checkout.verifyRelevantMessage("Subscription Product Purchase Success");
			GOR.subscriptionOrderPlaced = true;
		} catch (Exception e) {
			testStepFailed(
					"Placing Order to ship the product to different place other than the billing address for a subscription product could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Checkout Description : To Verify if the user is able to place
	 * the order with valid CC details for a subscription product.
	 */
	public void checkout_ValidccDetails_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			billingdata = new HashMap<String, String>();
			shippingData = new HashMap<String, String>();

			GOR.shipDifferentAddress = false;

			billingdata.put("firstName_data", retrieve("firstName_Billing"));
			billingdata.put("lastName_data", retrieve("lastName_Billing"));
			billingdata.put("companyName_data", retrieve("companyName_Billing"));
			billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
			billingdata.put("billingCountry_data", retrieve("country_Billing"));
			billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
			billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
			billingdata.put("postCode_data", retrieve("postCode_Billing"));
			billingdata.put("city_data", retrieve("city_Billing"));
			billingdata.put("state_data", retrieve("state_Billing"));
			billingdata.put("email_data", retrieve("emailAddress"));

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();
			headerAndfooter.goTo_Shop(0, 1);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			myCart.clickProceedToCheckOut();
			checkout.clickShipDifferentAddress(false);
			checkout.fillDetails(billingdata, shippingData);
			if (GOR.completeCheckoutWithSavedCard == false)
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			checkout.verifySuccessMessage();
			checkout.verifyRelevantMessage("Subscription Product Purchase Success");
			testStepInfo("Placing Order with valid CC details for a subscription product was verified");

		} catch (Exception e) {
			testStepFailed("Placing Order with valid CC details for a subscription product could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
