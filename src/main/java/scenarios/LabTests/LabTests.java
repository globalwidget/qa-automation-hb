package scenarios.LabTests;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HomePage;

public class LabTests extends ApplicationKeywords {
	BaseClass obj;
	pages.LabTests labTests;
	HomePage homePage;
	private boolean status = false;

	String password;

	public LabTests(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		labTests = new pages.LabTests(obj);
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify proper error
	 * message is displayed for the invalid batch search
	 */
	public void verifyInvalidSearch() {
		try {
			password = retrieve("securityPassword");
			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			labTests.goTo_LabTests();
			labTests.clickSearchIcon();
			labTests.enterBatchNumber("  ");
			labTests.clickSearchIcon();
			labTests.verifyNullResult();
		} catch (Exception e) {
			testStepFailed("Could not verify lab test results using the invalid batch number");
		}
		if (obj.testFailure || homePage.testFailure || labTests.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify if the user is able
	 * to search for lab test results using the valid batch number
	 */
	public void verifyValidSearch() {
		try {
			password = retrieve("securityPassword");
			String batchNumber = retrieve("batchNumber");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			labTests.goTo_LabTests();
			labTests.enterBatchNumber(batchNumber);
			labTests.clickSearchIcon();
			labTests.checkViewResults();
		} catch (Exception e) {
			testStepFailed("Could not verify lab test results using the valid batch number");
		}
		if (obj.testFailure || homePage.testFailure || labTests.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}