package scenarios.myAccounts;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class MyAccounts extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	Cart_AllProducts cart_AllProducts;
	MyCart myCart;
	pages.Checkout checkout;
	HeaderAndFooters headerAndfooter;

	private boolean status = false;
	HashMap<String, String> billingdata;
	HashMap<String, String> shippingData;
	String Username;
	String Password;
	String cardNumber_data;
	String expirationMonth_data;
	String expirationYear_data;
	String cardSecurityCode_data;
	String detailsSaved;
	String shipDifferentAddress;
	String password;

	public MyAccounts(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		myCart = new MyCart(obj);
		checkout = new pages.Checkout(obj);
		headerAndfooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : HB_PS_039 Description : To Verify the subscription order is
	 * getting displayed in the Subscriptions section in My Accounts page.
	 */
	public void myAccounts_verifyOrderInSubscriptions_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			if (GOR.subscriptionOrderPlaced == false) {
				billingdata = new HashMap<String, String>();
				shippingData = new HashMap<String, String>();

				GOR.shipDifferentAddress = true;

				billingdata.put("firstName_data", retrieve("firstName_Billing"));
				billingdata.put("lastName_data", retrieve("lastName_Billing"));
				billingdata.put("companyName_data", retrieve("companyName_Billing"));
				billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingdata.put("billingCountry_data", retrieve("country_Billing"));
				billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingdata.put("postCode_data", retrieve("postCode_Billing"));
				billingdata.put("city_data", retrieve("city_Billing"));
				billingdata.put("state_data", retrieve("state_Billing"));
				billingdata.put("email_data", retrieve("emailAddress"));

				shippingData.put("firstName_data", retrieve("firstName_shipping"));
				shippingData.put("lastName_data", retrieve("lastName_shipping"));
				shippingData.put("companyName_data", retrieve("companyName_shipping"));
				shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
				shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
				shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
				shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
				shippingData.put("postCode_data", retrieve("postCode_shipping"));
				shippingData.put("city_data", retrieve("city_shipping"));
				shippingData.put("state_data", retrieve("state_shipping"));
				shippingData.put("orderNotes", retrieve("orderNotes"));
			}

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.subscriptionOrderPlaced == false) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.clickProductWithSubscribeLabel();
				cart_AllProducts.clickSubscribeRadioButton();
				cart_AllProducts.selectOption_SubscriptionDropdown("month");
				cart_AllProducts.clickSignUpNow();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
				checkout.clickShipDifferentAddress(true);
				checkout.fillDetails(billingdata, shippingData);
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
				checkout.clickTermsAndConditions();
				checkout.clickPlaceOrder();
				GOR.orderNumberMainOrder = checkout.verifySuccessMessage();
				GOR.orderNumberSubscriptionOrder = checkout
						.verifyRelevantMessage("Subscription Product Purchase Success");
				GOR.subscriptionOrderPlaced = true;
			}
			headerAndfooter.goToMyAccount();
			myaccount.clickSubscriptions();
			myaccount.accessOrder("order presence confirmation", GOR.orderNumberSubscriptionOrder, null);
		} catch (Exception e) {
			testStepFailed(
					"Presence of the subscription order placed in Subscriptions under My Account could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_035 Description : To Verify the main order is getting
	 * displayed in the Orders section in My Accounts page.
	 */
	public void myAccounts_verifyMainOrderInOrders_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			if (GOR.subscriptionOrderPlaced == false) {
				billingdata = new HashMap<String, String>();
				shippingData = new HashMap<String, String>();

				GOR.shipDifferentAddress = true;

				billingdata.put("firstName_data", retrieve("firstName_Billing"));
				billingdata.put("lastName_data", retrieve("lastName_Billing"));
				billingdata.put("companyName_data", retrieve("companyName_Billing"));
				billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingdata.put("billingCountry_data", retrieve("country_Billing"));
				billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingdata.put("postCode_data", retrieve("postCode_Billing"));
				billingdata.put("city_data", retrieve("city_Billing"));
				billingdata.put("state_data", retrieve("state_Billing"));
				billingdata.put("email_data", retrieve("emailAddress"));

				shippingData.put("firstName_data", retrieve("firstName_shipping"));
				shippingData.put("lastName_data", retrieve("lastName_shipping"));
				shippingData.put("companyName_data", retrieve("companyName_shipping"));
				shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
				shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
				shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
				shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
				shippingData.put("postCode_data", retrieve("postCode_shipping"));
				shippingData.put("city_data", retrieve("city_shipping"));
				shippingData.put("state_data", retrieve("state_shipping"));
				shippingData.put("orderNotes", retrieve("orderNotes"));
			}

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.subscriptionOrderPlaced == false) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.clickProductWithSubscribeLabel();
				cart_AllProducts.clickSubscribeRadioButton();
				cart_AllProducts.selectOption_SubscriptionDropdown("month");
				cart_AllProducts.clickSignUpNow();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
				checkout.clickShipDifferentAddress(true);
				checkout.fillDetails(billingdata, shippingData);
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
				checkout.clickTermsAndConditions();
				checkout.clickPlaceOrder();
				GOR.orderNumberMainOrder = checkout.verifySuccessMessage();
				GOR.orderNumberSubscriptionOrder = checkout
						.verifyRelevantMessage("Subscription Product Purchase Success");
				GOR.subscriptionOrderPlaced = true;
			}
			headerAndfooter.goToMyAccount();
			myaccount.clickOrders();
			myaccount.checkMainOrderinOrders(GOR.orderNumberMainOrder);
		} catch (Exception e) {
			testStepFailed("Presence of the main order placed under Orders could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_043 Description : To Verify if the user is able to cancel
	 * the subscription order.
	 */
	public void myAccounts_Cancel_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			if (GOR.subscriptionOrderPlaced == false) {
				billingdata = new HashMap<String, String>();
				shippingData = new HashMap<String, String>();

				GOR.shipDifferentAddress = true;

				billingdata.put("firstName_data", retrieve("firstName_Billing"));
				billingdata.put("lastName_data", retrieve("lastName_Billing"));
				billingdata.put("companyName_data", retrieve("companyName_Billing"));
				billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingdata.put("billingCountry_data", retrieve("country_Billing"));
				billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingdata.put("postCode_data", retrieve("postCode_Billing"));
				billingdata.put("city_data", retrieve("city_Billing"));
				billingdata.put("state_data", retrieve("state_Billing"));
				billingdata.put("email_data", retrieve("emailAddress"));

				shippingData.put("firstName_data", retrieve("firstName_shipping"));
				shippingData.put("lastName_data", retrieve("lastName_shipping"));
				shippingData.put("companyName_data", retrieve("companyName_shipping"));
				shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
				shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
				shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
				shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
				shippingData.put("postCode_data", retrieve("postCode_shipping"));
				shippingData.put("city_data", retrieve("city_shipping"));
				shippingData.put("state_data", retrieve("state_shipping"));
				shippingData.put("orderNotes", retrieve("orderNotes"));
			}

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.subscriptionOrderPlaced == false) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.clickProductWithSubscribeLabel();
				cart_AllProducts.clickSubscribeRadioButton();
				cart_AllProducts.selectOption_SubscriptionDropdown("month");
				cart_AllProducts.clickSignUpNow();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
				checkout.clickShipDifferentAddress(true);
				checkout.fillDetails(billingdata, shippingData);
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
				checkout.clickTermsAndConditions();
				checkout.clickPlaceOrder();
				GOR.orderNumberMainOrder = checkout.verifySuccessMessage();
				GOR.orderNumberSubscriptionOrder = checkout
						.verifyRelevantMessage("Subscription Product Purchase Success");
				GOR.subscriptionOrderPlaced = true;
			}
			headerAndfooter.goToMyAccount();
			myaccount.clickSubscriptions();
			myaccount.accessOrder("click View", GOR.orderNumberSubscriptionOrder, null);
			myaccount.clickCancel_SubscriptionOrder();
			myaccount.verifyExpectedMessageAlert("Your subscription has been cancelled");
			GOR.subscriptionOrderCancelled = true;
		} catch (Exception e) {
			testStepFailed("Cancellation of a subscription order could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_PS_047 Description : To Verify if the user is able to
	 * resubscribe the cancelled subscription order.
	 */
	public void myAccounts_Resubscribe_SubscriptionProduct() {
		try {
			password = retrieve("securityPassword");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			cardNumber_data = retrieve("cardNumber");
			expirationYear_data = retrieve("expirationYear");
			expirationMonth_data = retrieve("expirationMonth");
			cardSecurityCode_data = retrieve("cardSecurityCode");

			if (GOR.subscriptionOrderPlaced == false) {
				billingdata = new HashMap<String, String>();
				shippingData = new HashMap<String, String>();

				GOR.shipDifferentAddress = true;

				billingdata.put("firstName_data", retrieve("firstName_Billing"));
				billingdata.put("lastName_data", retrieve("lastName_Billing"));
				billingdata.put("companyName_data", retrieve("companyName_Billing"));
				billingdata.put("phoneNumber_data", retrieve("phoneNumber_Billing"));
				billingdata.put("billingCountry_data", retrieve("country_Billing"));
				billingdata.put("streetAddress_data", retrieve("streetAddress_Billing"));
				billingdata.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_Billing"));
				billingdata.put("postCode_data", retrieve("postCode_Billing"));
				billingdata.put("city_data", retrieve("city_Billing"));
				billingdata.put("state_data", retrieve("state_Billing"));
				billingdata.put("email_data", retrieve("emailAddress"));

				shippingData.put("firstName_data", retrieve("firstName_shipping"));
				shippingData.put("lastName_data", retrieve("lastName_shipping"));
				shippingData.put("companyName_data", retrieve("companyName_shipping"));
				shippingData.put("phoneNumber_data", retrieve("phoneNumber_shipping"));
				shippingData.put("billingCountry_data", retrieve("billingCountry_shipping"));
				shippingData.put("streetAddress_data", retrieve("streetAddress_shipping"));
				shippingData.put("streetAddressNextLine_data", retrieve("streetAddressNextLine_shipping"));
				shippingData.put("postCode_data", retrieve("postCode_shipping"));
				shippingData.put("city_data", retrieve("city_shipping"));
				shippingData.put("state_data", retrieve("state_shipping"));
				shippingData.put("orderNotes", retrieve("orderNotes"));
			}

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.loggedIn == false) {
				if (GOR.OfferPopUpHandled == false) {
					homePage.closeOfferPopup();
				}
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}
			if (GOR.subscriptionOrderPlaced == false) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
				headerAndfooter.goTo_Shop(0, 1);
				cart_AllProducts.clickProductWithSubscribeLabel();
				cart_AllProducts.clickSubscribeRadioButton();
				cart_AllProducts.selectOption_SubscriptionDropdown("month");
				cart_AllProducts.clickSignUpNow();
				cart_AllProducts.clickViewCart();
				myCart.clickProceedToCheckOut();
				checkout.clickShipDifferentAddress(true);
				checkout.fillDetails(billingdata, shippingData);
				checkout.fillDetailsforCreditCard(cardNumber_data, expirationMonth_data, expirationYear_data,
						cardSecurityCode_data);
				checkout.clickTermsAndConditions();
				checkout.clickPlaceOrder();
				GOR.orderNumberMainOrder = checkout.verifySuccessMessage();
				GOR.orderNumberSubscriptionOrder = checkout
						.verifyRelevantMessage("Subscription Product Purchase Success");
				GOR.subscriptionOrderPlaced = true;
			}
			headerAndfooter.goToMyAccount();
			myaccount.clickSubscriptions();
			myaccount.accessOrder("click View", GOR.orderNumberSubscriptionOrder, null);
			if (GOR.subscriptionOrderCancelled == false) {
				myaccount.clickCancel_SubscriptionOrder();
			}
			myaccount.clickResubscribe_SubscriptionOrder();
			myaccount.verifyExpectedMessageAlert("Complete checkout to resubscribe");
//			checkout.clickOnUseNewCardIfPresent();
			checkout.clickTermsAndConditions();
			checkout.clickPlaceOrder();
			GOR.orderNumberMainOrder = checkout.verifySuccessMessage();
			GOR.orderNumberSubscriptionOrder = checkout.verifyRelevantMessage("Subscription Product Purchase Success");
			GOR.subscriptionOrderPlaced = true;

		} catch (Exception e) {
			testStepFailed("Cancellation of a subscription order could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || checkout.testFailure || headerAndfooter.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : AccountDashboard_HB_TS_055_063 Description : To Verify if the
	 * user is able to navigate to account dashboard page and verify if the user is
	 * able to view the account details.
	 */

	public void myAccount_viewAccountDetails() {
		try {
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			headerAndfooter.verifyNavigation("myAccountHeader");
			myaccount.clickOnAccountDetails();
			myaccount.checkAccountDetailsDisplay();

		} catch (Exception e) {
			testStepFailed("Navigation to My Account page and Account Details display could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure
				|| login_register.testFailure || login_register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : AccountDashboard_HB_TS_066 Description : To Verify proper
	 * validation error message is displayed upon saving account details with any
	 * invalid data.
	 */

	public void myAccount_invalidDataError() {
		try {
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			String currentPassword = retrieve("currentPassword");
			String newPassword = retrieve("newPassword");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			headerAndfooter.verifyNavigation("myAccountHeader");
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainAccountDetailsFields(" ", " ", " ", " ");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("emailAddressMissing");
			myaccount.enterDatainAccountDetailsFields("", "", "", "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkEmptyFieldErrorMessages();
			myaccount.enterDatainPasswordFields(currentPassword, "", "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("fillAllPasswordfieldsError");
			myaccount.enterDatainPasswordFields(currentPassword, newPassword, "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("reEnterPasswordError");
			myaccount.enterDatainPasswordFields("", newPassword, "");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("enterCurrentPasswordError");
			myaccount.enterDatainPasswordFields(" ", newPassword, " ");
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.checkMessageDisplayed("passwordsDoNotMatch");

		} catch (Exception e) {
			testStepFailed("The error messages for invalid account details could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure
				|| login_register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : AccountDashboard_HB_TS_064 Description : To Verify if the user
	 * is able to save the basic details in the account details section.
	 */
	public void myAccount_saveBasicDetails() {
		try {
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			String displayName = retrieve("displayName");
			String email = retrieve("email");
			String oldFirstName = retrieve("oldFirstName");
			String oldLastName = retrieve("oldLastName");
			String oldDisplayName = retrieve("oldDisplayName");
			String oldEmail = retrieve("oldEmail");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			headerAndfooter.verifyNavigation("myAccountHeader");
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainAccountDetailsFields(firstName, lastName, displayName, email);
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainAccountDetailsFields(oldFirstName, oldLastName, oldDisplayName, oldEmail);
			myaccount.clickOnSaveChangesMyAccounts();
		} catch (Exception e) {
			testStepFailed("Editing basic details of a account could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure
				|| login_register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : MyAccount_DFB_TS_100 Description : To Verify is the user is able
	 * to change the password.
	 */
	public void myAccount_ChangePassword() {
		try {
			Username = retrieve("loginUsername");
			Password = retrieve("loginPassword");
			String oldPassword = retrieve("oldPassword");
			String newPassword = retrieve("newPassword");

			headerAndfooter.goTo_Home();

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goTo_Home();
				homePage.goToLogin_Register();
				login_register.login(Username, Password);
			}

			headerAndfooter.goToMyAccount();
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainPasswordFields(oldPassword, newPassword, newPassword);
			myaccount.clickOnSaveChangesMyAccounts();
			myaccount.clickOnAccountDetails();
			myaccount.enterDatainPasswordFields(newPassword, oldPassword, oldPassword);
			myaccount.clickOnSaveChangesMyAccounts();

		} catch (Exception e) {
			testStepFailed("Changing password of the account could not be verified");
		}
		if (obj.testFailure || headerAndfooter.testFailure || myaccount.testFailure || homePage.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
