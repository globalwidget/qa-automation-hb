package scenarios.rewards;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;

public class Rewards extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	pages.Rewards rewards;
	HeaderAndFooters headerAndfooter;
	Login_Register login_Register;
	private boolean status = false;

	String password;

	public Rewards(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		rewards = new pages.Rewards(obj);
		headerAndfooter = new HeaderAndFooters(obj);
		login_Register = new Login_Register(obj);
	}

	/*
	 * TestCaseid : Navigations_HB_SR_001 Register Description : To Verify the
	 * rewards link are displayed on the header and footer
	 */
	public void verifyRewardsLink() {
		try {
			password = retrieve("securityPassword");
			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			headerAndfooter.goTo_Rewards();
			rewards.verifyRewardsPage();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_025 Register Description : To Verify if the
	 * user is able to get 50 points after successful creation of a new account.
	 */
	public void verifyAccountCreationPoints() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.clickRewardsHistory();
			rewards.verifyMyRewardsSection("create an account", 50, "approved");
			rewards.closeMyRewardsSection();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_031 Register Description : To Verify if the
	 * user is able to get 50 points upon clicking the Facebook share button and the
	 * Facebook window opened.
	 */
	public void verifyFacebookSharePoints() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("shareOnFacebook");
			rewards.verifyNavigationOrMessage("facebookLogin");
			headerAndfooter.goTo_Rewards();
			rewards.clickRewardsHistory();
			rewards.verifyMyRewardsSection("share on facebook", 50, "approved");
			rewards.closeMyRewardsSection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_037 Register Description : To Verify HB blogs
	 * page is displayed in new tab upon clicking 'Read Content' from blog section.
	 */
	public void verifyCBDblogPage() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("readOurBlog");
			rewards.verifyNavigationOrMessage("cbdBlogPage");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_038 Register Description : To Verify mouse
	 * hovering on blogs displays the question 'Have you read our blog'.
	 */
	public void verifyblogReadQuestion() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("readOurBlog");
			waitTime(2);
			switchToLastTab();
			driver.close();
			waitTime(2);
			switchToLastTab();
			rewards.verifyNavigationOrMessage("haveYouReadBlog");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_040 Register Description : To Verify the error
	 * message 'Sorry, This is not the right answer' is displayed for the wrong
	 * answers.
	 */
	public void readBlogQuestion_invalidAnswer() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("readOurBlog");
			waitTime(2);
			switchToLastTab();
			driver.close();
			waitTime(2);
			switchToLastTab();
			rewards.answerHaveYouReadBlog("no");
			rewards.verifyNavigationOrMessage("haveYouReadBlog-Wrong");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_039 Register Description : To Verify the blog
	 * points 25 is getting added to rewards once the user submits the valid answer
	 * (Yes) for the blog question.
	 */
	public void readBlogQuestion_validAnswer() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("readOurBlog");
			waitTime(2);
			switchToLastTab();
			driver.close();
			waitTime(2);
			switchToLastTab();
			rewards.answerHaveYouReadBlog("yes");
			rewards.verifyNavigationOrMessage("haveYouReadBlog-Correct");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_036 Register Description : To Verify if the
	 * user is able to get 25 points after reading the blog.
	 */
	public void readBlog_Points() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.clickRewardsHistory();
			rewards.verifyMyRewardsSection("read our blog", 25, "approved");
			rewards.closeMyRewardsSection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_041 Register Description : To Verify if the
	 * user is able to get 25 points after clicking the 'Follow Us' button in
	 * Instagram section. (Validation won't happen if they actually followed).
	 */
	public void verifyInstagramFollow_Points() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("followOnInstagram");
			rewards.verifyNavigationOrMessage("instagram");
			headerAndfooter.goTo_Rewards();
			rewards.clickRewardsHistory();
			rewards.verifyMyRewardsSection("follow us on instagram", 25, "approved");
			rewards.closeMyRewardsSection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_042 Register Description : To Verify if the
	 * user is able to get 25 rewards points once they click on 'Visit Page' on
	 * Facebook section (Validation won't happen if they actually visted the page).
	 */
	public void verifyFacebookVisit_Points() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("visitFacebook");
			rewards.verifyNavigationOrMessage("facebook");
			headerAndfooter.goTo_Rewards();
			rewards.clickRewardsHistory();
			rewards.verifyMyRewardsSection("visit us on facebook", 25, "approved");
			rewards.closeMyRewardsSection();
		} catch (

		Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_045 Register Description : To Verify if the
	 * user is able to get 25 points after clicking the 'Follow Us' button in
	 * Twitter section. (Validation won't happen if they actually followed).
	 */
	public void verifyTwitterFollow_Points() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("followTwitter");
			rewards.verifyNavigationOrMessage("twitter");
			headerAndfooter.goTo_Rewards();
			rewards.clickRewardsHistory();
			rewards.verifyMyRewardsSection("follow us on twitter", 25, "approved");
			rewards.closeMyRewardsSection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_047 Register Description : To Verify HB Youtube
	 * channel is opened in new tab upon clicking 'Subscribe' from Youtube section.
	 */
	public void verifyYoutubeSubscribeButton() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("subscribeToYoutube");
			rewards.verifyNavigationOrMessage("youtube");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_048 Register Description : To Verify mouse
	 * hovering on Youtube section displays the question 'Did you subscribe to the
	 * hemp bombs Youtube Channel'.
	 */
	public void verifyDidYouSubscribe() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("subscribeToYoutube");
			waitTime(2);
			switchToLastTab();
			driver.close();
			waitTime(2);
			switchToLastTab();
			rewards.verifyNavigationOrMessage("didYouSubscribe");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_050 Register Description : To Verify the error
	 * message 'Sorry, This is not the right answer' is displayed for the wrong
	 * answers.
	 */
	public void didYouSubscribe_invalidAnswer() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("subscribeToYoutube");
			waitTime(2);
			switchToLastTab();
			driver.close();
			waitTime(2);
			switchToLastTab();
			rewards.answerdidYouSubscribe("no");
			rewards.verifyNavigationOrMessage("didYouSubscribe-Wrong");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_049 Register Description : To Verify the
	 * Youtube Subscribe points 25 is getting added to rewards once the user submits
	 * the valid answer (Yes) for the Youtube subscribe question.
	 */
	public void didYouSubscribe_validAnswer() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.navigate("subscribeToYoutube");
			waitTime(2);
			switchToLastTab();
			driver.close();
			waitTime(2);
			switchToLastTab();
			rewards.answerdidYouSubscribe("yes");
			rewards.verifyNavigationOrMessage("didYouSubscribe-Correct");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : EarnPoints_HB_SR_046 Register Description : To Verify if the
	 * user is able to get 25 rewards points once they click on subscribe button in
	 * Youtube section and gives a valid answer for the question (Validation won't
	 * happen if they actually subscribed).
	 */
	public void didYouSubscribe_Points() {
		try {
			password = retrieve("securityPassword");
			String userId = retrieve("UserId");
			String userPassword = retrieve("Password");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.freshAccountCreated == false) {
				homePage.goToLogin_Register();
				login_Register.register(userId, userPassword);
				GOR.freshAccountCreated = true;
				GOR.loggedIn = true;
			} else {
				if (GOR.loggedIn == false) {
					homePage.goToLogin_Register();
					login_Register.login(userId, userPassword);
				} else {
					testStepInfo("Already logged in with relevant account");
				}
			}
			headerAndfooter.goTo_Rewards();
			rewards.clickRewardsHistory();
			rewards.verifyMyRewardsSection("subscribe to our youtube channel", 25, "approved");
			rewards.closeMyRewardsSection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || rewards.testFailure || headerAndfooter.testFailure
				|| login_Register.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
