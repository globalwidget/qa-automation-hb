package scenarios.wholesale;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Wholesale_BecomeDistributor;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;

public class Wholesale extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	Wholesale_BecomeDistributor becomeDistributor;
	HeaderAndFooters headerAndFooter;
	private boolean status = false;

	String password;

	public Wholesale(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		becomeDistributor = new Wholesale_BecomeDistributor(obj);
		headerAndFooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : HB_TS_010 Description : Verify if the user is able to submit the
	 * 'Become a Distributor' form with all the valid details
	 * 
	 */
	public void wholesale_submitWithvalidDetails() {
		try {
			password = retrieve("securityPassword");
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String ein = retrieve("ein");
			String businessName = retrieve("businessName");
			String email = retrieve("email");
			String phone = retrieve("phone");
			String address = retrieve("address");
			String city = retrieve("city");
			String state = retrieve("state");
			String zip = retrieve("zip");
			String message = retrieve("message");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndFooter.goTo_Wholesale(1);
			becomeDistributor.FillAllDetails(firstName, lastName, businessName, 1, ein, email, phone, address, city,
					state, zip, message);
			becomeDistributor.clickTellMeMore();
			becomeDistributor.VerifySucessMessage();
		} catch (

		Exception e) {
			testStepFailed("Error message for submitting valid details in distributor form could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || becomeDistributor.testFailure || headerAndFooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : HB_TS_011 Description : Verify if the user is not able to submit
	 * the 'Become a Distributor' form with invalid details
	 * 
	 */
	public void wholesale_submitWithInvalidDetails() {
		try {
			password = retrieve("securityPassword");
			String invalidEmail = retrieve("invalidEmail");
			String firstName = retrieve("firstName");
			String lastName = retrieve("lastName");
			String ein = retrieve("ein");
			String businessName = retrieve("businessName");
			String email = retrieve("email");
			String phone = retrieve("phone");
			String address = retrieve("address");
			String city = retrieve("city");
			String state = retrieve("state");
			String zip = retrieve("zip");
			String message = retrieve("message");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			headerAndFooter.goTo_Wholesale(1);
			becomeDistributor.FillAllDetails("", "", "", 0, "", invalidEmail, "", "", "", "", "", "");
			becomeDistributor.clickTellMeMore();
			becomeDistributor.VerifyErrorMessage_InvalidEmail();
			becomeDistributor.FillAllDetails(" ", " ", " ", 0, " ", " ", " ", " ", " ", " ", " ", " ");
			becomeDistributor.clickTellMeMore();
			driver.switchTo().activeElement();
			becomeDistributor.VerifyErrorMessage_EmptyFields();
			becomeDistributor.FillAllDetails(firstName, lastName, businessName, 1, ein, email, phone, address, city,
					state, zip, message);
		} catch (

		Exception e) {
			testStepFailed("Error message for Login using Invalid Credentials could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || becomeDistributor.testFailure || headerAndFooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
