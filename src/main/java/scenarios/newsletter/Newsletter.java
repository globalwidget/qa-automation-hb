package scenarios.newsletter;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Newsletter extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	HeaderAndFooters headersAndFooters;
	private boolean status = false;

	String password;

	public Newsletter(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		headersAndFooters = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : Newsletter Description : Verify the error message is displayed
	 * for invalid email address while submitting the newsletter
	 */
	public void newsletter_invalidData() {
		try {
			password = retrieve("securityPassword");
			String invalidEmail = retrieve("invalidEmail");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			headersAndFooters.goTo_Home();
			GOR.OfferPopUpHandled = false;
			homePage.closeOfferPopup();
			homePage.scrollToView_Newsletter();
			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			homePage.clickSignUpAndSave();
			homePage.verifyEmptyEmailErrorNewsletter();
			homePage.enterEmailNewsletter(invalidEmail);
			homePage.clickSignUpAndSave();
			homePage.verifyInvalidEmailErrorNewsletter();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headersAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Newsletter Description : Verify if the user is able to sign up
	 * for newsletter with valid email address
	 */
	public void newsletter_validData() {
		try {
			password = retrieve("securityPassword");
			String email = retrieve("email");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headersAndFooters.goTo_Home();
			homePage.scrollToView_Newsletter();
			homePage.enterEmailNewsletter(email);
			homePage.clickSignUpAndSave();
			homePage.verifySuccessMessage_Newsletter();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headersAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
