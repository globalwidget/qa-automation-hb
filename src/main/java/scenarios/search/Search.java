package scenarios.search;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Search extends ApplicationKeywords {
	BaseClass obj;
	HeaderAndFooters headerAndFooters;
	HomePage homepage;
	private boolean status = false;

	public Search(BaseClass obj) {
		super(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		this.obj = obj;
	}

	/*
	 * TestCaseid : Search_HB_TS_067 Description : To Verify 'no results' page is
	 * displayed upon searching with invalid / not available products.
	 */
	public void invalid_Search() {
		try {

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.enterQuery_SearchBox(retrieve("invalidQuery"));
			headerAndFooters.pressEnterButton();
			headerAndFooters.verifyNavigation("noSearchResults Message");
		} catch (Exception e) {
			testStepFailed("Invalid Search could not be verified");
		}
		if (obj.testFailure || homepage.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Search_HB_TS_068_069 Description : To Verify the user is able to
	 * search the valid products and user is able to see the proper results based on
	 * the search criteria.
	 */
	public void valid_Search() {
		try {

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}

			headerAndFooters.goTo_Home();
			headerAndFooters.enterQuery_SearchBox(retrieve("validQuery"));
			headerAndFooters.pressEnterButton();
			headerAndFooters.verifyNavigation("Number of Items");
			headerAndFooters.verifyNavigation("Gummies Search Product Title");

		} catch (Exception e) {
			testStepFailed("Valid Search could not be verified");
		}
		if (obj.testFailure || homepage.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
