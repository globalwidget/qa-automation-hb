package scenarios.PGP_PDP;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.MyCart;

public class PGP_PDP extends ApplicationKeywords {
	BaseClass obj;
	HeaderAndFooters headersAndFooters;
	HomePage homepage;
	Cart_AllProducts cart_AllProducts;
	MyCart myCart;
	private boolean status = false;

	String password;

	public PGP_PDP(BaseClass obj) {
		super(obj);
		this.obj = obj;
		headersAndFooters = new HeaderAndFooters(obj);
		homepage = new HomePage(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		myCart = new MyCart(obj);
	}

	/*
	 * (PGP - Product Grid Page) => TestCaseid : PGP_HB_PS_001 Description : Verify
	 * if the user is able to see the subscription text with % offer next to the
	 * product price in grid page for the subscription enabled products.
	 */
	public void verifySubscriptionDiscountLabel_PGP() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homepage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}
			headersAndFooters.goTo_Shop(0, 0);
			cart_AllProducts.verifySubscriptionMessage_PGP();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homepage.testFailure || headersAndFooters.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * (PDP - Product Description Page) => TestCaseid : PDP_HB_PS_002 Description :
	 * Verify if the user is able to see the subscription option in product
	 * description page for the enabled products.
	 */
	public void verifySubscriptionLabel_PDP() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homepage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}
			headersAndFooters.goTo_Shop(0, 0);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.verifySubscribeLabel_PDP();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homepage.testFailure || headersAndFooters.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * (PDP - Product Description Page) => TestCaseid : PDP_HB_PS_003 Description :
	 * Verify the default option is 'One time purchase' in product descripton page
	 */
	public void verifyDefaultOptionIsOneTimePurchase() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homepage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}
			headersAndFooters.goTo_Shop(0, 0);
			cart_AllProducts.clickProductWithSubscribeLabel();
			if (cart_AllProducts.verifyOneTimeSubscriptionIsSelected() == true) {
				testStepInfo("The default option selected is one time purchase");
			} else {
				testStepFailed("The default option selected is not one time purchase");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homepage.testFailure || headersAndFooters.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * (PDP - Product Description Page) => TestCaseid : PDP_HB_PS_004 Description :
	 * Verify clicking on 'Subscribe and save up to 20%' option is showing the
	 * subscription drop down options
	 */
	public void verifySubscribeDropdown() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homepage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}
			headersAndFooters.goTo_Shop(0, 0);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.verifySubscribeDropdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homepage.testFailure || headersAndFooters.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * (PDP - Product Description Page) => TestCaseid : PDP_HB_PS_005 Description :
	 * Verify the subscription period, price with % offer details are displayed in
	 * the drop down
	 */
	public void verifySubscribeDropdownOptions() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homepage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}
			headersAndFooters.goTo_Shop(0, 0);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.verifySubscribeDropdownOptions();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homepage.testFailure || headersAndFooters.testFailure || cart_AllProducts.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * (PDP - Product Description Page) => TestCaseid : PDP_HB_PS_006 Description :
	 * Verify clicking on 'Sign Up Now' is adding the product to cart with the
	 * selected subscription plan
	 */
	public void verifySubscribptionSelectedInMyCart() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homepage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homepage.closeOfferPopup();
			}
			headersAndFooters.clickOnMyCart();
			myCart.removeAllProducts();
			headersAndFooters.goTo_Shop(0, 0);
			cart_AllProducts.clickProductWithSubscribeLabel();
			cart_AllProducts.clickSubscribeRadioButton();
			cart_AllProducts.selectOption_SubscriptionDropdown("month");
			cart_AllProducts.clickSignUpNow();
			cart_AllProducts.clickViewCart();
			if (myCart.verifySubscriptionSelectedIsPerMonth() == true) {
				testStepInfo("The Subscription option selected in the PDP page is reflected in the My Cart Page");
			} else {
				testStepFailed("The Subscription option selected in the PDP page is not reflected in the My Cart Page");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homepage.testFailure || headersAndFooters.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
