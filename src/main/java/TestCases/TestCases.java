package TestCases;

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import scenarios.LabTests.LabTests;
import scenarios.PGP_PDP.PGP_PDP;
import scenarios.cart.Cart;
import scenarios.checkout.Checkout;
import scenarios.contact.Contact;
import scenarios.headerAndFooter.HeaderAndFooters;
import scenarios.myAccounts.MyAccounts;
import scenarios.newsletter.Newsletter;
import scenarios.rewards.Rewards;
import scenarios.search.Search;
import scenarios.wholesale.*;
import automationFramework.GenericKeywords;
import baseClass.BaseClass;

@Listeners({ utilities.HtmlReport.class })
public class TestCases {
	private BaseClass obj;

	////////////////////////////////////////////////////////////////////////////////
	// Function Name :
	// Purpose :
	// Parameters :
	// Return Value :
	// Created by :
	// Created on :
	// Remarks :
	/////////////////////////////////////////////////////////////////////////////////

	private void TestStart(String testCaseName) {

		obj.currentTestCaseName = testCaseName;
		obj.setEnvironmentTimeouts();
		obj.reportStart();
		obj.iterationCount.clear();
		obj.iterationCountInTextData();

	}

	@BeforeClass
	@Parameters({ "selenium.machinename", "selenium.host", "selenium.port", "selenium.browser", "selenium.os",
			"selenium.browserVersion", "selenium.osVersion", "TestData.SheetNumber" })
	public void precondition(String machineName, String host, String port, String browser, String os,
			String browserVersion, String osVersion, String sheetNo) {
		obj = new BaseClass();
		if (os.contains("Android")) {
			obj.startServer(host, port);
			obj.waitTime(10);
		}
		obj.setup(machineName, host, port, browser, os, browserVersion, osVersion, sheetNo);
	}

	@Test(alwaysRun = true)
	public void Login_HB_TS_005(Method method) {
		TestStart(method.getName());
		scenarios.login_logout.Login_Logout login_logout = new scenarios.login_logout.Login_Logout(obj);
		login_logout.dataRowNo = Integer.parseInt(login_logout.iterationCount.get(0).toString());
		login_logout.login_invalidCredentials();

		obj.testFailure = login_logout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Login_HB_TS_004(Method method) {
		TestStart(method.getName());
		scenarios.login_logout.Login_Logout login_logout = new scenarios.login_logout.Login_Logout(obj);
		login_logout.dataRowNo = Integer.parseInt(login_logout.iterationCount.get(0).toString());
		login_logout.login();

		obj.testFailure = login_logout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Logout_HB_TS_006(Method method) {
		TestStart(method.getName());
		scenarios.login_logout.Login_Logout login_logout = new scenarios.login_logout.Login_Logout(obj);
		login_logout.dataRowNo = Integer.parseInt(login_logout.iterationCount.get(0).toString());
		login_logout.logout();

		obj.testFailure = login_logout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Register_HB_TS_001(Method method) {
		TestStart(method.getName());
		scenarios.register.Register register = new scenarios.register.Register(obj);
		register.dataRowNo = Integer.parseInt(register.iterationCount.get(0).toString());
		register.register();
		obj.testFailure = register.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Register_HB_TS_002(Method method) {
		TestStart(method.getName());
		scenarios.register.Register register = new scenarios.register.Register(obj);
		register.dataRowNo = Integer.parseInt(register.iterationCount.get(0).toString());
		register.register_invalidCredentials();
		register.register_existingCredentials();
		obj.testFailure = register.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Wholesale_HB_TS_010(Method method) {
		TestStart(method.getName());
		Wholesale wholesale = new Wholesale(obj);
		wholesale.dataRowNo = Integer.parseInt(wholesale.iterationCount.get(0).toString());
		wholesale.wholesale_submitWithvalidDetails();
		obj.testFailure = wholesale.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Wholesale_HB_TS_011(Method method) {
		TestStart(method.getName());
		Wholesale wholesale = new Wholesale(obj);
		wholesale.dataRowNo = Integer.parseInt(wholesale.iterationCount.get(0).toString());
		wholesale.wholesale_submitWithInvalidDetails();
		obj.testFailure = wholesale.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_HB_TS_015(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.addProductToCart(1);
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_HB_TS_017(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyRemoveProductFromCart();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_HB_TS_018(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyUpdateQuantityInCart();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_HB_TS_019(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyValidCouponEntry();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_HB_TS_020(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyInvalidValidCouponEntry();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_HB_TS_037(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_HB_TS_024(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidCreditCardDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_HB_TS_036_023(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ValidDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_HB_TS_030(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_VerifySavedDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void General_HB_TS_013(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyHeader();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void General_HB_TS_014(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyFooter();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Contact_HB_TS_048(Method method) {
		TestStart(method.getName());
		Contact contact = new Contact(obj);
		contact.dataRowNo = Integer.parseInt(contact.iterationCount.get(0).toString());
		contact.submitInvalidDetails();
		obj.testFailure = contact.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Contact_HB_TS_047(Method method) {
		TestStart(method.getName());
		Contact contact = new Contact(obj);
		contact.dataRowNo = Integer.parseInt(contact.iterationCount.get(0).toString());
		contact.submitValidDetails();
		obj.testFailure = contact.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Newsletter_HB_TS_052(Method method) {
		TestStart(method.getName());
		Newsletter newsletter = new Newsletter(obj);
		newsletter.dataRowNo = Integer.parseInt(newsletter.iterationCount.get(0).toString());
		newsletter.newsletter_invalidData();
		obj.testFailure = newsletter.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Newsletter_HB_TS_051(Method method) {
		TestStart(method.getName());
		Newsletter newsletter = new Newsletter(obj);
		newsletter.dataRowNo = Integer.parseInt(newsletter.iterationCount.get(0).toString());
		newsletter.newsletter_validData();
		obj.testFailure = newsletter.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void LabTest_HB_TS_054(Method method) {
		TestStart(method.getName());
		LabTests labTests = new LabTests(obj);
		labTests.dataRowNo = Integer.parseInt(labTests.iterationCount.get(0).toString());
		labTests.verifyInvalidSearch();
		obj.testFailure = labTests.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void LabTest_HB_TS_053(Method method) {
		TestStart(method.getName());
		LabTests labTests = new LabTests(obj);
		labTests.dataRowNo = Integer.parseInt(labTests.iterationCount.get(0).toString());
		labTests.verifyValidSearch();
		obj.testFailure = labTests.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_HB_TS_055(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.addProductToCart(2);
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Navigations_HB_SR_001(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyRewardsLink();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_025(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyAccountCreationPoints();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_031(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyFacebookSharePoints();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_037(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyCBDblogPage();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_038(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyblogReadQuestion();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_040(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.readBlogQuestion_invalidAnswer();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_039(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.readBlogQuestion_validAnswer();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_036(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.readBlog_Points();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_041(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyInstagramFollow_Points();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_042(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyFacebookVisit_Points();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_045(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyTwitterFollow_Points();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_047(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyYoutubeSubscribeButton();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_048(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.verifyDidYouSubscribe();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_050(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.didYouSubscribe_invalidAnswer();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_049(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.didYouSubscribe_validAnswer();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void EarnPoints_HB_SR_046(Method method) {
		TestStart(method.getName());
		Rewards rewards = new Rewards(obj);
		rewards.dataRowNo = Integer.parseInt(rewards.iterationCount.get(0).toString());
		rewards.didYouSubscribe_Points();
		obj.testFailure = rewards.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_079(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.loginFromMyCart();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_063(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.checkCurrentPointsDisplay();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_055(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.checkRedemtptionsDropdown();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_070(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyCouponRewardFeature();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_056(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verify150pointsRewardAmount();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_061(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifySubtotalAfterPointsRedemption();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_065(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyRemoveRewardsApplied();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_SR_062(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyUnselectableDropdownForLackOfPoints();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PGP_HB_PS_001(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.verifySubscriptionDiscountLabel_PGP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_HB_PS_002(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.verifySubscriptionLabel_PDP();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_HB_PS_003(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.verifyDefaultOptionIsOneTimePurchase();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_HB_PS_004(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.verifySubscribeDropdown();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_HB_PS_005(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.verifySubscribeDropdownOptions();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void PDP_HB_PS_006(Method method) {
		TestStart(method.getName());
		PGP_PDP pgp_pdp = new PGP_PDP(obj);
		pgp_pdp.dataRowNo = Integer.parseInt(pgp_pdp.iterationCount.get(0).toString());
		pgp_pdp.verifySubscribptionSelectedInMyCart();
		obj.testFailure = pgp_pdp.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_PS_012(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyDifferentSubscriptionSelection();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_PS_013(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyPercentageOffer();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_PS_020(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifySalesTaxForSubscriptionOrder();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Cart_HB_PS_021(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyRecurringTotalsSection();
		obj.testFailure = cart.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_HB_PS_022(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_fillDetails_SubscriptionProduct();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_HB_PS_031(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidCreditCardDetails_SubscriptionProduct();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_HB_PS_030(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_VerifyCreateAccountPassword_SubscriptionProduct();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_HB_PS_023(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ShipDifferentAddress_SubscriptionProduct();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Checkout_HB_PS_032(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ValidccDetails_SubscriptionProduct();
		obj.testFailure = checkout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccounts_HB_PS_039(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccounts_verifyOrderInSubscriptions_SubscriptionProduct();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccounts_HB_PS_035(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccounts_verifyMainOrderInOrders_SubscriptionProduct();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccounts_HB_PS_043(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccounts_Cancel_SubscriptionProduct();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void MyAccounts_HB_PS_047(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccounts_Resubscribe_SubscriptionProduct();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void AccountDashboard_HB_TS_055_063(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccount_viewAccountDetails();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void AccountDashboard_HB_TS_066(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccount_invalidDataError();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void AccountDashboard_HB_TS_064(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccount_saveBasicDetails();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void AccountDashboard_HB_TS_065(Method method) {
		TestStart(method.getName());
		MyAccounts myAccounts = new MyAccounts(obj);
		myAccounts.dataRowNo = Integer.parseInt(myAccounts.iterationCount.get(0).toString());
		myAccounts.myAccount_ChangePassword();
		obj.testFailure = myAccounts.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Search_HB_TS_067(Method method) {
		TestStart(method.getName());
		Search search = new Search(obj);
		search.dataRowNo = Integer.parseInt(search.iterationCount.get(0).toString());
		search.invalid_Search();
		obj.testFailure = search.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Search_HB_TS_068_069(Method method) {
		TestStart(method.getName());
		Search search = new Search(obj);
		search.dataRowNo = Integer.parseInt(search.iterationCount.get(0).toString());
		search.valid_Search();
		obj.testFailure = search.testFailure;
		TestEnd();
	}

	@AfterClass
	public void closeSessions() {
		obj.closeAllSessions();
	}

	private void TestEnd() {
		obj.waitTime(1);
		if (obj.testFailure) {
			GenericKeywords.testFailed();
		}
	}

	@BeforeMethod
	public void before() {
		obj.testFailure = false;
	}

}
