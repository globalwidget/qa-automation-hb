package iSAFE;

public class GOR {
	public static final String ifm_iFrame = "IFrame#xpath=//iframe[contains(@id,'iframe')]";
	public static final String btn_refreshHomePageTab = "Refresh home page#xpath=//div[@title='Refresh tab']";
	public static final String ifm_iFrameOfAddToFavorites = "IFrame#xpath=//iframe[contains(@id,'floating_iframe')]";
	public static final String txt_addToCartResultsMessage = "Add to cart result message#xpath=//td[@id='tdMessageWindowMessage']";
	public static final String ifm_supplierWebsite = "Grainger iFrame #xpath=//iframe[@name='IFrameControl_Supplier_Website']";
	public static final String lbl_siteHeaderText = "Site Header#classname=SiteHeaderText";
	public static final String ele_messageWindow = "Message window#id=tdMessageWindowMessage";

	// Display View
	public static final String img_DisplaylistView = "Displayed list view icon#xpath=//img[contains(@description,'List Display')]";
	public static final String img_DisplayThumbnailView = "Displayed list view icon#xpath=//img[contains(@description,'Thumbnail Display')]";

	public static boolean OfferPopUpHandled = false;
	public static boolean securityLogin = true;
	// Make it true to avoid registering a new account, for rewards scenario.
	public static boolean freshAccountCreated = true;
	public static boolean completeCheckoutWithSavedCard = false; // Make it false for prod. runs

	// Need not be changed manually.
	public static boolean loggedIn = false;
	public static boolean productAdded = false;
	public static boolean shipDifferentAddress = false;
	public static boolean rewardsApplied_150Points = false;
	public static boolean subscriptionOrderPlaced = false;
	public static boolean subscriptionOrderCancelled = false;
	public static String orderNumberMainOrder = "";
	public static String orderNumberSubscriptionOrder = "";

}
