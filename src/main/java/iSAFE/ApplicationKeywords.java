package iSAFE;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import automationFramework.DBKeywords;
import baseClass.BaseClass;

public class ApplicationKeywords extends DBKeywords {

	public ApplicationKeywords(BaseClass obj) {
		// TODO Auto-generated constructor stub
		super(obj);
	}

	public ApplicationKeywords() {

	}

	public void highLighterMethod(String element) {
		try {
			WebElement e = findWebElement(element);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", e);
			waitTime(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void highLighterMethod(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",
					element);
			waitTime(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void scrollToViewElement(String element_String) {
		try {
			if (isElementPresent(element_String)) {
				WebElement element = findWebElement(element_String);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
				testStepInfo("Successfully scrolled to view the element");

			} else {
				testStepFailed("Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not scroll to view the element");
			e.printStackTrace();
		}
	}

	public void scrollToViewElement(WebElement element) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			testStepInfo("Successfully scrolled to view the element");
		} catch (Exception e) {
			testStepFailed("Could not scroll to view the element");
			e.printStackTrace();
		}
	}

	public void scrollUp() {
		try {
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0)");
			testStepInfo("Successfully scrolled up");
		} catch (Exception e) {
			testStepFailed("Could not scroll up");
			e.printStackTrace();
		}
	}

	public void scrollDown() {
		try {
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0,50)");
			testStepInfo("Successfully scrolled down");

		} catch (Exception e) {
			testStepFailed("Could not scroll down");
			e.printStackTrace();
		}
	}

	public void navigateMenu_Two(String element_1, String element_2) {
		try {
			Actions action = new Actions(driver);
			highLighterMethod(element_1);
			highLighterMethod(element_2);
			WebElement webelement_1 = findWebElement(element_1);
			WebElement webelement_2 = findWebElement(element_2);
			action.moveToElement(webelement_1).moveToElement(webelement_2).click().build().perform();
		} catch (Exception e) {
			testStepFailed("Could not navigate as requested");
			e.printStackTrace();
		}
	}

	public void navigateMenu_Three(String element_1, String element_2, String element_3) {
		try {
			Actions action = new Actions(driver);
			highLighterMethod(element_1);
			highLighterMethod(element_2);
			highLighterMethod(element_3);
			WebElement webelement_1 = findWebElement(element_1);
			WebElement webelement_2 = findWebElement(element_2);
			WebElement webelement_3 = findWebElement(element_3);
			action.moveToElement(webelement_1).moveToElement(webelement_2).build().perform();
			waitTime(1);
			action.moveToElement(webelement_3).click().build().perform();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void navigateMenu_Four(String element_1, String element_2, String element_3, String element_4) {
		try {
			Actions action = new Actions(driver);
			highLighterMethod(element_1);
			highLighterMethod(element_2);
			highLighterMethod(element_3);
			highLighterMethod(element_4);
			WebElement webelement_1 = findWebElement(element_1);
			WebElement webelement_2 = findWebElement(element_2);
			WebElement webelement_3 = findWebElement(element_3);
			WebElement webelement_4 = findWebElement(element_4);
			action.moveToElement(webelement_1).moveToElement(webelement_2).build().perform();
			waitTime(1);
			action.moveToElement(webelement_3).build().perform();
			waitTime(1);
			action.moveToElement(webelement_4).build().perform();
			waitTime(1);
			action.moveToElement(webelement_4).click().build().perform();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void switchToLastTab() {
		try {
			int index;
			Set<String> windowHandles = driver.getWindowHandles();
			List<String> windowStrings = new ArrayList<>(windowHandles);
			index = windowStrings.size() - 1;
			String reqWindow = windowStrings.get(index);
			driver.switchTo().window(reqWindow);
			testStepInfo("Switched to " + driver.getTitle());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}