package pages;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class MyAccount extends ApplicationKeywords {

	private static final String logoutButton = "Logout Button #xpath=//a[contains(text(),'Log out')]";
	private static final String addresses = "Addresses #xpath=//a[contains(text(),'Addresses')]";
	private static final String edit_Billing = "Edit Billing Adress #xpath=//a[@class='edit' and contains(@href,'billing')]";
	private static final String edit_Shipping = "Edit Shipping Adress #xpath=//a[@class='edit' and contains(@href,'shipping')]";
	private static final String firstName_Billing = "First Name Billing #id=billing_first_name";
	private static final String lastName_Billing = "Last Name Billing #id=billing_last_name";
	private static final String company_Billing = "Company Name Billing #id=billing_company";
	private static final String phoneNumber_Billing = "Phone Number Billing #id=billing_phone";
	private static final String country_Billing = "Country Billing #id=select2-billing_country-container";
	private static final String streetAddress_Billing = "Street Address Billing #id=billing_address_1";
	private static final String streetAddressNextLine_Billing = "Street Address Next Line Billing #id=billing_address_2";
	private static final String postCode_Billing = "Post Code Billing #id=billing_postcode";
	private static final String city_Billing = "City Billing #id=billing_city";
	private static final String state_Billing = "State Billing #id=select2-billing_state-container";
	private static final String email_Billing = "Email Billing #id=billing_email";
	private static final String firstName_Shipping = "First Name Shipping #id=shipping_first_name";
	private static final String lastName_Shipping = "Last Name Shipping #id=shipping_last_name";
	private static final String company_Shipping = "Company Name Shipping #id=shipping_company";
	private static final String country_Shipping = "Country Shipping #id=select2-shipping_country-container";
	private static final String streetAddress_Shipping = "Street Address Shipping #id=shipping_address_1";
	private static final String streetAddressNextLine_Shipping = "Street Address Next Line Shipping #id=shipping_address_2";
	private static final String postCode_Shipping = "Post Code Shipping #id=shipping_postcode";
	private static final String city_Shipping = "City Shipping #id=shipping_city";
	private static final String state_Shipping = "State Shipping #id=select2-shipping_state-container";
	private static final String subscriptions = "Subscriptions #xpath=//li[contains(@class,'navigation')]//a[contains(@href,'subscription')]";
	private static final String orders = "Subscriptions #xpath=//li[contains(@class,'navigation')]//a[contains(@href,'order')]";
	private static final String cancel_Subscription = "Cancel #xpath=//a[contains(text(),'Cancel')]";
	private static final String alertMessage = "Alert #xpath=//div[@role='alert']";
	private static final String resubscribe_Subscription = "Resubscribe #xpath=//a[contains(text(),'Resubscribe')]";
	private static final String accountDetails = "Account Details #xpath=//a[contains(text(),'Account details')]";
	private static final String firstNameMyAccount = "First Name - Account details #xpath=//input[@id='account_first_name']";
	private static final String lastNameMyAccount = "Second Name - Account details #xpath=//input[@id='account_last_name']";
	private static final String displayNameMyAccount = "Display Name - Account details #xpath=//input[@id='account_display_name']";
	private static final String accountEmailMyAccount = "Account Email - Account details #xpath=//input[@id='account_email']";
	private static final String currentPasswordMyAccount = "Current Password - Account details #xpath=//input[@id='password_current']";
	private static final String newPasswordMyAccount = "New Password - Account details #xpath=//input[@id='password_1']";
	private static final String confirmPasswordMyAccount = "Confirm Password - Account details #xpath=//input[@id='password_2']";
	private static final String saveChangesMyAccount = "Save Changes - Account details #xpath=//button[contains(text(),'Save changes')]";
	private static final String firstNameError = "First Name Missing Error - Account details #xpath=//ul/li[@data-id='account_first_name']";
	private static final String lastNameError = "Last Name Missing Error - Account details #xpath=//ul/li[@data-id='account_last_name']";
	private static final String displayNameError = "Display Name Missing Error - Account details #xpath=//ul/li[@data-id='account_display_name']";
	private static final String accountEmailError = "Account Email Missing Missing Error - My Account #xpath=//ul/li[@data-id='account_email']";
//	private static final String accountEmailInvalidError = "Account Email Invalid Error - Account details #xpath=//ul/li[contains(text(),'Please provide a valid email address')]";
	private static final String fillAllPasswordFieldsError = "Fill All Password Fields Error #xpath=//ul/li[contains(text(),'Please fill out all password fields.')]";
	private static final String enterCurrentPasswordError = "Enter Current Password Error #xpath=//ul/li[contains(text(),'Please enter your current password.')]";
	private static final String reEnterPasswordError = "Re-enter Password Error #xpath=//ul/li[contains(text(),'Please re-enter your password.')]";
//	private static final String accountDetailsSavedMessage = "Re-enter Password Error #xpath=//div[contains(text(),'Account details changed successfully.')]";
	private static final String passwordsDoNotMatchError = "Passwords do not match Error #xpath=//ul/li[contains(text(),'New passwords do not match')]";

	public MyAccount(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to logout
	 */
	public void clickOnLogout() {

		try {
			if (isElementPresent(logoutButton)) {
				highLighterMethod(logoutButton);
				clickOn(logoutButton);
				GOR.loggedIn = false;
				testStepInfo("Logged Out");
			} else {
				testStepFailed("Logout Button Not present");
			}
		} catch (Exception e) {
			testStepFailed("Logout could not be performed");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to go to addresses
	 */
	public void clickOnAddresses() {

		try {
			if (isElementPresent(addresses)) {
				highLighterMethod(addresses);
				clickOn(addresses);
			} else {
				testStepFailed("Addresses is not present");
			}
		} catch (Exception e) {
			testStepFailed("Addresses could not be clicked");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method to get saved billing details
	 */
	public HashMap<String, String> getSavedBilllingDetails() {
		HashMap<String, String> billingDetails = null;
		try {
			billingDetails = new HashMap<String, String>();
			if (isElementDisplayed(edit_Billing)) {
				highLighterMethod(edit_Billing);
				clickOn(edit_Billing);
				String[] fields = { firstName_Billing, lastName_Billing, company_Billing, phoneNumber_Billing,
						country_Billing, streetAddress_Billing, streetAddressNextLine_Billing, city_Billing,
						state_Billing, postCode_Billing, email_Billing };
				if (isElementPresent(firstName_Billing)) {
					for (int i = 0; i < fields.length; i++) {
						if (isElementDisplayed(fields[i])) {
							if (fields[i].contains("Country") || fields[i].contains("State")) {
								{
									String value = getText(fields[i]);
									highLighterMethod(fields[i]);
									billingDetails.put(fields[i], value);
								}
							} else {
								String value = findWebElement(fields[i]).getAttribute("value");
								highLighterMethod(fields[i]);
								billingDetails.put(fields[i], value);
							}
						} else {
							testStepFailed(fields[i].split("#")[0] + " input field was not displayed");
						}
					}
				} else {
					testStepFailed("First Name field was not present");
				}
			} else {
				testStepPassed("The edit button for billing was not present");
			}
		} catch (Exception e) {
			testStepFailed("Saved Billing Details could not be fetched successfully");
			e.printStackTrace();
		}
		return billingDetails;

	}

	/**
	 * Description: Method to get saved shipping details
	 */
	public HashMap<String, String> getSavedShippingDetails() {
		HashMap<String, String> shippingDetails = null;
		try {
			shippingDetails = new HashMap<String, String>();
			if (isElementDisplayed(edit_Shipping)) {
				highLighterMethod(edit_Shipping);
				clickOn(edit_Shipping);
				String[] fields = { firstName_Shipping, lastName_Shipping, company_Shipping, country_Shipping,
						streetAddress_Shipping, streetAddressNextLine_Shipping, city_Shipping, state_Shipping,
						postCode_Shipping };
				if (isElementPresent(firstName_Shipping)) {
					for (int i = 0; i < fields.length; i++) {
						if (isElementDisplayed(fields[i])) {
							if (fields[i].contains("Country") || fields[i].contains("State")) {
								highLighterMethod(fields[i]);
								shippingDetails.put(fields[i], getText(fields[i]));
							} else {
								highLighterMethod(fields[i]);
								shippingDetails.put(fields[i], getAttributeValue(fields[i], "value"));
							}
						} else {
							testStepFailed(fields[i].split("#")[0] + " input field was not displayed");
						}
					}
				} else {
					testStepFailed("First Name field was not present");
				}
			} else {
				testStepPassed("The edit button for billing was not present");
			}
		} catch (Exception e) {
			testStepFailed("Saved Shipping Details could not be fetched successfully");
			e.printStackTrace();
		}
		return shippingDetails;

	}

	/**
	 * Description: Method is used verify whether the user is logged In
	 */

	public void verifyPresenceOfLogout() {

		try {
			if (isElementPresent(logoutButton)) {
				highLighterMethod(logoutButton);
				testStepInfo("User is logged In");
			} else {
				testStepFailed("User login verification successful");
			}
		} catch (Exception e) {
			testStepFailed("Logout presence could not be verified successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on Subscriptions.
	 */
	public void clickSubscriptions() {

		try {
			if (isElementPresent(subscriptions)) {
				highLighterMethod(subscriptions);
				clickOn(subscriptions);
			} else {
				testStepFailed("Subscriptions is not present");
			}
		} catch (Exception e) {
			testStepFailed("Subscriptions could not be clicked");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to access a specific order in the subscription
	 * page
	 */
	public void accessOrder(String option, String orderNumber, String statusCheck) {
		try {
			switch (option) {
			case "order presence confirmation": {
				String order = "Order #xpath=//a[contains(text(),'" + orderNumber + "')]";
				if (isElementDisplayed(order)) {
					testStepInfo("Order - " + orderNumber + " is present in Subscriptions");
					highLighterMethod(order);
				} else {
					testStepFailed("Order - " + orderNumber + " is not present in Subscriptions");
				}
				break;
			}

			case "status Check": {
				String order = " Order status #xpath=//a[contains(text(),'" + orderNumber
						+ "')]/../..//td[contains(@data-title,'Status')]";
				if (isElementDisplayed(order)) {
					String status = getText(order);
					if (status.toLowerCase().contains(statusCheck.toLowerCase())) {
						testStepInfo("Order - " + orderNumber + " has the expected status - " + status);
						highLighterMethod(order);
					} else {
						testStepFailed(
								"Order - " + orderNumber + " does not have the expected status - " + statusCheck);
					}
				} else {
					testStepFailed("Order - " + orderNumber + " is not present in Subscriptions");
				}
				break;
			}

			case "click View": {
				String order = "View Order #xpath=//a[contains(text(),'" + orderNumber
						+ "')]/../..//a[contains(text(),'View')]";
				if (isElementDisplayed(order)) {
					highLighterMethod(order);
					clickOn(order);
				} else {
					testStepFailed("Order - " + orderNumber + "could not viewed",
							"View button not displayed for the order");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Requested action in Subscriptions could not be performed");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on orders.
	 */
	public void clickOrders() {

		try {
			if (isElementPresent(orders)) {
				highLighterMethod(orders);
				clickOn(orders);
			} else {
				testStepFailed("Orders is not present");
			}
		} catch (Exception e) {
			testStepFailed("Orders could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to check presence of main order in orders.
	 */
	public void checkMainOrderinOrders(String orderNumber) {
		try {
			String order = "Order #xpath=//a[contains(text(),'" + orderNumber + "')]";
			if (isElementDisplayed(order)) {
				testStepInfo("Order - " + orderNumber + " is present in Orders");
				highLighterMethod(order);
			} else {
				testStepFailed("Order - " + orderNumber + " is not present in Orders");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify the presence of main order in orders");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on cancel for a subscription order.
	 */
	public void clickCancel_SubscriptionOrder() {

		try {
			if (isElementPresent(cancel_Subscription)) {
				highLighterMethod(cancel_Subscription);
				clickOn(cancel_Subscription);
			} else {
				testStepFailed("Cancel button is not present");
			}
		} catch (Exception e) {
			testStepFailed("Cancel button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify expected message is displayed in the Alert
	 * 
	 * @param: The messages whose presence are to be verified in the alert.
	 */
	public void verifyExpectedMessageAlert(String message) {
		try {
			waitForElementToDisplay(alertMessage, 30);
			if (isElementDisplayed(alertMessage)) {
				highLighterMethod(alertMessage);
				manualScreenshot("Alert message displayed");
				if (getText(alertMessage).contains(message))
					testStepInfo("Relevant Alert Message was displayed");
				else
					testStepFailed("Relevant Alert Message was not displayed");

			} else
				testStepFailed("No Alert message was displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate expected Messages");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on resubscribe for a subscription order.
	 */
	public void clickResubscribe_SubscriptionOrder() {

		try {
			if (isElementPresent(resubscribe_Subscription)) {
				highLighterMethod(resubscribe_Subscription);
				clickOn(resubscribe_Subscription);
			} else {
				testStepFailed("Resubscribe button is not present");
			}
		} catch (Exception e) {
			testStepFailed("Resubscribe button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on 'Account Details'.
	 */
	public void clickOnAccountDetails() {
		try {
			if (isElementPresent(accountDetails)) {
				highLighterMethod(accountDetails);
				clickOn(accountDetails);
			} else {
				testStepFailed("Account Details is not present");
			}
		} catch (Exception e) {
			testStepFailed("Account Details could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to check the display of empty field error
	 * messages.
	 */
	public void checkEmptyFieldErrorMessages() {
		try {
			if (isElementPresent(firstNameError)) {
				highLighterMethod(firstNameError);
			} else {
				testStepFailed("First Name Missing Error is not present");
			}
			if (isElementPresent(lastNameError)) {
				highLighterMethod(lastNameError);
			} else {
				testStepFailed("Last Name Missing Error is not present");
			}

			if (isElementPresent(displayNameError)) {
				highLighterMethod(displayNameError);
			} else {
				testStepFailed("Display Name Missing Error is not present");
			}

			if (isElementPresent(accountEmailError)) {
				highLighterMethod(accountEmailError);
			} else {
				testStepFailed("Account Email Missing Error is not present");
			}

		} catch (Exception e) {
			testStepFailed("Account Details could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to enter data in fields under Account Details.
	 */
	public void enterDatainAccountDetailsFields(String firstName, String lastName, String displayName, String email) {
		try {
			if (isElementDisplayed(firstNameMyAccount)) {
				highLighterMethod(firstNameMyAccount);
				typeIn(firstNameMyAccount, firstName);
			} else {
				testStepFailed("First Name field is not displayed");
			}
			if (isElementDisplayed(lastNameMyAccount)) {
				highLighterMethod(lastNameMyAccount);
				typeIn(lastNameMyAccount, lastName);
			} else {
				testStepFailed("Last Name field is not displayed");
			}

			if (isElementDisplayed(displayNameMyAccount)) {
				highLighterMethod(displayNameMyAccount);
				typeIn(displayNameMyAccount, displayName);
			} else {
				testStepFailed("Display Name field is not displayed");
			}

			if (isElementDisplayed(accountEmailMyAccount)) {
				highLighterMethod(accountEmailMyAccount);
				typeIn(accountEmailMyAccount, email);
			} else {
				testStepFailed("Account Email field is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Account Details could not be entered");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to check if account details are displayed.
	 */
	public void checkAccountDetailsDisplay() {
		try {
			if (isElementDisplayed(firstNameMyAccount) && isElementDisplayed(lastNameMyAccount)
					&& isElementDisplayed(displayNameMyAccount) && isElementDisplayed(accountEmailMyAccount)
					&& isElementDisplayed(currentPasswordMyAccount) && isElementDisplayed(newPasswordMyAccount)
					&& isElementDisplayed(confirmPasswordMyAccount)) {
				highLighterMethod(firstNameMyAccount);
				highLighterMethod(lastNameMyAccount);
				highLighterMethod(displayNameMyAccount);
				highLighterMethod(accountEmailMyAccount);
				highLighterMethod(currentPasswordMyAccount);
				highLighterMethod(newPasswordMyAccount);
				highLighterMethod(confirmPasswordMyAccount);
				testStepInfo("All account details fields are displayed");
			} else {
				testStepFailed("An account detail field is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Account Details display could not be checked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to check relevant message is displayed.
	 */
	public void checkMessageDisplayed(String message) {
		try {
			switch (message) {
//			case "invalidEmailError": {
//				if (isElementDisplayed(accountEmailInvalidError)) {
//					highLighterMethod(accountEmailInvalidError);
//					testStepInfo("The relevant error message was displayed for invalid email");
//				} else {
//					testStepFailed("The relevant error message was not displayed for invalid email");
//				}
//				break;
//			}
			case "emailAddressMissing": {
				if (isElementPresent(accountEmailError)) {
					highLighterMethod(accountEmailError);
				} else {
					testStepFailed("Account Email Missing Error is not present");
				}
				break;
			}

			case "fillAllPasswordfieldsError": {
				if (isElementDisplayed(fillAllPasswordFieldsError)) {
					highLighterMethod(fillAllPasswordFieldsError);
					testStepInfo("The relevant error message was displayed for all password fileds not being filled");
				} else {
					testStepFailed(
							"The relevant error message was not displayed for all password fileds not being filled");
				}
				break;
			}

			case "enterCurrentPasswordError": {
				if (isElementDisplayed(enterCurrentPasswordError)) {
					highLighterMethod(enterCurrentPasswordError);
					testStepInfo(
							"The relevant error message was displayed for current password field not being filled");
				} else {
					testStepFailed(
							"The relevant error message was not displayed for current password field not being filled");
				}
				break;
			}

			case "reEnterPasswordError": {
				highLighterMethod(reEnterPasswordError);
				if (isElementDisplayed(reEnterPasswordError)) {
					testStepInfo(
							"The relevant error message was displayed for re-enter password field not being filled");
				} else {
					testStepFailed(
							"The relevant error message was not displayed for re-enter password not being filled");
				}
				break;
			}
			case "passwordsDoNotMatch": {
				highLighterMethod(passwordsDoNotMatchError);
				if (isElementDisplayed(passwordsDoNotMatchError)) {
					testStepInfo("The relevant error message was displayed for passwords not matching");
				} else {
					testStepFailed("The relevant error message was not displayed for passwords not matching");
				}
				break;
			}

//			case "accountDetailsSaved": {
//				highLighterMethod(accountDetailsSavedMessage);
//				if (isElementDisplayed(accountDetailsSavedMessage)) {
//					testStepInfo("The relevant message was displayed for saving account details successfully");
//				} else {
//					testStepFailed("The relevant message was not displayed for saving account details successfully");
//				}
//				break;
//			}

			}
		} catch (Exception e) {
			testStepFailed("The relevant message display could not be checked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Save Changes.
	 */
	public void clickOnSaveChangesMyAccounts() {
		try {
			if (isElementDisplayed(saveChangesMyAccount)) {
				highLighterMethod(saveChangesMyAccount);
				clickOn(saveChangesMyAccount);
			} else {
				testStepFailed("Save Changes is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Save Changes could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to enter data in fields under Account Details for
	 * changing password.
	 */
	public void enterDatainPasswordFields(String currentPassword, String newPassword, String confirmPassword) {
		try {
			if (currentPassword != null) {
				if (isElementDisplayed(currentPasswordMyAccount)) {
					highLighterMethod(currentPasswordMyAccount);
					typeIn(currentPasswordMyAccount, currentPassword);
				} else {
					testStepFailed("Current Password field is not displayed");
				}
			}

			if (confirmPassword != null) {
				if (isElementDisplayed(confirmPasswordMyAccount)) {
					highLighterMethod(confirmPasswordMyAccount);
					typeIn(confirmPasswordMyAccount, confirmPassword);
				} else {
					testStepFailed("Confirm Password field is not displayed");
				}
			}

			if (newPassword != null) {
				if (isElementDisplayed(newPasswordMyAccount)) {
					highLighterMethod(newPasswordMyAccount);
					typeIn(newPasswordMyAccount, newPassword);
				} else {
					testStepFailed("New Password field is not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Password data could not be entered");
			e.printStackTrace();
		}
	}
}
