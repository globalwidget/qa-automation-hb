package pages;

import java.util.Set;

import org.openqa.selenium.Keys;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class HeaderAndFooters extends ApplicationKeywords {

	// Header
	private static final String home = "Home #id=menu-item-6";
	private static final String shop = "Shop #id=menu-item-8";
	private static final String learn = "Learn #id=menu-item-765";
	private static final String wholesale = "Wholesale #xpath=//a[contains(text(),'WHOLESALE')]";
	private static final String contact = "Contact #id=menu-item-12";
	private static final String rewards = "Rewards #xpath=//li[not(contains(@class,'menu'))]/a[contains(text(),'Rewards')]";
	private static final String delta8 = "Delta-8 #xpath=//a[contains(text(),'Delta-8')]";

	private static final String shop_AllProducts = "All Products #xpath=//a[contains(text(),'All Products')]";
	private static final String shop_NewProductReleases = "New Product Releases #xpath=//a[contains(text(),'New Product Releases')]";
	private static final String shop_CBDEdibles = "CBD Edibles #xpath=//ul[contains(@class,'onepress-menu')]//a[contains(text(),'CBD Edibles')]";
	private static final String shop_CBDEdibles_CBDGummies = "CBD Gummies #xpath=//a[contains(text(),'CBD Gummies')]";
	private static final String shop_CBDEdibles_CBDCapsules = "CBD Capsules #xpath=//a[contains(text(),'CBD Capsules')]";
//	private static final String shop_CBDEdibles_CBDSyrup = "CBD Syrup #xpath=//a[contains(text(),'CBD Syrup')]";
	private static final String shop_CBDEdibles_CBDLollipops = "CBD Lollipops #xpath=//a[contains(text(),'CBD Lollipops')]";
	private static final String shop_CBDEdibles_MaxChill = "Max Chill #xpath=//li/a[contains(text(),'Max Chill')]";
	private static final String shop_CBDoil = "CBD Oil #xpath=//li[contains(@id,'menu-item-8')]//a[contains(text(),'CBD Oil')]";
	private static final String shop_CBDvape = "CBD Vape #xpath=//ul[contains(@class,'onepress-menu')]//a[contains(text(),'CBD Vape')]";
	private static final String shop_CBDtopicals = "CBD Topicals #xpath=//ul[contains(@class,'onepress-menu')]//a[contains(text(),'CBD Topicals')]";
	private static final String shop_CBDtopicals_painFreeze = "Pain Freeze #xpath=//a[contains(text(),'CBD Pain Freeze')]";
	private static final String shop_CBDtopicals_CBDlotion = "CBD Lotion #xpath=//a[contains(text(),'Lotion')]";
	private static final String shop_CBDtopicals_CBDbubbleBombs = "CBD Bubble Bombs #xpath=//a[contains(text(),'CBD Bubble Bombs')]";
	private static final String shop_CBDtopicals_CBDheatReleiefSpray = "CBD Heat Relief Spray #xpath=//a[contains(text(),'CBD Heat Relief Spray')]";
	private static final String shop_CBDtopicals_CBDpatches = "CBD Patches #xpath=//a[contains(text(),'CBD Patches')]";
	private static final String shop_CBDtopicals_CBDessentialOils = "CBD Essential Oils #xpath=//a[contains(text(),'CBD Essential Oils')]";
	private static final String shop_CBDtopicals_LipBalm = "CBD Lip Balm #xpath=//a[contains(text(),'CBD Lip Balm')]";
	private static final String shop_CBDtopicals_CBDbeardProducts = "CBD Beard Products #xpath=//a[contains(text(),'CBD Beard Products')]";
	private static final String shop_CBDtopicals_CBDtattooOinment = "CBD tattoo Oinment #xpath=//a[contains(text(),'CBD Tattoo Ointment')]";

	private static final String shop_CBDbundles = "CBD Bundles #xpath=//a[contains(text(),'CBD Bundles')]";
	private static final String shop_CBDpetProdcuts = "Pet CBD Products #xpath=//ul[contains(@class,'onepress-menu')]//a[contains(text(),'CBD Pet Products')]";
	private static final String shop_CBDpetProdcuts_CBDpetoils = "Pet CBD Oils #xpath=//a[contains(text(),'CBD Pet Oils')]";
	private static final String shop_CBDpetProdcuts_CBDdogTreats = "Pet CBD Dog Treats #xpath=//a[contains(text(),'CBD Dog Treats')]";
//	private static final String shop_CBDpetProdcuts_CBDshampoo = "Pet CBD Shampoo #xpath=//li/a[contains(text(),'Shampoo')]";
	private static final String shop_CBDpetProdcuts_CBDpawButter = "CBD Paw Butter for Pets #xpath=//a[contains(text(),'CBD Paw Butter')]";
	private static final String shop_apparel = "Apparel #xpath=//ul[contains(@class,'onepress-menu')]//a[contains(text(),'Apparel')]";
	private static final String shop_Subscriptions = "Subscriptions #xpath=//a[contains(text(),'Subscriptions')]";
	private static final String learn_Blog = "Blog #xpath=//li[@id='menu-item-11']";
	private static final String learn_OurStory = "Our Story #xpath=//a[contains(text(),'Our Story')]";
	private static final String learn_CBDfacts = "CBD Facts #xpath=//a[contains(text(),'CBD Facts')]";
	private static final String learn_benefitsOfCBDoil = "Benefits of CBD Oil #xpath=//a[contains(text(),'Benefits')]";
	private static final String learn_InTheNews = "In the News #xpath=//a[contains(text(),'In the News')]";
	private static final String learn_thirdPartyLabTesting = "Third Party Lab Testing #xpath=//a[contains(text(),'Third Party Lab Testing')]";
	private static final String learn_Faq = "Faq #xpath=//a[contains(text(),'FAQ')]";
	private static final String learn_ProductFinderQuiz = "Product Finder Quiz #xpath=//a[contains(text(),'Product Finder Quiz')]";
	private static final String learn_Rewards = "Learn - Rewards #xpath=//ul[contains(@class,'sub')]//a[contains(text(),'Rewards')]";
	private static final String wholesale_BecomeDistributor = "Become Distributor #xpath=//a[contains(text(),'Become a Distributor')]";
	private static final String wholesale_ProductLiability = "_Product Liability #xpath=//a[contains(text(),'Product Liability')]";
	private static final String wholesale_LegalSummary = "Legal Summary #xpath=//a[contains(text(),'Legal Summary')]";
	private static final String wholesale_DistributorTerms = "Distributor Terms #xpath=//a[contains(text(),'Distributor Terms & Conditions')]";
	private static final String wholesale_DistributorBlog = "Distributor Blog #xpath=//a[contains(text(),'Distributors Blog')]";
	private static final String emailUs = "Email Us #xpath=//li/a[contains(@href,'contact')]/i";
	private static final String callUs = "Call Us #xpath=//span[contains(text(),'Call Us')]/..";
	private static final String privacy = "Privacy #xpath=//a[contains(@href,'privacy')]";
	private static final String account = "My Account #xpath=//a[contains(@href,'account')]";
	private static final String myAccount = "Account #xpath=//ul[@class='user-menu']//a[contains(@href,'my-account')]";
	private static final String myAccountHeader = "My Account Header #xpath=//div[contains(text(),'My Account')]";
	private static final String cart = "Cart #xpath=//a[contains(@href,'cart')]";

	private static final String contactUsHeader = "Contact Us Header #xpath=//h1[contains(text(),'Contact Us')]";
	private static final String privacyPolicyHeader = "Privacy Policy Header #xpath=//strong[contains(text(),'Privacy Policy')]";
	private static final String myCheckoutHeader = "My Checkout Header #xpath=//div[contains(text(),'Checkout')]";
	private static final String MyCartHeader = "My Cart Header #xpath=//div[contains(text(),'My Cart')]";
	private static final String emptyCartMessage = "Empty Cart Message #xpath=//p[contains(text(),'Your cart is currently empty.')]";
	private static final String signUp_HomePage = "Home Banner Title #xpath=//h4[contains(text(),'SIGN UP TODAY & SAVE')]";
	private static final String allProductsHeader = "All Products Header #xpath=//h1[contains(text(),'CBD PRODUCTS')]";
	private static final String newProductReleasesHeader = "New CBD Products Header #xpath=//h1[contains(text(),'New CBD Products')]";
	private static final String cbdEdiblesHeader = "CBD Edibles Header #xpath=//h1[contains(text(),'CBD Edibles')]";
	private static final String cbdGummiesHeader = "CBD Gummies Header #xpath=//h1[contains(text(),'CBD Gummies')]";
	private static final String cbdCapsulesHeader = "CBD Capsules Header #xpath=//h1[contains(text(),'CBD Capsules')]";
	private static final String cbdSyrupsHeader = "CBD Syrup Header #xpath=//h1[contains(text(),'CBD Syrup')]";
	private static final String cbdLollipopsHeader = "CBD Lollipops Header #xpath=//h1[contains(text(),'CBD LOLLIPOPS')]";
	private static final String maxChillHeader = "Max Chill Header #xpath=//div/h1[contains(text(),'Shot')]";
	private static final String cbdOilHeader = "CBD Oil Header #xpath=//h1[contains(text(),'CBD Oil')]";
	private static final String cbdVapeHeader = "CBD Vape Header #xpath=//h1[contains(text(),'CBD Vape Products')]";
	private static final String cbdtopicalsHeader = "CBD Topicals Header #xpath=//h1[contains(text(),'CBD Topical')]";
	private static final String cbdPainFreezeHeader = "CBD Pain Freeze Header #xpath=//h1[contains(text(),'CBD Pain Freeze')]";
	private static final String cbdLotionHeader = "CBD Lotion Header #xpath=//h1[contains(text(),'Lotion')]";
	private static final String cbdBubbleBombsHeader = "CBD Bubble Bombs Header #xpath=//h1[contains(text(),'CBD Bath Bombs')]";
	private static final String cbdHeatReliefSprayHeader = "CBD Heat Relief Spray Header #xpath=//h1[contains(text(),'Spray')]";
	private static final String cbdPatchesHeader = "CBD Patches Header #xpath=//h1[contains(text(),'CBD Patches')]";
	private static final String cbdEssentialOilHeader = "CBD Essential Oil Header #xpath=//h1[contains(text(),'CBD Essential Oil')]";
	private static final String cbdLipBalmHeader = "CBD Lip Balm Header #xpath=//h1[contains(text(),'CBD Lip Balm')]";
	private static final String cbdBeardProductsHeader = "CBD Beard Products Header #xpath=//h1[contains(text(),'CBD Beard Oil')]";
	private static final String cbdTattooOinmentHeader = "CBD Tattoo Oinment Header #xpath=//h1[contains(text(),'CBD Tattoo Ointment')]";
	private static final String cbdBundlesHeader = "CBD Bundles Header #xpath=//h1[contains(text(),'CERTIFIED PREMIUM CBD BUNDLES')]";
	private static final String petCBDproductsHeader = "Pet CBD Products Header #xpath=//div/h1[contains(text(),'CBD for Pets')]";
	private static final String petCBDoilHeader = "Pet CBD Oil Header #xpath=//h1[contains(text(),'CBD Oil for Pets')]";
	private static final String petCBDdogtreatsHeader = "Pet CBD Dog Treats Header #xpath=//h1[contains(text(),'CBD Dog Treats')]";
	private static final String petCBDshampooHeader = "CBD Shampoo for Pets Header #xpath=//h1[contains(text(),'Shampoo')]";
	private static final String petCBDpawButterHeader = "CBD Paw Butter for Pets Header #xpath=//div/h1[contains(text(),'Paw Butter')]";
	private static final String apparelHeader = "CBD Apparel Header #xpath=//h1[contains(text(),'CBD APPAREL')]";
	private static final String subscriptionsHeader = "Subscriptions Header #xpath=//h1[contains(text(),'Subscription')]";
	private static final String blogHeader = "Blog Header #xpath=//h1[contains(text(),'CBD Blog')]";
	private static final String ourStoryHeader = "Our Story Header #xpath=//h1[contains(text(),'OUR STORY')]";
	private static final String cbdFactsHeader = "CBD Facts Header #xpath=//h1[contains(text(),'CBD FACTS')]";
	private static final String benefitsOfCBDoilHeader = "Benefits of CBD Oil Header #xpath=//h1[contains(text(),'CBD OIL BENEFITS')]";
	private static final String intheNewsHeader = "In the News Header #xpath=//h1[contains(text(),'In the News')]";
	private static final String labtestingResultsHeader = "Lab Testing Results Header #xpath=//h1[contains(text(),'CBD Lab Testing Results')]";
	private static final String faqHeader = "FAQ Header #xpath=//h1[contains(text(),'FREQUENTLY ASKED QUESTIONS')]";
	private static final String productFinderQuizHeader = "Product Finder Quiz Header #xpath=//h1[contains(text(),'CBD Product Finder Quiz')]";
	private static final String becomeDistributorHeader = "Become Deistributor Header #xpath=//div/h1[contains(text(),'CBD Wholesale')]";
	private static final String productLiabilityHeader = "Product Liability Header #xpath=//div[contains(text(),'CBD Legality for Distributors | Hemp Bombs')]";
	private static final String legalSummaryHeader = "Legal Summary Header #xpath=//div[contains(text(),'Legal Summary - Selling CBD Products | Hemp Bombs')]";
	private static final String distributorTermsHeader = "Distributor Terms Header #xpath=//h1[contains(text(),'Wholesale Terms and Conditions')]";
	private static final String distributorBlogHeader = "Distributor Blog Header #xpath=//h1[contains(text(),'Distributors Blog')]";
	private static final String rewardsHeader = "Rewards Header #xpath=//h1[contains(text(),'CBD Rewards')]";

	// Footer
	private static final String allCBDProduct = "All CBD Product #xpath=//a[contains(text(),'All CBD Product')]";
	private static final String cbdEdibles = "CBD Edibles #xpath=//div[@id='link-col']/a[contains(text(),'CBD Edibles')]";
//	private static final String cbdVape = "CBD Vape #xpath=//div[@id='link-col']/a[contains(text(),'CBD Vape')]";
	private static final String cbdPetProducts = "CBD Pet Products #xpath=//footer//a[contains(text(),'CBD Pet Products')]";
	private static final String cbdOils = "CBD Oils #xpath=//footer//a[contains(text(),'CBD Oil')]";
	private static final String cbdTopicals = "CBD Topicals #xpath=//div[@id='link-col']/a[contains(text(),'CBD Topicals')]";
	private static final String hempBombsApparel = "HempBombs Apparel #xpath=//a[contains(text(),'HempBombs Apparel')]";
	private static final String facebook = "Facebook #xpath=//a[@title='Facebook']/i";
	private static final String instagram = "Instagram #xpath=//a[@title='Instagram']/i";
	private static final String youtube = "Youtube #xpath=//a[@title='YouTube']/i";
	private static final String twitter = "Twitter #xpath=//a[@title='Twitter']/i";
	private static final String homeFooter = "Footer Home #xpath=//div[@id='bottom-nav']/a[contains(text(),'Home')]";
	private static final String distributors_storeOwners = "Distributors/Store Owners #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'Distributors / Store Owners')]";
	private static final String affiliates = "Affiliates #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'Affiliates')]";
	private static final String seniorDiscount = "Senior Discount #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'Senior Discount')]";
	private static final String veteran_activeDuty = "Veteran/Active Duty Program #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'Veteran / Active Duty Program')]";
	private static final String firstResponders = "First Responders #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'First Responders')]";
	private static final String contactFooter = "Footer Contact #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'Contact')]";
	private static final String siteMap = "SiteMap #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'Sitemap')]";
	private static final String ccpa = "CCPA #xpath=//div[contains(@id,'bottom')]//a[contains(text(),'CCPA')]";
//	private static final String distributors_storeOwnersHeader = "Distributors/Store Owners Header #xpath=//h1[contains(text(),'GROW YOUR REVENUE AS A CBD DISTRIBUTOR')]";
	private static final String affiliateHeader = "Affiliate Header #xpath=//div/h1[contains(text(),'Affiliate')]";
	private static final String seniorCitizenDiscountHeader = "Senior Citizen Discount Header #xpath=//div/h1[contains(text(),'SENIOR')]";
	private static final String militaryAndVeteranDiscountHeader = "Military and Veteran Discount Header #xpath=//h1[contains(text(),'Veterans')]";
	private static final String firstRespondersDiscountHeader = "First Reponders Discount Header #xpath=//h1[contains(text(),'First Responders')]";
	private static final String siteMapHeader = "SiteMap Header Header #xpath=//h1[contains(text(),'XML Sitemap')]";
	private static final String ccpaHeader = "CCPA Header Header #xpath=//h1[contains(text(),'CCPA Data Request')]";

	// Search
	private static final String searchBox = "Search #xpath=//input[contains(@id,'search')]";
	private static final String noSearchResults_Message_SearchPage = "No Search Results Message #xpath=//div//p[contains(text(),'No products were found matching your selection')]";
	private static final String minimumQueryLength_Message_SearchPage = "Minimum Query Length Message #xpath=//div[contains(text(),'Minimum Search query length is 3')]";
	private static final String numberOfItems_SearchPage = "Number of items in Search Page #xpath=//main//p[contains(@class,'result-count')]";
	private static final String gummiesSearchProductTitle_SearchPage = "Gummies Search Product Title #xpath=//li//h2[contains(text(),'Gummies')]";

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to click on Email Us
	 */
	public void clickEmailUs() {

		try {
			if (isElementPresent(emailUs)) {
				highLighterMethod(emailUs);
				clickOn(emailUs);
			} else {
				testStepFailed("Email Us not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Email Us could not be clicked");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method to verify presence of Call Us
	 */
	public void Verifypresence_CallUs(String phoneNumber) {

		try {
			if (isElementPresent(callUs) && findWebElement(callUs).isEnabled()) {
				highLighterMethod(callUs);
				if (getText(callUs).equalsIgnoreCase(phoneNumber))
					testStepInfo("The Number present in Call Us is : " + phoneNumber);
				else
					testStepFailed("The Number present in Call Us is not : " + phoneNumber);
			} else {
				testStepFailed("Call Us not present or not enabled in Header");
			}
		} catch (Exception e) {
			testStepFailed("Call Us could not be verified");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method to verify the function of privacy policy
	 */
	public void Verify_PrivacyPolicy() {
		try {
			if (isElementPresent(privacy)) {
				highLighterMethod(privacy);
				clickOn(privacy);
				if (isElementDisplayed(privacyPolicyHeader)) {
					highLighterMethod(privacyPolicyHeader);
					testStepInfo("The privacy policy page opened successfully");
				} else
					testStepFailed("The privacy policy page could not be opened");
			} else {
				testStepFailed("Privacy not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Privacy policy could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is to click on Account
	 */
	public void clickAccount() {
		try {
			if (isElementDisplayed(account)) {
				highLighterMethod(account);
				clickOn(account);
			} else {
				testStepFailed("Could not click on the account button in the Header", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Account could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on My cart
	 */
	public void clickOnMyCart() {

		try {
			if (isElementPresent(cart)) {
				highLighterMethod(cart);
				clickOn(cart);
			} else {
				testStepFailed("My Cart not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Cart could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on home.
	 */
	public void goTo_Home() {

		try {
			if (isElementPresent(home)) {
				highLighterMethod(home);
				clickOn(home);
			} else {
				testStepFailed("Home not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Home could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on desired option from Shop menu
	 */
	public void goTo_Shop(int subMenu, int subMenu_2) {

		try {
			if (subMenu == 0) {
				if (isElementPresent(shop)) {
					highLighterMethod(shop);
					clickOn(shop);
				} else {
					testStepFailed("Shop not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(shop_AllProducts)) {
						navigateMenu_Two(shop, shop_AllProducts);
					} else {
						testStepFailed("All Products not present under Shop");
					}

					break;
				}

				case 2: {
					if (isElementPresent(shop_NewProductReleases)) {
						navigateMenu_Two(shop, shop_NewProductReleases);
					} else {
						testStepFailed("New Product Releases not present under Shop");
					}
					break;
				}

				case 3: {
					if (subMenu_2 == 0) {
						if (isElementPresent(shop_CBDEdibles)) {
							navigateMenu_Two(shop, shop_CBDEdibles);
						} else {
							testStepFailed("CBD Edibles not present under Shop Menu");
						}
						break;
					} else {
						switch (subMenu_2) {
						case 1: {
							if (isElementPresent(shop_CBDEdibles_CBDGummies)) {
								navigateMenu_Three(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies);
							} else {
								testStepFailed("CBD Gummies not present under CBD Edibles");
							}
							break;
						}

						case 2: {
							if (isElementPresent(shop_CBDEdibles_CBDCapsules)) {
								navigateMenu_Four(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies,
										shop_CBDEdibles_CBDCapsules);
							} else {
								testStepFailed("CBD Capsules not present under CBD Edibles");
							}
							break;
						}

//						case 3: {
//							if (isElementPresent(shop_CBDEdibles_CBDSyrup)) {
//								navigateMenu_Four(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies,
//										shop_CBDEdibles_CBDSyrup);
//							} else {
//								testStepFailed("CBD Syrup not present under CBD Edibles");
//							}
//							break;
//						}

						case 3: {
							if (isElementPresent(shop_CBDEdibles_CBDLollipops)) {
								navigateMenu_Four(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies,
										shop_CBDEdibles_CBDLollipops);
							} else {
								testStepFailed("CBD Lollipops not present under CBD Edibles");
							}
							break;
						}

						case 4: {
							if (isElementPresent(shop_CBDEdibles_MaxChill)) {
								navigateMenu_Four(shop, shop_CBDEdibles, shop_CBDEdibles_CBDGummies,
										shop_CBDEdibles_MaxChill);
							} else {
								testStepFailed("Max Chill not present under CBD Edibles");
							}
							break;
						}
						}
					}
					break;
				}

				case 4: {
					if (isElementPresent(shop_CBDoil)) {
						navigateMenu_Two(shop, shop_CBDoil);
					} else {
						testStepFailed("CBD Oil not present under Shop Menu");
					}
					break;
				}

				case 5: {
					if (isElementPresent(shop_CBDvape)) {
						navigateMenu_Two(shop, shop_CBDvape);
					} else {
						testStepFailed("CBD Vape not present under Shop Menu");
					}
					break;
				}

				case 6: {
					if (subMenu_2 == 0) {
						if (isElementPresent(shop_CBDtopicals)) {
							navigateMenu_Two(shop, shop_CBDtopicals);
						} else {
							testStepFailed("CBD Topicals not present under Shop Menu");
						}
						break;
					} else {
						switch (subMenu_2) {
						case 1: {
							if (isElementPresent(shop_CBDtopicals_painFreeze)) {
								navigateMenu_Three(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze);
							} else {
								testStepFailed("Pain Freeze not present under CBD Topicals");
							}
							break;
						}

						case 2: {
							if (isElementPresent(shop_CBDtopicals_CBDlotion)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_CBDlotion);
							} else {
								testStepFailed("CBD Lotion not present under CBD Topicals");
							}
							break;
						}

						case 3: {
							if (isElementPresent(shop_CBDtopicals_CBDbubbleBombs)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_CBDbubbleBombs);
							} else {
								testStepFailed("CBD Bubble Bombs not present under CBD Topicals");
							}
							break;
						}

						case 4: {
							if (isElementPresent(shop_CBDtopicals_CBDheatReleiefSpray)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_CBDheatReleiefSpray);
							} else {
								testStepFailed("CBD Heat Relief not present under CBD Topicals");
							}
							break;
						}

						case 5: {
							if (isElementPresent(shop_CBDtopicals_CBDpatches)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_CBDpatches);
							} else {
								testStepFailed("Patches not present under CBD Topicals");
							}
							break;
						}

						case 6: {
							if (isElementPresent(shop_CBDtopicals_CBDessentialOils)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_CBDessentialOils);
							} else {
								testStepFailed("Essential Oils not present under CBD Topicals");
							}
							break;
						}

						case 7: {
							if (isElementPresent(shop_CBDtopicals_LipBalm)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_LipBalm);
							} else {
								testStepFailed("Lip Balm not present under CBD Topicals");
							}
							break;
						}

						case 8: {
							if (isElementPresent(shop_CBDtopicals_CBDbeardProducts)) {
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_CBDbeardProducts);
							} else {
								testStepFailed("Beard Products not present under CBD Topicals");
							}
							break;
						}

						case 9: {
							if (isElementPresent(shop_CBDtopicals_CBDtattooOinment)) {
								scrollToViewElement(shop);
								navigateMenu_Four(shop, shop_CBDtopicals, shop_CBDtopicals_painFreeze,
										shop_CBDtopicals_CBDtattooOinment);
							} else {
								testStepFailed("Tattoo Oinment not present under CBD Topicals");
							}
							break;
						}
						}
					}
					break;
				}

				case 7: {
					if (isElementPresent(shop_CBDbundles)) {
						navigateMenu_Two(shop, shop_CBDbundles);
					} else {
						testStepFailed("CBD Bundles not present under Shop Menu");
					}
					break;
				}

				case 8: {
					if (subMenu_2 == 0) {
						scrollToViewElement(shop);
						if (isElementPresent(shop_CBDpetProdcuts)) {
							navigateMenu_Two(shop, shop_CBDpetProdcuts);
						} else {
							testStepFailed("CBD Pet Products not present under Shop Menu");
						}
						break;
					} else {
						switch (subMenu_2) {
						case 1: {
							scrollToViewElement(shop);
							if (isElementPresent(shop_CBDpetProdcuts_CBDpetoils)) {
								navigateMenu_Three(shop, shop_CBDpetProdcuts, shop_CBDpetProdcuts_CBDpetoils);
							} else {
								testStepFailed("CBD Pet Oils not present under CBD Pet Products");
							}
							break;
						}

						case 2: {
							scrollToViewElement(shop);
							if (isElementPresent(shop_CBDpetProdcuts_CBDdogTreats)) {
								navigateMenu_Three(shop, shop_CBDpetProdcuts, shop_CBDpetProdcuts_CBDdogTreats);
							} else {
								testStepFailed("CBD Dog Treats not present under CBD Pet Products");
							}
							break;
						}

//						case 3: {
//							if (isElementPresent(shop_CBDpetProdcuts_CBDshampoo)) {
//								scrollToViewElement(shop);
//								navigateMenu_Three(shop, shop_CBDpetProdcuts, shop_CBDpetProdcuts_CBDshampoo);
//							} else {
//								testStepFailed("CBD Shampoo not present under CBD Pet Products");
//							}
//							break;
//						}

						case 4: {
							scrollToViewElement(shop);
							if (isElementPresent(shop_CBDpetProdcuts_CBDpawButter)) {
								navigateMenu_Three(shop, shop_CBDpetProdcuts, shop_CBDpetProdcuts_CBDpawButter);
							} else {
								testStepFailed("CBD Paw Butter not present under CBD Pet Products");
							}
							break;
						}

						}
					}
					break;
				}

				case 9: {
					if (isElementPresent(shop_apparel)) {
						navigateMenu_Two(shop, shop_apparel);
					} else {
						testStepFailed("Apparel not present under Shop Menu");
					}
					break;
				}
				case 10: {
					if (isElementPresent(shop_Subscriptions)) {
						navigateMenu_Two(shop, shop_Subscriptions);
					} else {
						testStepFailed("Subscriptions not present under Shop Menu");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the menu could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on desired option from Learn menu
	 */
	public void goTo_Learn(int subMenu) {
		try {
			if (subMenu == 0) {
				if (isElementPresent(learn)) {
					highLighterMethod(learn);
					clickOn(learn);
				} else {
					testStepFailed("Learn not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(learn_ProductFinderQuiz)) {
						navigateMenu_Two(learn, learn_ProductFinderQuiz);
					} else {
						testStepFailed("Product Finder Quiz not present under Learn Menu");
					}
					break;
				}

				case 2: {
					if (isElementPresent(learn_Blog)) {
						navigateMenu_Two(learn, learn_Blog);
					} else {
						testStepFailed("Blog not present under Learn Menu");
					}
					break;
				}

				case 3: {
					if (isElementPresent(learn_OurStory)) {
						navigateMenu_Two(learn, learn_OurStory);
					} else {
						testStepFailed("Our Story not present under Learn Menu");
					}
					break;
				}
				case 4: {
					if (isElementPresent(learn_CBDfacts)) {
						navigateMenu_Two(learn, learn_CBDfacts);
					} else {
						testStepFailed("Learn More about CBD not present under Learn Menu");
					}
					break;
				}

				case 5: {
					if (isElementPresent(learn_benefitsOfCBDoil)) {
						navigateMenu_Two(learn, learn_benefitsOfCBDoil);
					} else {
						testStepFailed("Benefits of CBD Oil not present under Learn Menu");
					}
					break;
				}

				case 6: {
					if (isElementPresent(learn_InTheNews)) {
						navigateMenu_Two(learn, learn_InTheNews);
					} else {
						testStepFailed("In the News not present under Learn Menu");
					}
					break;
				}

				case 7: {
					if (isElementPresent(learn_thirdPartyLabTesting)) {
						navigateMenu_Two(learn, learn_thirdPartyLabTesting);
					} else {
						testStepFailed("Third Part Lab Testing not present under Learn Menu");
					}
					break;
				}
				case 8: {
					if (isElementPresent(learn_Faq)) {
						navigateMenu_Two(learn, learn_Faq);
					} else {
						testStepFailed("FAQ not present under Learn Menu");
					}
					break;
				}
				case 9: {
					if (isElementPresent(learn_Rewards)) {
						navigateMenu_Two(learn, learn_Rewards);
					} else {
						testStepFailed("Rewards not present under Learn Menu");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the menu could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on desired option from Wholesale menu
	 */
	public void goTo_Wholesale(int subMenu) {
		try {
			if (subMenu == 0) {
				if (isElementPresent(wholesale)) {
					highLighterMethod(wholesale);
					clickOn(wholesale);
				} else {
					testStepFailed("Wholesale not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(wholesale_BecomeDistributor)) {
						navigateMenu_Two(wholesale, wholesale_BecomeDistributor);
					} else {
						testStepFailed("Become Distributor not present under Wholesale Menu");
					}
					break;
				}

				case 2: {
					if (isElementPresent(wholesale_ProductLiability)) {
						navigateMenu_Two(wholesale, wholesale_ProductLiability);
					} else {
						testStepFailed("Product Liability not present under Wholesale Menu");
					}
					break;
				}

				case 3: {
					if (isElementPresent(wholesale_LegalSummary)) {
						navigateMenu_Two(wholesale, wholesale_LegalSummary);
					} else {
						testStepFailed("Legal Summary not present under Wholesale Menu");
					}
					break;
				}

				case 4: {
					if (isElementPresent(wholesale_DistributorTerms)) {
						navigateMenu_Two(wholesale, wholesale_DistributorTerms);
					} else {
						testStepFailed("Distributor Terms present under Wholesale Menu");
					}
					break;
				}

				case 5: {
					if (isElementPresent(wholesale_DistributorBlog)) {
						navigateMenu_Two(wholesale, wholesale_DistributorBlog);
					} else {
						testStepFailed("Distributor Blog present under Wholesale Menu");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the menu could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on contact
	 */
	public void goTo_Contact() {

		try {
			if (isElementPresent(contact)) {
				highLighterMethod(contact);
				clickOn(contact);
			} else {
				testStepFailed("Contact not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Contact could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on rewards
	 */
	public void goTo_Rewards() {

		try {
			if (isElementPresent(rewards)) {
				highLighterMethod(rewards);
				clickOn(rewards);
			} else {
				testStepFailed("Rewards not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Rewards could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Delta-8 link
	 */
	public void goTo_delta8() {

		try {
			if (isElementDisplayed(delta8)) {
				highLighterMethod(delta8);
				clickOn(delta8);
			} else {
				testStepFailed("Delta-8 not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Delta-8 could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to verify the routing.
	 */
	public void verifyNavigation(String option) {

		try {
			switch (option) {
			case "emailUs": {
				if (isElementDisplayed(contactUsHeader)) {
					highLighterMethod(contactUsHeader);
					testStepInfo("The relevant page was opened upon clicking Email in Header");
				} else {
					testStepFailed("Contact Us Title was not present");
				}
				break;
			}

			case "cart": {
				if (isElementDisplayed(MyCartHeader)) {
					highLighterMethod(MyCartHeader);
					testStepInfo("The relevant page was opened upon clicking My Cart in Header");
				} else {
					testStepFailed("My Cart Title was not present");
				}
				break;
			}

			case "emptyCart": {
				if (isElementDisplayed(emptyCartMessage)) {
					highLighterMethod(emptyCartMessage);
					testStepInfo("Empty Cart message was displayed");
				} else {
					testStepFailed("Empty Cart message was not displayed");
				}
				break;
			}

			case "checkout": {
				if (isElementDisplayed(myCheckoutHeader)) {
					testStepInfo("The relevant page was opened upon clicking Checkout in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Checkout in Header");
				}
				break;
			}

			case "homeBanner": {
				scrollToViewElement(signUp_HomePage);
				if (isElementDisplayed(signUp_HomePage)) {
					scrollToViewElement(signUp_HomePage);
					highLighterMethod(signUp_HomePage);
					testStepInfo("The relevant page was opened upon clicking Home in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Home in Header");
				}
				break;
			}

			case "myAccountHeader": {
				if (isElementDisplayed(myAccountHeader)) {
					highLighterMethod(myAccountHeader);
					testStepInfo("My Account Header was displayed");
				} else {
					testStepFailed("My Account Header was not displayed");
				}
				break;
			}

			case "shop": {
				if (isElementDisplayed(allProductsHeader)) {
					highLighterMethod(allProductsHeader);
					testStepInfo("The relevant page was opened upon clicking Shop in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Shop in Header");
				}
				break;
			}

			case "AllProducts": {
				if (isElementDisplayed(allProductsHeader)) {
					highLighterMethod(allProductsHeader);
					testStepInfo("The relevant page was opened upon clicking Shop in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Shop in Header");
				}
				break;
			}

			case "NewProductReleases": {
				if (isElementDisplayed(newProductReleasesHeader)) {
					highLighterMethod(newProductReleasesHeader);
					testStepInfo("The relevant page was opened upon clicking NewProductReleases under Shop");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Shop in Header");
				}
				break;
			}

			case "cbdEdibles": {
				if (isElementDisplayed(cbdEdiblesHeader)) {
					highLighterMethod(cbdEdiblesHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Edibles under Shop");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Edibles under Shop");
				}
				break;
			}

			case "Gummies": {
				if (isElementDisplayed(cbdGummiesHeader)) {
					highLighterMethod(cbdGummiesHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Gummies under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Gummies under CBD Edibles in Shop Menu");
				}
				break;
			}

			case "Capsules": {
				if (isElementDisplayed(cbdCapsulesHeader)) {
					highLighterMethod(cbdCapsulesHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Capsules under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Capsules under CBD Edibles in Shop Menu");
				}
				break;
			}

			case "Syrups": {
				if (isElementDisplayed(cbdSyrupsHeader)) {
					highLighterMethod(cbdSyrupsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Syrup under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Syrup under CBD Edibles in Shop Menu");
				}
				break;
			}

			case "Lollipops": {
				if (isElementDisplayed(cbdLollipopsHeader)) {
					highLighterMethod(cbdLollipopsHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Lollipops under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Lollipops under CBD Edibles in Shop Menu");
				}
				break;
			}

			case "maxChill": {
				if (isElementDisplayed(maxChillHeader)) {
					highLighterMethod(maxChillHeader);
					testStepInfo(
							"The relevant page was opened upon clicking Max Chill Shot under CBD Edibles in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Max Chill Shot under CBD Edibles in Shop Menu");
				}
				break;
			}

			case "cbdOil": {
				if (isElementDisplayed(cbdOilHeader)) {
					highLighterMethod(cbdOilHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Oil in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Oil in Shop Menu");
				}
				break;
			}

			case "cbdVape": {
				if (isElementDisplayed(cbdVapeHeader)) {
					highLighterMethod(cbdVapeHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Vape in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Vape in Shop Menu");
				}
				break;
			}

			case "cbdTopicals": {
				if (isElementDisplayed(cbdtopicalsHeader)) {
					highLighterMethod(cbdtopicalsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Topicals in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdPainFreeze": {
				if (isElementDisplayed(cbdPainFreezeHeader)) {
					highLighterMethod(cbdPainFreezeHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Pain Freeze under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Pain Freeze under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdLotion": {
				if (isElementDisplayed(cbdLotionHeader)) {
					highLighterMethod(cbdLotionHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Lotion under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Lotion under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdBubbleBombs": {
				if (isElementDisplayed(cbdBubbleBombsHeader)) {
					highLighterMethod(cbdBubbleBombsHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Bubble Bombs under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Bubble Bombs under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdHeatReliefSpray": {
				if (isElementDisplayed(cbdHeatReliefSprayHeader)) {
					highLighterMethod(cbdHeatReliefSprayHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Heat Relief Spray under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Heat Relief Spray under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdPatches": {
				if (isElementDisplayed(cbdPatchesHeader)) {
					highLighterMethod(cbdPatchesHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Patches under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Patches under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdEssentialOil": {
				if (isElementDisplayed(cbdEssentialOilHeader)) {
					highLighterMethod(cbdEssentialOilHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Essential Oil under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Essential Oil under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdLipBalm": {
				if (isElementDisplayed(cbdLipBalmHeader)) {
					highLighterMethod(cbdLipBalmHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Lip Balm under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Lip Balm under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdBeardProducts": {
				if (isElementDisplayed(cbdBeardProductsHeader)) {
					highLighterMethod(cbdBeardProductsHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Beard Products under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Beard Products under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdTattooOinment": {
				waitForElementToDisplay(cbdTattooOinmentHeader, 30);
				if (isElementDisplayed(cbdTattooOinmentHeader)) {
					highLighterMethod(cbdTattooOinmentHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Tattoo Oinment under CBD Topicals in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was opened upon clicking CBD Tattoo Oinment under CBD Topicals in Shop Menu");
				}
				break;
			}

			case "cbdBundles": {
				if (isElementDisplayed(cbdBundlesHeader)) {
					highLighterMethod(cbdBundlesHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Bundles in Shop Menu");
				} else {
					testStepFailed("The relevant page was opened upon clicking CBD Bundles in Shop Menu");
				}
				break;
			}

			case "petCBDproducts": {
				if (isElementDisplayed(petCBDproductsHeader)) {
					highLighterMethod(petCBDproductsHeader);
					testStepInfo("The relevant page was opened upon clicking Pet CBD Products in Shop Menu");
				} else {
					testStepFailed("The relevant page was opened upon clicking Pet CBD Products in Shop Menu");
				}
				break;
			}

			case "petCBDoil": {
				if (isElementDisplayed(petCBDoilHeader)) {
					highLighterMethod(petCBDoilHeader);
					testStepInfo(
							"The relevant page was opened upon clicking Pet CBD Oil under Pet CBD Products in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Pet CBD Oil under Pet CBD Products in Shop Menu");
				}
				break;
			}

			case "petCBDdogtreats": {
				if (isElementDisplayed(petCBDdogtreatsHeader)) {
					highLighterMethod(petCBDdogtreatsHeader);
					testStepInfo(
							"The relevant page was opened upon clicking Pet CBD Dog Treats under Pet CBD Products in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Pet CBD Dog Treats under Pet CBD Products in Shop Menu");
				}
				break;
			}

			case "petCBDshampoo": {
				if (isElementDisplayed(petCBDshampooHeader)) {
					highLighterMethod(petCBDshampooHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Shampoo for Pets under Pet CBD Products in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Shampoo for Pets under Pet CBD Products in Shop Menu");
				}
				break;
			}

			case "petCBDpawButter": {
				if (isElementDisplayed(petCBDpawButterHeader)) {
					highLighterMethod(petCBDpawButterHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Paw Butter for Pets under Pet CBD Products in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Paw Butter for Pets under Pet CBD Products in Shop Menu");
				}
				break;
			}

			case "apparel": {
				waitForElementToDisplay(apparelHeader, 30);
				if (isElementDisplayed(apparelHeader)) {
					highLighterMethod(apparelHeader);
					testStepInfo("The relevant page was opened upon clicking Apparel in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Apparel in Shop Menu");
				}
				break;
			}

			case "subscriptions": {
				if (isElementDisplayed(subscriptionsHeader)) {
					highLighterMethod(subscriptionsHeader);
					testStepInfo("The relevant page was opened upon clicking Subscriptions in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Subscriptions in Shop Menu");
				}
				break;
			}

			case "blog": {
				if (isElementDisplayed(blogHeader)) {
					highLighterMethod(blogHeader);
					testStepInfo("The relevant page was opened upon clicking Blog in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Blog in Learn Menu");
				}
				break;
			}

			case "ourStory": {
				if (isElementDisplayed(ourStoryHeader)) {
					highLighterMethod(ourStoryHeader);
					testStepInfo("The relevant page was opened upon clicking Our Story in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Our Story in Learn Menu");
				}
				break;
			}

			case "CBDfacts": {
				if (isElementDisplayed(cbdFactsHeader)) {
					highLighterMethod(cbdFactsHeader);
					testStepInfo("The relevant page was opened upon clicking Learn More About CBD in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Learn More About CBD in Learn Menu");
				}
				break;
			}

			case "benefitsOfCBDoil": {
				if (isElementDisplayed(benefitsOfCBDoilHeader)) {
					highLighterMethod(benefitsOfCBDoilHeader);
					testStepInfo("The relevant page was opened upon clicking Benefits of CBD Oil in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Benefits of CBD Oil in Learn Menu");
				}
				break;
			}

			case "intheNews": {
				if (isElementDisplayed(intheNewsHeader)) {
					highLighterMethod(intheNewsHeader);
					testStepInfo("The relevant page was opened upon clicking In The News in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking In The News in Learn Menu");
				}
				break;
			}

			case "thirdPartLabTesting": {
				if (isElementDisplayed(labtestingResultsHeader)) {
					highLighterMethod(labtestingResultsHeader);
					testStepInfo("The relevant page was opened upon clicking Third Party Lab Testing in Learn Menu");
				} else {
					testStepFailed("The relevant page was opened upon clicking Third Lab Testing in Learn Menu");
				}
				break;
			}

			case "faq": {
				if (isElementDisplayed(faqHeader)) {
					highLighterMethod(faqHeader);
					testStepInfo("The relevant page was opened upon clicking FAQ in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking FAQ in Learn Menu");
				}
				break;
			}

			case "productFinderQuiz": {
				if (isElementDisplayed(productFinderQuizHeader)) {
					highLighterMethod(productFinderQuizHeader);
					testStepInfo("The relevant page was opened upon clicking Product Finder Quiz in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Product Finder Quiz in Learn Menu");
				}
				break;
			}

			case "becomeDistributor": {
				if (isElementDisplayed(becomeDistributorHeader)) {
					highLighterMethod(becomeDistributorHeader);
					testStepInfo("The relevant page was opened upon clicking Become Distributor in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Become Distributor in Learn Menu");
				}
				break;
			}

			case "productLiability": {
				if (isElementDisplayed(productLiabilityHeader)) {
					highLighterMethod(productLiabilityHeader);
					testStepInfo("The relevant page was opened upon clicking Product Liability in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Product Liability in Learn Menu");
				}
				break;
			}

			case "legalSummary": {
				if (isElementDisplayed(legalSummaryHeader)) {
					highLighterMethod(legalSummaryHeader);
					testStepInfo("The relevant page was opened upon clicking Legal Summary in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Legal Summary in Learn Menu");
				}
				break;
			}

			case "distributorTerms": {
				if (isElementDisplayed(distributorTermsHeader)) {
					highLighterMethod(distributorTermsHeader);
					testStepInfo("The relevant page was opened upon clicking Distributor Terms in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Distributor Terms in Learn Menu");
				}
				break;
			}

			case "distributorBlog": {
				if (isElementDisplayed(distributorBlogHeader)) {
					highLighterMethod(distributorBlogHeader);
					testStepInfo("The relevant page was opened upon clicking Distributor Blog in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Distributor Blog in Learn Menu");
				}
				break;
			}

			case "contact": {
				if (isElementDisplayed(contactUsHeader)) {
					highLighterMethod(contactUsHeader);
					testStepInfo("The relevant page was opened upon clicking contact");
				} else {
					testStepFailed("The relevant page was not opened upon clicking contact");
				}
				break;
			}

			case "rewards": {
				if (isElementDisplayed(rewardsHeader)) {
					highLighterMethod(rewardsHeader);
					testStepInfo("The relevant page was opened upon clicking Rewards in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Rewards in Learn Menu");
				}
				break;
			}

			case "facebook": {
				switchToLastTab();
				if (driver.getCurrentUrl().equalsIgnoreCase("https://www.facebook.com/HempBombs/")) {
					testStepInfo("Facebook page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Facebook page of HempBombs could not be opened");
				}
				break;
			}

			case "instagram": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("www.instagram.com/hempbombs/")
						|| driver.getCurrentUrl().contains("www.instagram.com/accounts/login")) {
					testStepInfo("Instagram page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Instagram page of HempBombs could not be opened");
				}
				break;
			}

			case "youtube": {
				switchToLastTab();
				if (driver.getCurrentUrl()
						.equalsIgnoreCase("https://www.youtube.com/channel/UCdFNadVkVyq_CwJWETPCa3g")) {
					testStepInfo("Youtube page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Youtube page of HempBombs could not be opened");
				}
				break;
			}

			case "twitter": {
				switchToLastTab();
				if (driver.getCurrentUrl().equalsIgnoreCase("https://twitter.com/HempBombs")) {
					testStepInfo("Twitter page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Twitter page of HempBombs could not be opened");
				}
				break;
			}

			case "delta8": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://hempbombsplus.com")) {
					testStepInfo("HempBombs Delta-8 page was opened");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("HempBombs Delta-8 page could not be opened");
				}
				break;
			}

			case "distributors_storeOwners": {
				if (isElementDisplayed(becomeDistributorHeader)) {
					highLighterMethod(becomeDistributorHeader);
					testStepInfo("The relevant page was opened upon clicking Distributors/storeOwners in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Distributors/storeOwners in Footer");
				}
				break;
			}

			case "affiliate": {
				if (isElementDisplayed(affiliateHeader)) {
					highLighterMethod(affiliateHeader);
					testStepInfo("The relevant page was opened upon clicking affiliate in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking affiliate in Footer");
				}
				break;
			}

			case "seniorDiscount": {
				waitForElementToDisplay(seniorCitizenDiscountHeader, 30);
				if (isElementDisplayed(seniorCitizenDiscountHeader)) {
					highLighterMethod(seniorCitizenDiscountHeader);
					testStepInfo("The relevant page was opened upon clicking Senior Discount in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Senior Discount in Footer");
				}
				break;
			}

			case "veteran_activeDuty": {
				waitForElementToDisplay(militaryAndVeteranDiscountHeader, 60);
				if (isElementDisplayed(militaryAndVeteranDiscountHeader)) {
					highLighterMethod(militaryAndVeteranDiscountHeader);
					testStepInfo("The relevant page was opened upon clicking Veteran/active Duty program in Footer");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Veteran/active Duty program in Footer");
				}
				break;
			}

			case "firstResponders": {
				if (isElementDisplayed(firstRespondersDiscountHeader)) {
					highLighterMethod(firstRespondersDiscountHeader);
					testStepInfo("The relevant page was opened upon clicking First Responders in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking First Responders in Footer");
				}
				break;
			}

			case "siteMap": {
				if (isElementDisplayed(siteMapHeader)) {
					highLighterMethod(siteMapHeader);
					testStepInfo("The relevant page was opened upon clicking SiteMap in Footer");
					driver.navigate().back();
				} else {
					testStepFailed("The relevant page was not opened upon clicking SiteMap in Footer");
				}
				break;
			}

			case "ccpa": {
				switchToLastTab();
				if (isElementDisplayed(ccpaHeader)) {
					highLighterMethod(ccpaHeader);
					testStepInfo("The relevant page was opened upon clicking CCPA in Footer");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("The relevant page was not opened upon clicking CCPA in Footer");
				}
				break;
			}

			case "noSearchResults Message": {
				if (isElementDisplayed(noSearchResults_Message_SearchPage)) {
					highLighterMethod(noSearchResults_Message_SearchPage);
					testStepInfo("No Search Results Message was displayed");
				} else {
					testStepFailed("No Search Results Message was not displayed");
				}
				break;
			}

			case "Minimum Query Length": {
				if (isElementDisplayed(minimumQueryLength_Message_SearchPage)) {
					highLighterMethod(minimumQueryLength_Message_SearchPage);
					testStepInfo("Minimum Query Length Message was displayed");
				} else {
					testStepFailed("Minimum Query Length Message was not displayed");
				}
				break;
			}

			case "Number of Items": {
				if (isElementDisplayed(numberOfItems_SearchPage)) {
					highLighterMethod(numberOfItems_SearchPage);
					testStepInfo("A valid search was performed successfully");
				} else {
					testStepFailed("A valid search could not be performed", "Number of items is not displayed");
				}
				break;
			}

			case "Gummies Search Product Title": {
				if (isElementDisplayed(gummiesSearchProductTitle_SearchPage)) {
					scrollToViewElement(gummiesSearchProductTitle_SearchPage);
					highLighterMethod(gummiesSearchProductTitle_SearchPage);
					testStepInfo("Valid Product was displayed for the search done");
				} else {
					testStepFailed("Valid Product was not displayed for the search done");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Relevant verification could not be done successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method to go to my account page after User is logged in.
	 */
	public void goToMyAccount() {
		try {
			if (isElementPresent(myAccount)) {
				highLighterMethod(myAccount);
				clickOn(myAccount);
			} else {
				testStepFailed("Could not go to My Account page", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on My Account successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to navigate through Footer.
	 */
	public void navigateFooter(String option) {

		try {
			switch (option) {
			case "allCBDProduct": {
				if (isElementDisplayed(allCBDProduct)) {
					highLighterMethod(allCBDProduct);
					clickOn(allCBDProduct);
				} else {
					testStepFailed("All CBD Products not displayed in Footer");
				}
				break;
			}

			case "cbdEdibles": {
				if (isElementDisplayed(cbdEdibles)) {
					highLighterMethod(cbdEdibles);
					clickOn(cbdEdibles);
				} else {
					testStepFailed("CBD Edibles not displayed in Footer");
				}
				break;
			}
//
//			case "cbdVape": {
//				if (isElementPresent(cbdVape)) {
//					scrollToViewElement(cbdVape);
//					highLighterMethod(cbdVape);
//					clickOn(cbdVape);
//				} else {
//					testStepFailed("CBD Vape was not displayed in Footer");
//				}
//				break;
//			}

			case "cbdPetProducts": {
				if (isElementDisplayed(cbdPetProducts)) {
					scrollToViewElement(cbdPetProducts);
					highLighterMethod(cbdPetProducts);
					clickOn(cbdPetProducts);
				} else {
					testStepFailed("CBD Pet Products was not displayed in Footer");
				}
				break;
			}

			case "cbdOils": {
				if (isElementDisplayed(cbdOils)) {
					highLighterMethod(cbdOils);
					clickOn(cbdOils);
				} else {
					testStepFailed("CBD Oils not displayed in Footer");
				}
				break;
			}

			case "cbdTopicals": {
				if (isElementDisplayed(cbdTopicals)) {
					highLighterMethod(cbdTopicals);
					mouseOver(cbdTopicals);
					clickOn(cbdTopicals);
				} else {
					testStepFailed("CBD Topicals not displayed in Footer");
				}
				break;
			}

			case "hempBombsApparel": {
				if (isElementDisplayed(hempBombsApparel)) {
					mouseOver(hempBombsApparel);
					highLighterMethod(hempBombsApparel);
					clickOn(hempBombsApparel);
				} else {
					testStepFailed("HempBombs Apparel not displayed in Footer");
				}
				break;
			}

			case "facebook": {
				if (isElementDisplayed(facebook)) {
					highLighterMethod(facebook);
					clickOn(facebook);
				} else {
					testStepFailed("Facebook Icon not displayed in Footer");
				}
				break;
			}

			case "instagram": {
				if (isElementDisplayed(instagram)) {
					highLighterMethod(instagram);
					clickOn(instagram);
				} else {
					testStepFailed("Instagram Icon not displayed in Footer");
				}
				break;
			}

			case "youtube": {
				if (isElementDisplayed(youtube)) {
					highLighterMethod(youtube);
					clickOn(youtube);
				} else {
					testStepFailed("Youtube Icon not displayed in Footer");
				}
				break;
			}

			case "twitter": {
				if (isElementDisplayed(twitter)) {
					highLighterMethod(twitter);
					clickOn(twitter);
				} else {
					testStepFailed("Twitter Icon not displayed in Footer");
				}
				break;
			}

			case "homeFooter": {
				if (isElementDisplayed(homeFooter)) {
					highLighterMethod(homeFooter);
					clickOn(homeFooter);
				} else {
					testStepFailed("Home not displayed in Footer");
				}
				break;
			}

			case "distributors_storeOwners": {
				if (isElementDisplayed(distributors_storeOwners)) {
					highLighterMethod(distributors_storeOwners);
					clickOn(distributors_storeOwners);
				} else {
					testStepFailed("Distributors/storeOwners not displayed in Footer");
				}
				break;
			}

			case "affiliates": {
				if (isElementDisplayed(affiliates)) {
					scrollToViewElement(affiliates);
					highLighterMethod(affiliates);
					clickOn(affiliates);
				} else {
					testStepFailed("Affiliates not displayed in Footer");
				}
				break;
			}

			case "seniorDiscount": {
				scrollToViewElement(seniorDiscount);
				waitForElementToDisplay(seniorDiscount, 30);
				if (isElementDisplayed(seniorDiscount)) {
					highLighterMethod(seniorDiscount);
					clickOn(seniorDiscount);
				} else {
					testStepFailed("Senior Discount not displayed in Footer");
				}
				break;
			}

			case "veteran_activeDuty": {
				scrollToViewElement(veteran_activeDuty);
				if (isElementDisplayed(veteran_activeDuty)) {
					highLighterMethod(veteran_activeDuty);
					clickOn(veteran_activeDuty);
				} else {
					testStepFailed("Veteran/ActiveDuty Program not displayed in Footer");
				}
				break;
			}

			case "firstResponders": {
				if (isElementDisplayed(firstResponders)) {
					highLighterMethod(firstResponders);
					clickOn(firstResponders);
				} else {
					testStepFailed("First Responders not displayed in Footer");
				}
				break;
			}

			case "contactFooter": {
				scrollToViewElement(contactFooter);
				if (isElementDisplayed(contactFooter)) {
					highLighterMethod(contactFooter);
					clickOn(contactFooter);
				} else {
					testStepFailed("Contact not displayed in Footer");
				}
				break;
			}

			case "siteMap": {
				if (isElementDisplayed(siteMap)) {
					highLighterMethod(siteMap);
					clickOn(siteMap);
				} else {
					testStepFailed("SiteMap not displayed in Footer");
				}
				break;
			}

			case "ccpa": {
				if (isElementDisplayed(ccpa)) {
					highLighterMethod(ccpa);
					clickOn(ccpa);
				} else {
					testStepFailed("CCPA not displayed in Footer");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Requested navigation in footer could not be done");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to enter query into search box.
	 */
	public void enterQuery_SearchBox(String query) {

		try {
			if (isElementPresent(searchBox)) {
				highLighterMethod(searchBox);
				typeIn(searchBox, query);
			} else {
				testStepFailed("Search box not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Query could not be entered into the search box successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to press enter button to .
	 */
	public void pressEnterButton() {

		try {
			webElement.sendKeys(Keys.RETURN);
		} catch (Exception e) {
			testStepFailed("Enter button could not be clicked successfully");
			e.printStackTrace();
		}
	}
}
