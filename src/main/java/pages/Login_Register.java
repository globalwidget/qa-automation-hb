package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Login_Register extends ApplicationKeywords {

	private static final String inputUsername_Login = "Login Username #id=username";
	private static final String inputPassword_Login = "Login Password #id=password";
	private static final String loginButton = "Login Button #xpath=//button[contains(text(),'Log in')]";
	private static final String errorMessage_Login = "Login Error Message #xpath=//li/strong[contains(text(),'ERROR')]";
	private static final String errorMessage_Register = "Register Error Message #xpath=//li/strong[contains(text(),'Error')]";
	private static final String inputUsername_Register = "Register Username #xpath=//input[@id='reg_email']";
	private static final String inputPassword_Register = "Register Password #xpath=//input[@id='reg_password']";
	private static final String registerButton = "Register Button #xpath=//button[contains(text(),'Register')]";

	public Login_Register(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to login
	 * 
	 * @param loginUsername is the value of login id
	 * @param loginPassword is the value of login password.
	 */
	public void login(String loginUsername, String loginPassword) {
		try {
			if (isElementDisplayed(loginButton)) {
				highLighterMethod(inputUsername_Login);
				typeIn(inputUsername_Login, loginUsername);
				highLighterMethod(inputPassword_Login);
				typeIn(inputPassword_Login, loginPassword);
				highLighterMethod(loginButton);
				testStepInfo("Valid Credentials entered");
				clickOn(loginButton);
				GOR.loggedIn = true;
				testStepInfo("Login button clicked successfully");
			} else {
				testStepFailed("Login Page not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not login successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to register
	 * 
	 * @param registerUsername is the value of register id
	 * @param registerPassword is the value of register password.
	 */
	public void register(String registerUsername, String registerPassword) {
		try {
			if (isElementDisplayed(loginButton)) {
				highLighterMethod(inputUsername_Register);
				typeIn(inputUsername_Register, registerUsername);
				highLighterMethod(inputPassword_Register);
				typeIn(inputPassword_Register, registerPassword);
				highLighterMethod(registerButton);
				testStepInfo("Valid Credentials entered");
				clickOn(registerButton);
				GOR.freshAccountCreated = true;
			} else {
				testStepFailed("Register Page not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not register successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Error Message for invalid login
	 */
	public void verifyErrorMessage_Login() {
		try {
			if (isElementPresent(errorMessage_Login)) {
				highLighterMethod(errorMessage_Login);
				manualScreenshot("Error Message for Invalid Login Credentials is displayed");
			} else {
				testStepFailed("Error Message not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify error message for Invalid Login Credentials successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Error Message for pre-existing
	 * credentials in Register
	 */
	public void verifyErrorMessage_Register() {
		try {
			if (isElementDisplayed(errorMessage_Register)) {
				highLighterMethod(errorMessage_Register);
				testStepInfo("Error Message for pre-existing Credentials is displayed");
			} else {
				testStepFailed("Error Message not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify error message for pre-existing Login Credentials successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify invalid password restriction for Register
	 */
	public void verifyInvalidPasswordRestriction_Resgister() {
		try {
			if (isElementDisplayed(registerButton)) {
				waitTime(5);
				String value = getAttributeValue(registerButton, "disabled");
				if (value.contains("true")) {
					testStepInfo("Invalid Password restriction verified");
				} else {
					testStepFailed("Invalid Password restriction could not be verified");
				}
			} else {
				testStepFailed("Register Button not present");
			}
		} catch (Exception e) {
			{
				testStepFailed("Invalid Password restriction could not be verified");
				e.printStackTrace();

			}

		}

	}

	/**
	 * Description: Method is used to check if the user is logged Out.
	 */

	public void verifyPresenceOfLogin() {

		try {
			if (isElementPresent(loginButton) == true) {
				testStepInfo("User is not logged In");
			} else {
				testStepFailed("Logout verification failed");
			}
		} catch (Exception e) {
			testStepFailed("Presence of Login button could not be verified");
			e.printStackTrace();
		}

	}
}
