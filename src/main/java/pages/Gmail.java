package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class Gmail extends ApplicationKeywords {

	private static final String email = "Email #xpath=//input[@type='email']";
	private static final String next = "Next #xpath=//span[contains(text(),'Next')]";
	private static final String password = "Email #xpath=//input[@type='password']";

	public Gmail(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to login.
	 * 
	 * @param : Mail ID, Password
	 */
	public void fillDetails(String mailId, String password_data) {
		try {
			if (isElementDisplayed(email)) {
				highLighterMethod(email);
				typeIn(email, mailId);
				clickOn(next);
			} else {
				testStepFailed("The mail ID could not be entered", "Element not displayed");
			}

			if (isElementDisplayed(password)) {
				highLighterMethod(password);
				typeIn(password, password_data);
				clickOn(next);
			} else {
				testStepFailed("The password could not be entered", "Element not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not login to Gmail");
			e.printStackTrace();
		}
	}

}
