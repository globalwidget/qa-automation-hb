package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class Rewards extends ApplicationKeywords {

	private static final String rewardsHistory = "Rewards History #xpath=//span[contains(text(),'REWARDS HISTORY')]/..";
	private static final String myRewards_Action = "My Rewards - Action #xpath=//div[@class='yotpo-grid-row']//div[@class='first-row-desktop']/div[2]/div";
	private static final String myRewards_Points = "My Rewards - Points #xpath=//div[@class='yotpo-grid-row']//div[@class='second-row-desktop']/div[1]/div/div";
	private static final String myRewards_Status = "My Rewards - Status #xpath=//div[@class='yotpo-grid-row']//div[@class='second-row-desktop']/div[2]/div/div";
	private static final String closeMyRewards = "Close My Rewards Section #xpath=//div[@class='yotpo-close-button']";
	private static final String rewardsHeader = "Rewards Header #xpath=//h1[contains(text(),'CBD Rewards')]";
	private static final String shareOnFacebookTile = "Share On Facebook Tile #xpath=//div[contains(@class,'facebooksharecampaign')]";
	private static final String shareOnFacebookButton = "Share On Facebook Button #xpath=//button[contains(text(),'Share on Facebook')]";
	private static final String readOurBlogTile = "Read Our Blog Tile #xpath=//div[contains(text(),'Blog')]/..";
	private static final String readContentButton = "Read Content Button #xpath=//button[contains(text(),'Read Content')]";
	private static final String cbdBlogHeader = "CBD Blog Header #xpath=//h1[contains(text(),'CBD Blog')]";
	private static final String haveYouReadBlog_Header = "Have you read our Blog - Header #xpath=//div[contains(text(),'Have you read our blog?')]";
	private static final String haveYouReadBlog_TextBox = "Have you read our Blog - TextBox #xpath=//div[contains(@class,'ReadContentCampaign')]//textarea";
	private static final String haveYouReadBlog_Submit = "Have you read our Blog - Submit #xpath=//div[contains(@class,'ReadContentCampaign')]//button";
	private static final String GoBack = "Have you read our Blog - Go Back #xpath=//a[contains(text(),'Go Back')]";
	private static final String correctAnswer = "Have you read our Blog - Correct Answer #xpath=//div[contains(text(),'Great')]";
	private static final String wrongAnswer = "Have you read our Blog - Wrong Answer #xpath=//div[contains(text(),'Sorry, this is not the right answer')]";
	private static final String followOnInstagramTile = "Follow On Instagram Tile #xpath=//div[contains(@class,'instagramfollowcampaign')]";
	private static final String followOnInstagramButton = "Follow On Instagram Button #xpath=//button[contains(text(),'Follow Us')]";
	private static final String visitFacebookTile = "Share On Facebook Tile #xpath=//div[contains(@class,'facebookpagevisitcampaign')]";
	private static final String visitFacebookButton = "Share On Facebook Button #xpath=//button[contains(text(),'Visit Page')]";
	private static final String followOnTwitterTile = "Follow On Twitter Tile #xpath=//div[contains(@class,'twitterfollowcampaign')]";
	private static final String followOnTwitterButton = "Follow On Twitter Button #xpath=//button[contains(text(),'Follow Us')]";
	private static final String subscribeToYoutubeTile = "Subscribe To Youtube Tile #xpath=//div[contains(text(),'YouTube') or contains(text(),'subscribe')]/..";
	private static final String subscribeToYoutubeButton = "Subscribe To Youtube Button #xpath=//button[contains(text(),'Subscribe')]";
	private static final String didYouSubscribe_Header = "Did You Subscribe - Header #xpath=//div[contains(text(),'Did you subscribe to the Hemp Bombs YouTube channel?')]";
	private static final String didYouSubscribe_TextBox = "Did You Subscribe - TextBox #xpath=//div[contains(text(),'Did you subscribe to the Hemp Bombs YouTube channel?')]/following-sibling::textarea";
	private static final String didYouSubscribe_Submit = "Did You Subscribe - Submit #xpath=//div[contains(@class,'ReadContentCampaign')]//button";
	private static final String pointsAvailableDisplay = "Points Available Display #xpath=//div[contains(text(),'YOU HAVE')]";

	public Rewards(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to verify Rewards page opened successfully.
	 * 
	 */
	public void verifyRewardsPage() {
		try {
			if (isElementDisplayed(rewardsHeader)) {
				highLighterMethod(rewardsHeader);
				testStepInfo("The Rewards Page was opened successfully.");
			} else {
				testStepFailed("Could not verify the Rewards Page", "Rewards Page Header was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify the Rewards Page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Rewards History.
	 * 
	 */
	public void clickRewardsHistory() {
		try {
			if (isElementDisplayed(rewardsHistory)) {
				highLighterMethod(rewardsHistory);
				clickOn(rewardsHistory);
			} else {
				testStepFailed("Could not click on Rewards History", "Rewards History button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Rewards History");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to close My Rewards Section.
	 * 
	 */
	public void closeMyRewardsSection() {
		try {
			if (isElementDisplayed(closeMyRewards)) {
				highLighterMethod(closeMyRewards);
				clickOn(closeMyRewards);
			} else {
				testStepFailed("Could not close My Rewards Section",
						"Close button not displayed for My Rewards section");
			}
		} catch (Exception e) {
			testStepFailed("Could not close My Rewards Section");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify My Rewards section.
	 * 
	 */
	public void verifyMyRewardsSection(String action_data, int points_data, String status_data) {
		try {
			boolean actionFound = false;
			List<WebElement> actions_Table = new ArrayList<WebElement>();
			List<WebElement> points_Table = new ArrayList<WebElement>();
			List<WebElement> status_Table = new ArrayList<WebElement>();

			actions_Table = findWebElements(myRewards_Action);
			points_Table = findWebElements(myRewards_Points);
			status_Table = findWebElements(myRewards_Status);

			if (actions_Table.size() != 0) {
				for (int index = 0; index < actions_Table.size(); index++) {
					String action = actions_Table.get(index).getText().toLowerCase();
					int point = Integer.parseInt(points_Table.get(index).getText());
					String status = status_Table.get(index).getText().toLowerCase();

					if (action.contains(action_data)) {
						actionFound = true;
						if (actions_Table.get(index).isDisplayed() && action.contains(action_data)) {
							highLighterMethod(actions_Table.get(index));
							if (points_Table.get(index).isDisplayed() && point == points_data) {
								highLighterMethod(points_Table.get(index));
								if (status_Table.get(index).isDisplayed() && status.contains(status_data)) {
									highLighterMethod(status_Table.get(index));
									testStepInfo("The expected action - " + action_data + " with points - "
											+ points_data + " was present with the status - " + status);
									break;
								} else {
									testStepFailed("The expected status - " + status_data + "was not found",
											"Status not displayed or wrong status is displayed");
								}
							} else {
								testStepFailed("The expected points - " + points_data + "was not found",
										"Points not displayed or wrong points is displayed");
							}
						} else {
							testStepFailed("The expected actions - " + action_data + "was not found",
									"Action not displayed or wrong points is displayed");
						}
					}
				}

			} else {
				testStepFailed("No rewards found in My Rewards section");
			}
			if (actionFound == false) {
				testStepFailed(
						"The expected Reward for the action - " + action_data + " was not found in My Rewards section");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Rewards History");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify the routing or a message displayed.
	 */
	public void verifyNavigationOrMessage(String option) {
		try {
			switch (option) {
			case "cbdBlogPage": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("blog")) {
					if (isElementDisplayed(cbdBlogHeader)) {
						highLighterMethod(cbdBlogHeader);
						testStepInfo("CBD Blog Page opened successfully");
						driver.close();
						switchToLastTab();
					} else {
						Set<String> numberOfWindows = driver.getWindowHandles();
						if (numberOfWindows.size() > 1) {
							driver.close();
						}
						switchToLastTab();
						testStepFailed("CBD Blog Page could not be opened successfully",
								"Relevant Header not displayed");
					}
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("CBD Blog Page could not be opened successfully");
				}
				break;
			}

			case "haveYouReadBlog-Correct": {
				mouseOver(readOurBlogTile);
				waitForElementToDisplay(correctAnswer, 4);
				if (isElementDisplayed(correctAnswer)) {
					highLighterMethod(correctAnswer);
					testStepInfo("Answer Submitted successfully for - 'Have You Read Our Blog''");
					if (isElementDisplayed(GoBack)) {
						highLighterMethod(GoBack);
						clickOn(GoBack);
					} else {
						testStepFailed("Go Back button is  not displayed");
					}
				} else {
					testStepFailed("Answer not submitted successfully for - 'Have You Read Our Blog'",
							"Relevant message not displayed");
				}
				break;
			}

			case "haveYouReadBlog-Wrong": {
				mouseOver(readOurBlogTile);
				waitForElementToDisplay(wrongAnswer, 4);
				if (isElementDisplayed(wrongAnswer)) {
					highLighterMethod(wrongAnswer);
					testStepInfo("Message displayed for wrong answer submitted in - 'Have You Read Our Blog'");
					if (isElementDisplayed(GoBack)) {
						highLighterMethod(GoBack);
						clickOn(GoBack);
					} else {
						testStepFailed("Go Back button is  not displayed");
					}
				} else {
					testStepFailed("Message not displayed for wrong answer submitted in - 'Have You Read Our Blog'",
							"Relevant message not displayed");
				}
				break;
			}

			case "didYouSubscribe-Correct": {
				mouseOver(subscribeToYoutubeTile);
				waitForElementToDisplay(correctAnswer, 4);
				if (isElementDisplayed(correctAnswer)) {
					highLighterMethod(correctAnswer);
					testStepInfo("Answer Submitted successfully for - 'Did You Subscribe'");
					if (isElementDisplayed(GoBack)) {
						highLighterMethod(GoBack);
						clickOn(GoBack);
					} else {
						testStepFailed("Go Back button is  not displayed");
					}
				} else {
					testStepFailed("Answer not submitted successfully for - 'Did You Subscribe'",
							"Relevant message not displayed");
				}
				break;
			}

			case "didYouSubscribe-Wrong": {
				mouseOver(subscribeToYoutubeTile);
				waitForElementToDisplay(wrongAnswer, 4);
				if (isElementDisplayed(wrongAnswer)) {
					highLighterMethod(wrongAnswer);
					testStepInfo("Message displayed for wrong answer submitted in - 'Did You Subscribe'");
					if (isElementDisplayed(GoBack)) {
						highLighterMethod(GoBack);
						clickOn(GoBack);
					} else {
						testStepFailed("Go Back button is  not displayed");
					}
				} else {
					testStepFailed("Message not displayed for wrong answer submitted in - 'Did You Subscribe'",
							"Relevant message not displayed");
				}
				break;
			}

			case "haveYouReadBlog": {
				mouseOver(readOurBlogTile);
				waitForElementToDisplay(haveYouReadBlog_Header, 4);
				if (isElementDisplayed(haveYouReadBlog_Header) && isElementDisplayed(haveYouReadBlog_TextBox)) {
					highLighterMethod(haveYouReadBlog_Header);
					highLighterMethod(haveYouReadBlog_TextBox);
					testStepInfo("'Have You Read Our Blog' question displayed with textbox'");
				} else {
					testStepFailed("'Have You Read Our Blog' question not displayed ",
							"Header or textbox is not displayed");
				}
				break;
			}

			case "didYouSubscribe": {
				mouseOver(subscribeToYoutubeTile);
				waitForElementToDisplay(didYouSubscribe_Header, 4);
				if (isElementDisplayed(didYouSubscribe_Header) && isElementDisplayed(didYouSubscribe_TextBox)) {
					highLighterMethod(didYouSubscribe_Header);
					highLighterMethod(didYouSubscribe_TextBox);
					testStepInfo("'Did You Subscribe' question displayed with textbox'");
				} else {
					testStepFailed("'Did You Subscribe' question not displayed ", "Header or textbox is not displayed");
				}
				break;
			}

			case "instagram": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.instagram.com/hempbombs")) {
					testStepInfo("Instagram Page of HempBombs opened successfully");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Instagram Page of HempBombs could not be opened successfully");
				}
				break;
			}
			case "twitter": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://twitter.com/intent/follow?screen_name=HempBombs")) {
					testStepInfo("Twitter Page of HempBombs opened successfully");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Twitter Page of HempBombs could not be opened successfully");
				}
				break;
			}

			case "youtube": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.youtube.com/c/HempBombs")) {
					testStepInfo("Youtube Page of HempBombs opened successfully");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Youtube Page of HempBombs could not be opened successfully");
				}
				break;
			}

			case "facebook": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.facebook.com/HempBombs")) {
					testStepInfo("Facebook Page of HempBombs opened successfully");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Facebook Page of HempBombs could not be opened successfully");
				}
				break;
			}

			case "facebookLogin": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.facebook.com/login")) {
					testStepInfo("Facebook Login Page of HempBombs opened successfully");
					driver.close();
					switchToLastTab();
				} else {
					Set<String> numberOfWindows = driver.getWindowHandles();
					if (numberOfWindows.size() > 1) {
						driver.close();
					}
					switchToLastTab();
					testStepFailed("Facebook Login Page of HempBombs could not be opened successfully");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Could not click on 'Read Content'");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to answer 'Have You Read Blog' question.
	 * 
	 */
	public void answerHaveYouReadBlog(String answer) {
		try {
			if (isElementPresent(haveYouReadBlog_TextBox)) {
				mouseOver(readOurBlogTile);
				waitForElementToDisplay(haveYouReadBlog_Header, 4);
				highLighterMethod(haveYouReadBlog_TextBox);
				typeIn(haveYouReadBlog_TextBox, answer);
				highLighterMethod(haveYouReadBlog_Submit);
				clickOn(haveYouReadBlog_Submit);
			} else {
				testStepFailed("Could not submit the answer for 'Have You Read Blog' question", "TextBox not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not submit the answer for 'Have You Read Blog' question");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to answer 'Did You Subscribe' question.
	 * 
	 */
	public void answerdidYouSubscribe(String answer) {
		try {
			mouseOver(subscribeToYoutubeTile);
			if (isElementPresent(didYouSubscribe_TextBox)) {
				mouseOver(subscribeToYoutubeTile);
				waitForElementToDisplay(didYouSubscribe_Header, 4);
				highLighterMethod(didYouSubscribe_TextBox);
				typeIn(didYouSubscribe_TextBox, answer);
				highLighterMethod(didYouSubscribe_Submit);
				clickOn(didYouSubscribe_Submit);
			} else {
				testStepFailed("Could not submit the answer for 'Did You Subscribe' question", "TextBox not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not submit the answer for 'Did You Subscribe' question");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to navigate and click.
	 */
	public void navigate(String option) {
		try {
			switch (option) {
			case "shareOnFacebook": {
				waitForElement(shareOnFacebookTile);
				if (isElementPresent(shareOnFacebookTile)) {
					mouseOver(shareOnFacebookTile);
					waitForElementToDisplay(shareOnFacebookButton, 6);
					highLighterMethod(shareOnFacebookButton);
					clickOn(shareOnFacebookButton);

				} else {
					testStepFailed("Could not click on 'Share on Facebook'", "'Share on Facebook' Tile not present");
				}
				break;
			}

			case "readOurBlog": {
				waitForElement(readOurBlogTile);
				if (isElementPresent(readOurBlogTile)) {
					mouseOver(readOurBlogTile);
					waitForElementToDisplay(readContentButton, 6);
					highLighterMethod(readContentButton);
					clickOn(readContentButton);
				} else {
					testStepFailed("Could not click on 'Read Content'", "'Read Our Blog' Tile not present");
				}
				break;
			}

			case "followOnInstagram": {
				waitForElement(followOnInstagramTile);
				if (isElementPresent(followOnInstagramTile)) {
					mouseOver(followOnInstagramTile);
					waitForElementToDisplay(followOnInstagramButton, 6);
					highLighterMethod(followOnInstagramButton);
					clickOn(followOnInstagramButton);
				} else {
					testStepFailed("Could not click on button for 'Follow Instagram'",
							"'Follow Instagram' Tile not present");
				}
				break;
			}

			case "visitFacebook": {
				waitForElement(visitFacebookTile);
				if (isElementPresent(visitFacebookTile)) {
					mouseOver(visitFacebookTile);
					waitForElementToDisplay(visitFacebookButton, 6);
					highLighterMethod(visitFacebookButton);
					clickOn(visitFacebookButton);
				} else {
					testStepFailed("Could not click on button for 'Visit Facebook'",
							"'Visit Facebook' Tile not present");
				}
				break;
			}

			case "followTwitter": {
				waitForElement(followOnTwitterTile);
				if (isElementPresent(followOnTwitterTile)) {
					mouseOver(followOnTwitterTile);
					waitForElementToDisplay(followOnTwitterButton, 6);
					highLighterMethod(followOnTwitterButton);
					clickOn(followOnTwitterButton);
				} else {
					testStepFailed("Could not click on button for 'Follow Twitter'",
							"'Follow Twitter' Tile not present");
				}
				break;
			}

			case "subscribeToYoutube": {
				waitForElement(subscribeToYoutubeTile);
				if (isElementPresent(subscribeToYoutubeTile)) {
					mouseOver(subscribeToYoutubeTile);
					waitForElementToDisplay(subscribeToYoutubeButton, 6);
					highLighterMethod(subscribeToYoutubeButton);
					clickOn(subscribeToYoutubeButton);
				} else {
					testStepFailed("Could not click on button for 'Subscribe Youtube Channel'",
							"'Subscribe Youtube Channel' Tile not present");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Could not perform the requested action");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to fetch the available points displayed.
	 * 
	 */
	public int fetchPoints() {
		try {
			waitForElementToDisplay(pointsAvailableDisplay, 5);
			scrollToViewElement(pointsAvailableDisplay);
			if (isElementDisplayed(pointsAvailableDisplay)) {
				highLighterMethod(pointsAvailableDisplay);
				int points = Integer
						.parseInt(getText(pointsAvailableDisplay).split("HAVE")[1].split("POINTS")[0].trim());
				return points;
			} else {
				testStepFailed("Points available could not be fetched",
						"Points available not displayed  in the My Cart Page");
			}
		} catch (Exception e) {
			testStepFailed("Points available displayed in the My Cart Page");
			e.printStackTrace();
		}
		return 0;
	}
}
