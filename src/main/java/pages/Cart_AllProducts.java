package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Cart_AllProducts extends ApplicationKeywords {

	private static final String getAllProducts_gummies_firstElement = "All Gummies Products #xpath=(//li[contains(@class,'gummies')]/a[1])[1]";
	private static final String getAllProducts_gummies_secondElement = "All Gummies Products #xpath=(//li[contains(@class,'gummies')]/a[1])[2]";
	private static final String addToCart = "Add To Cart #xpath=//button[contains(text(),'Add to cart')]";
	private static final String viewCart = "View Cart #xpath=//a[contains(text(),'View cart')]";
	private static final String title_selectedProduct = "Title of product selected #xpath=//h1[contains(@class,'title')]";
	private static final String priceLabel_PGP = "Price - PGP Page #xpath=//span[contains(@class,'price')]";
	private static final String subscribeLabel_PGP = "Subscribe Label - PGP Page #xpath=//small[contains(@class,'sub-options')]";
	private static final String subscribeLabel_PDP = "Subscribe Label - PDP Page #xpath=//span[contains(text(),'Subscribe')]/..";
	private static final String subscribeDropdownSection = "Subscribe and save dropdown section #xpath=//div[@class='wcsatt-options-product-wrapper']";
	private static final String oneTimePurchaseLabel = "One time purchase label #xpath=//label[contains(@class,'prompt-label-one-time')]";
	private static final String subscribeDropdown = "Subscribe Dropdown #xpath=//select[contains(@class,'product-dropdown')]";
	private static final String subscribeRadioButton = "Subscribe radio button #xpath=//input[contains(@name,'subscribe') and @value='yes']";
	private static final String subscribeEveryMonthOption = "Subscribe Dropdown - every month option #xpath=//option[contains(text(),'Every month')]";
	private static final String subscribeEvery2MonthsOption = "Subscribe Dropdown - every 2 months option #xpath=//option[contains(text(),'Every 2 months')]";
	private static final String subscribeEvery3MonthsOption = "Subscribe Dropdown - every 3 months option #xpath=//option[contains(text(),'Every 3 months')]";
	private static final String signUpNow = "SignUp Now #xpath=//button[contains(text(),'Sign up now')]";

	public Cart_AllProducts(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to select the first product on the page
	 */
	public void selectFirstProduct() {
		try {
			if (isElementDisplayed(getAllProducts_gummies_firstElement)) {
				clickOn(getAllProducts_gummies_firstElement);
				testStepInfo("The first product under Gummies is clicked");
			} else {
				testStepFailed("Products were not displayed under Gummies");
			}
		} catch (Exception e) {
			testStepFailed("First Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to select the second product on the page
	 */
	public void selectSecondProduct() {
		try {
			if (isElementDisplayed(getAllProducts_gummies_secondElement)) {
				clickOn(getAllProducts_gummies_secondElement);
				testStepInfo("The second product under Gummies is clicked");
			} else {
				testStepFailed("Products were not displayed under Gummies");
			}
		} catch (Exception e) {
			testStepFailed("Second Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart
	 */
	public void clickAddToCart() {
		try {
			if (isElementDisplayed(addToCart)) {
				highLighterMethod(addToCart);
				clickOn(addToCart);
				testStepInfo("The add to cart button was clicked");
			} else {
				testStepFailed("The add to cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on View Cart
	 */
	public void clickViewCart() {
		try {
			waitForElementToDisplay(viewCart, 30);
			if (isElementDisplayed(viewCart)) {
				highLighterMethod(viewCart);
				clickOn(viewCart);
				GOR.productAdded = true;
				testStepInfo("The View Cart button was clicked");
			} else {
				testStepFailed("The View Cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("View cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get title of the product in the PDP page.
	 */
	public String getProductTitle() {
		try {
			if (isElementDisplayed(title_selectedProduct)) {
				highLighterMethod(title_selectedProduct);
				return getText(title_selectedProduct);
			} else {
				testStepFailed("Product Title of the selected product not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product title");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Description: Method to verify the presence of Subscription availability and
	 * discount under relevant product.
	 */
	public void verifySubscriptionMessage_PGP() {
		try {
			WebElement price = null;
			for (WebElement element : findWebElements(priceLabel_PGP)) {
				if (element.findElements(By.xpath("//span")).size() > 1) {
					price = element;
					break;
				}
			}
			if (price.isDisplayed()) {
				WebElement subscription = price.findElements(By.xpath("//small[contains(@class,'sub-options')]"))
						.get(0);
				WebElement discount = price.findElements(By.xpath("//span[contains(@class,'sub-discount')]")).get(0);
				String subscription_text = subscription.getText();
				String discount_text = discount.getText();
				if (subscription.isDisplayed() && subscription_text.contains(" or subscribe and save up to")) {
					scrollToViewElement(subscription);
					highLighterMethod(subscription);
					testStepInfo("The Subscription availability is displayed under a relevant product");
				}

				if (discount.isDisplayed() && discount_text.contains("20%")) {
					highLighterMethod(discount);
					testStepInfo("The Discount availability is displayed under the same relevant product");
				}
			} else {
				testStepFailed("Price of the product not displayed");
			}
		} catch (Exception e) {
			testStepFailed(
					"Could verify the presence of Subscription availability and discount under relevant product.");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the first product with subscribe label in the
	 * PGP page.
	 */
	public void clickProductWithSubscribeLabel() {
		try {
			if (isElementDisplayed(subscribeLabel_PGP)) {
				highLighterMethod(subscribeLabel_PGP);
				clickOn(subscribeLabel_PGP);
			} else {
				testStepFailed("Could not click on the first product with subscribe label",
						"Subscribe Label not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on the first product with subscribe labelw");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the presence of the subscribe label in the PDP
	 * Page.
	 */
	public void verifySubscribeLabel_PDP() {
		try {
			if (isElementDisplayed(subscribeLabel_PDP)) {
				testStepInfo("The Subcribe Label is displayed in the PDP Page");
				highLighterMethod(subscribeLabel_PDP);
			} else {
				testStepFailed("The Subcribe Label was not displayed in the PDP Page");
			}
		} catch (Exception e) {
			testStepFailed("The Subcribe Label was not displayed in the PDP Page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the the subscription drop down is not displayed
	 * and hence one time subscription is selected.
	 */
	public boolean verifyOneTimeSubscriptionIsSelected() {
		try {
			if (isElementDisplayed(oneTimePurchaseLabel)) {
				highLighterMethod(oneTimePurchaseLabel);
				String style = getAttributeValue(subscribeDropdownSection, "style");
				if (style.contains("none")) {
					testStepInfo("The Subcribe dropdown is not displayed and hence, one time purchase is selected");
					return true;
				} else {
					testStepFailed("The Subcribe dropdown is displayed and hence, one time purchase is not selected");
					return false;
				}

			} else {
				testStepFailed("The One time purchase Label was not displayed");
				return false;
			}
		} catch (Exception e) {
			testStepFailed("Could not verify whether one time purchase is selected");
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Description: Method to verify the subscribe drop down is displayed
	 */
	public void verifySubscribeDropdown() {
		try {
			if (isElementDisplayed(subscribeDropdown)) {
				highLighterMethod(subscribeDropdown);
				testStepInfo("The Subcribe Dropdown is displayed");
			} else {
				testStepFailed("The Subcribe Dropdown is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("The Subcribe Dropdown is not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the subscribe radio button
	 */
	public void clickSubscribeRadioButton() {
		try {
			if (isElementDisplayed(subscribeRadioButton)) {
				highLighterMethod(subscribeLabel_PDP);
				clickOn(subscribeRadioButton);
			} else {
				testStepFailed("The Subcribe radio button is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("The Subcribe radio button is not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the subscribe drop down and verify all
	 * options displayed
	 */
	public void verifySubscribeDropdownOptions() {
		try {
			if (isElementDisplayed(subscribeDropdown)) {
				highLighterMethod(subscribeDropdown);
				scrollToViewElement(subscribeDropdown);
				clickOn(subscribeDropdown);
				String[] options = { subscribeEveryMonthOption, subscribeEvery2MonthsOption,
						subscribeEvery3MonthsOption };
				for (String option : options) {
					if (isElementDisplayed(option)) {
						highLighterMethod(option);
						testStepInfo("The dropdown option - " + option.split("#xpath")[0] + " is displayed");
					} else {
						testStepFailed("The dropdown option - " + option.split("#xpath")[0] + " is not displayed");
					}
				}
				manualScreenshot("The options displayed in the subscription dropdown");
			} else {
				testStepFailed("The Subcribe dropdown is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("The Subcribe dropdown options could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to select an option from the subscription drop down
	 */
	public void selectOption_SubscriptionDropdown(String deliveryForEvery) {
		try {
			waitForElementToDisplay(subscribeDropdown, 60);
			String option_1 = getText(subscribeDropdown + "/option[1]");
			String option_2 = getText(subscribeDropdown + "/option[2]");
			String option_3 = getText(subscribeDropdown + "/option[3]");
			switch (deliveryForEvery) {
			case "month": {
				if (isElementDisplayed(subscribeDropdown)) {
					highLighterMethod(subscribeDropdown);
					selectFromDropdown(subscribeDropdown, option_1);
				} else {
					testStepFailed("The Subcribe Dropdown is not displayed");
				}
				break;
			}
			case "2 months": {
				if (isElementDisplayed(subscribeDropdown)) {
					highLighterMethod(subscribeDropdown);
					selectFromDropdown(subscribeDropdown, option_2);
				} else {
					testStepFailed("The Subcribe Dropdown is not displayed");
				}
				break;
			}
			case "3 months": {
				if (isElementDisplayed(subscribeDropdown)) {
					highLighterMethod(subscribeDropdown);
					selectFromDropdown(subscribeDropdown, option_3);
				} else {
					testStepFailed("The Subcribe Dropdown is not displayed");
				}
				break;
			}
			default: {
				testStepInfo("Requested option not present");
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Could not select an option from the Subcribe Dropdown");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Sign Up Now
	 */
	public void clickSignUpNow() {
		try {
			if (isElementDisplayed(signUpNow)) {
				highLighterMethod(signUpNow);
				clickOn(signUpNow);
			} else {
				testStepFailed("SignUpNow is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on SignUpNow");
			e.printStackTrace();
		}
	}
}
