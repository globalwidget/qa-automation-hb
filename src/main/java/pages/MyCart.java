package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class MyCart extends ApplicationKeywords {

	private static final String productsPresent = "Products Present #xpath=//td[@data-title='Product']/a";
	private static final String removeProduct = "Remove Product #xpath=//td[@class='product-remove']/a";
	private static final String alertMessage = "Alert #xpath=//div[@role='alert']";
	private static final String alertMessage_invalidCoupon = "Alert #xpath=//ul[@role='alert']";
	private static final String quantityUpdateBox = "Quantity Update Box #xpath=//input[contains(@id,'quantity')]";
	private static final String updateCart = "Update Cart button #xpath=//input[@value='Update cart']";
	private static final String couponCode = "Coupon Code #id=coupon_code";
	private static final String applyCoupon = "Coupon Code #name=apply_coupon";
	private static final String proceedCheckout = "Proceed Checkout #xpath=//a[contains(text(),'Proceed to checkout')]";
	private static final String removeCoupon = "Remove Coupon #xpath=//td[contains(@data-title,'Coupon')]//a[contains(@href,'remove')]";
	private static final String loginUsername = "Remove Coupon #xpath=//td[@class='lwa-username-input']/input";
	private static final String loginPassword = "Remove Coupon #xpath=//td[@class='lwa-password-input']/input";
	private static final String loginSubmit = "Remove Coupon #xpath=//input[@id='lwa_wp-submit']";
	private static final String pointsAvailableDisplay = "Points Available Display #xpath=//div[contains(text(),'YOU HAVE')]";
	private static final String pointRedemptionSection = "Point Redemption Section #xpath=//div[contains(@class,'checkout-redemption')]";
	private static final String dropdownButton_RedemptionOptions = "Dropdown button - Remdemption options #xpath=//div[contains(@class,'vs__actions')]";
	private static final String redemptionOptions = "Remdemption options #xpath=//ul[@id='vs1__listbox']";
	private static final String redeemButton = "Remdeem button #xpath=//span[contains(text(),'REDEEM')]/..";
	private static final String notEnoughPointsMessage = "Rewards Applied Amount #xpath=//div[contains(text(),\"You don't have enough points to redeem just yet\")]";
	private static final String subTotal = "Subtotal Amount #xpath=//tr[@class='cart-subtotal']/td//bdi";
	private static final String rewardsDiscount = "Rewards Discount Amount #xpath=//tr[contains(@class,'discount')]/td/span";
	private static final String total = "Total Amount #xpath=//tr[contains(@class,'order-total')]/td//bdi";
	private static final String removeRewardsApplied = "Total Amount #xpath=//tr[contains(@class,'discount')]//a[contains(@href,'remove')]";
	private static final String subscriptionPerMonthRadioButton = "Subscription Per Month radio button #xpath=//input[@type='radio' and contains(@value,'1_month')]";
	private static final String subscriptionPer2MonthsRadioButton = "Subscription Per 2 months radio button #xpath=//input[@type='radio' and contains(@value,'2_month')]";
	private static final String subscriptionPer3MonthsRadioButton = "Subscription Per 2 months radio button #xpath=//input[@type='radio' and contains(@value,'3_month')]";

	private static final String subscriptionPerMonthAmount = "Subscription Per Month amount #xpath=//input[@type='radio' and contains(@value,'1_month')]//following-sibling::span//span[contains(@class,'amount')]";
	private static final String subscriptionPer2MonthsAmount = "Subscription Per 2 months amount #xpath=//input[@type='radio' and contains(@value,'2_month')]//following-sibling::span//span[contains(@class,'amount')]";
	private static final String subscriptionPer3MonthsAmount = "Subscription Per 2 months amount #xpath=//input[@type='radio' and contains(@value,'3_month')]//following-sibling::span//span[contains(@class,'amount')]";

	public MyCart(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to verify presence of the expected product in the cart
	 * 
	 * @param ProductName: The name of the product that has been just added to the
	 *        cart.
	 */
	public void verifyPresenceOfExpectedProduct(String productName) {
		try {
			boolean productPresent = false;
			if (isElementPresent(productsPresent)) {
				manualScreenshot("The Product added is present in the cart");
				List<WebElement> products = new ArrayList<WebElement>();
				products = findWebElements(productsPresent);
				for (WebElement product : products) {
					if (product.getText().contains(productName)) {
						productPresent = true;
						highLighterMethod(product);
						break;
					}
				}
				if (productPresent == true) {
					testStepInfo("The product added to cart " + productName + ", found in My Cart page");
				} else
					testStepFailed("Product added to cart " + productName + ", not found in My Cart page");
			} else
				testStepFailed("No products present in My cart page");
		} catch (Exception e) {
			testStepFailed("Presence of expected product could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on delete button in My Cart page
	 */
	public String clickOnRemove() {
		String productName = "";
		try {
			if (isElementDisplayed(removeProduct)) {
				highLighterMethod(removeProduct);
				productName = findWebElements(productsPresent).get(0).getText();
				findWebElements(removeProduct).get(0).click();
				waitForElementToDisplay(alertMessage, 15);
				GOR.productAdded = false;
				testStepInfo("The remove button was clicked for the product " + productName);
				return productName;
			} else
				testStepFailed("Could not find any product in My Cart");
		} catch (Exception e) {
			testStepFailed("Could not remove products from cart");
			e.printStackTrace();
		}
		return productName;
	}

	/**
	 * Description: Method to verify expected message is displayed in the Alert
	 * 
	 * @param: The messages whose presence are to be verified in the alert.
	 */
	public void verifyExpectedMessageAlert(String message1, String message2, String message3, int index) {
		try {
			String alertMessageWithIndex = alertMessage + "[" + index + "]";
			waitForElementToDisplay(alertMessageWithIndex, 200);
			if (isElementDisplayed(alertMessageWithIndex)) {
				highLighterMethod(alertMessageWithIndex);
				manualScreenshot("Alert message displayed");
				if (getText(alertMessageWithIndex).contains(message1)
						&& getText(alertMessageWithIndex).contains(message2)
						&& getText(alertMessageWithIndex).contains(message3))
					testStepInfo("Relevant Alert Message was displayed");
				else
					testStepFailed("Relevant Alert Message was not displayed");

			} else
				testStepFailed("No Alert message was displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate expected Messages");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify expected message is displayed in the Alert for
	 * Invalid Coupon
	 * 
	 * @param: The messages whose presence are to be verified in the alert.
	 */
	public void verifyExpectedMessageAlert_InvalidCoupon(String message1, String message2, String message3) {
		try {
			waitForElementToDisplay(alertMessage_invalidCoupon, 60);
			if (isElementDisplayed(alertMessage_invalidCoupon)) {
				highLighterMethod(alertMessage_invalidCoupon);
				manualScreenshot("Alert Message is displayed");
				if (getText(alertMessage_invalidCoupon).contains(message1)
						&& getText(alertMessage_invalidCoupon).contains(message2)
						&& getText(alertMessage_invalidCoupon).contains(message3))
					testStepInfo("Relevant Alert Message was displayed");
				else
					testStepFailed("Relevant Alert Message was not displayed");

			} else
				testStepFailed("No Alert message was displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate expected messages");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to update the cart quantity of the first product in the
	 * cart
	 */
	public void updateCartQuantity() {
		try {
			if (isElementDisplayed(updateCart)) {
				checkStatusUpdateCart();
				if (isElementDisplayed(quantityUpdateBox)) {
					highLighterMethod(quantityUpdateBox);
					findWebElements(quantityUpdateBox).get(0).clear();
					findWebElements(quantityUpdateBox).get(0).sendKeys("5");
					checkStatusUpdateCart();
				} else
					testStepFailed("The box to update quantity of the product is not found");
			}
		} catch (Exception e) {
			testStepFailed("Could not update quantity of the product");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check the status of Update Cart - disabled or enabled
	 */
	public void checkStatusUpdateCart() {
		try {
			if (getAttributeValue(updateCart, "aria-disabled").contains("true")) {
				testStepInfo("The Update Cart button is disabled");
			} else
				testStepInfo("The Update Cart button is not disabled");
		} catch (Exception e) {
			testStepInfo("The Cart button status could not fetched");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Update Cart
	 */
	public void clickUpdateCart() {
		try {
			if (isElementDisplayed(updateCart)) {
				highLighterMethod(updateCart);
				clickOn(updateCart);
			} else
				testStepFailed("The Update Cart button is not displayed");
		} catch (Exception e) {
			testStepFailed("The Update Cart button could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to enter the coupon code
	 * 
	 * @param The coupon code to be entered.
	 */
	public void enterCouponCode(String couponCode_data) {
		try {
			if (isElementDisplayed(couponCode)) {
				highLighterMethod(couponCode);
				typeIn(couponCode, couponCode_data);
			} else
				testStepFailed("The Coupon Code text box was not displayed");
		} catch (Exception e) {
			testStepFailed("The Coupon Code could not be entered");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Apply Coupon
	 */
	public void clickApplyCoupon() {
		try {
			if (isElementDisplayed(applyCoupon)) {
				highLighterMethod(applyCoupon);
				clickOn(applyCoupon);
			} else
				testStepFailed("The Apply Coupon button was not displayed");
		} catch (Exception e) {
			testStepFailed("Apply Coupon could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to remove all products in My Cart page
	 */
	public void removeAllProducts() {
		List<WebElement> removeProducts = new ArrayList<WebElement>();
		try {
			removeProducts = new ArrayList<WebElement>();
			removeProducts = findWebElements(removeProduct);
			int totalProducts = removeProducts.size();

			if (totalProducts > 0) {
				GOR.productAdded = false;
				for (int removeCounter = 0; removeCounter < totalProducts; removeCounter++) {
					removeProducts = new ArrayList<WebElement>();
					removeProducts = findWebElements(removeProduct);
					for (WebElement removeTheProduct : removeProducts) {
						highLighterMethod(removeTheProduct);
						waitForElementToDisplay(removeProduct, 20);
						removeTheProduct.click();
						waitForElementToDisplay(alertMessage, 20);
						break;
					}
					if (removeCounter != totalProducts - 1) {
						removeProducts = new ArrayList<WebElement>();
						removeProducts = findWebElements(MyCart.removeProduct);
					}
				}
				testStepInfo("All Products Removed");
			} else
				testStepInfo("No Products in Cart");
		} catch (Exception e) {
			testStepFailed("All products removal from cart was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Proceed to Checkout
	 */
	public void clickProceedToCheckOut() {
		try {
			if (isElementDisplayed(proceedCheckout)) {
				highLighterMethod(proceedCheckout);
				clickOn(proceedCheckout);
			} else
				testStepFailed("Proceed to Checkout was not displayed");
		} catch (Exception e) {
			testStepFailed("Proceed to checkout could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Remove Coupon, if present
	 */
	public void removeCoupon() {
		try {
			if (findWebElements(removeCoupon).size() > 0) {
				highLighterMethod(removeCoupon);
				clickOn(removeCoupon);
			} else
				testStepInfo("Remove Coupon was not present");
		} catch (Exception e) {
			testStepFailed("Remove Coupon could not be performed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to fetch the available points displayed.
	 * 
	 */
	public int fetchPoints() {
		try {
			waitForElementToDisplay(pointsAvailableDisplay, 5);
			scrollToViewElement(pointsAvailableDisplay);
			if (isElementDisplayed(pointsAvailableDisplay)) {
				highLighterMethod(pointsAvailableDisplay);
				int points = Integer
						.parseInt(getText(pointsAvailableDisplay).split("HAVE")[1].split("POINTS")[0].trim());
				return points;
			} else {
				testStepFailed("Points available could not be fetched", "Points available not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Points available could not be fetched");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Description: Method is used to check if the points are same.
	 * 
	 */
	public void comparePoints(int rewardsPoints, int myCartPoints) {
		try {
			if (rewardsPoints == myCartPoints) {
				testStepInfo("The valid current points is displayed in the My Cart page");
			} else {
				testStepFailed("The valid current points is not displayed in the My Cart page");
			}
		} catch (Exception e) {
			testStepFailed("The valid current points is displayed in the My Cart page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to login from the My Cart Page.
	 * 
	 */
	public void login(String username, String password) {
		try {
			if (isElementDisplayed(loginUsername)) {
				highLighterMethod(loginUsername);
				typeIn(loginUsername, username);
				highLighterMethod(loginPassword);
				typeIn(loginPassword, password);
				highLighterMethod(loginSubmit);
				clickOn(loginSubmit);
				GOR.loggedIn = true;
			} else {
				testStepFailed("Could not complete login from My Cart Page", "Username textbox not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not complete login from My Cart Page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check if the points redemption section is displayed.
	 */
	public void checkPointsRedemptionSection() {
		try {
			if (isElementDisplayed(pointRedemptionSection)) {
				highLighterMethod(pointRedemptionSection);
				testStepInfo("Points Redemption Section was displayed");
			} else
				testStepFailed("Points Redemption Section was not displayed");
		} catch (Exception e) {
			testStepFailed("Points Redemption Section was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check if the points redemption section is not
	 * displayed.
	 */
	public void checkPointsRedemptionSectionAbsence() {
		try {
			waitForElement(couponCode, 20);
			waitTime(6);
			if (findWebElements(pointRedemptionSection).size() == 0) {
				testStepInfo("Redemption Section is not displayed in the My Cart page");
			} else
				testStepFailed("Redemption Section is displayed in the My Cart page");
		} catch (Exception e) {
			testStepFailed("Redemption Section is displayed in the My Cart page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to choose a Reward from the drop down .
	 * 
	 */
	public void chooseReward(int index) {
		try {
			waitForElementToDisplay(pointRedemptionSection, 5);
			scrollToViewElement(pointRedemptionSection);
			if (isElementDisplayed(dropdownButton_RedemptionOptions)) {
				highLighterMethod(dropdownButton_RedemptionOptions);
				clickOn(dropdownButton_RedemptionOptions);
				findWebElement(redemptionOptions).findElements(By.xpath(".//*")).get(index).click();
			} else {
				testStepFailed("Could not select a reward option", "Rewards dropdown not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not select a reward option");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Redeem button.
	 * 
	 */
	public void clickRedeem() {
		try {
			if (isElementDisplayed(redeemButton)) {
				highLighterMethod(redeemButton);
				clickOn(redeemButton);
				GOR.rewardsApplied_150Points = true;
			} else {
				testStepFailed("Could not click on Redeem button", "Redeem button not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Redeem button");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify the points redemption drop down menu.
	 * 
	 */
	public void verifypointsredemptionDropdown() {
		try {
			waitForElementToDisplay(pointRedemptionSection, 5);
			scrollToViewElement(pointRedemptionSection);
			if (isElementDisplayed(dropdownButton_RedemptionOptions)) {
				highLighterMethod(dropdownButton_RedemptionOptions);
				clickOn(dropdownButton_RedemptionOptions);
				if (findWebElement(redemptionOptions).findElements(By.xpath(".//*")).size() > 0) {
					manualScreenshot("The Points redemption options available");
					testStepInfo("User is able to see the redemptions drop down options in the cart page ");
				} else {
					testStepFailed("User is not able to see the redemptions drop down options in the cart page ");
				}
			}
		} catch (Exception e) {
			testStepFailed("Could not verify the points redemption drop down menu");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify if coupon section is disabled.
	 * 
	 */
	public void checkCouponSectionIsDisabled() {
		try {
			waitForElementToDisplay(couponCode, 5);
			if (isElementDisplayed(couponCode)) {
				if (getAttributeValue(couponCode, "readonly").contains("true")
						&& getAttributeValue(applyCoupon, "disabled").contains("true")) {
					highLighterMethod(couponCode);
					highLighterMethod(applyCoupon);
					testStepInfo("The Coupon Section is disabled");
				} else {
					testStepFailed("The Coupon Section is not disabled");
				}
			}
		} catch (Exception e) {
			testStepFailed("The Coupon Section is not disabled");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify the rewards amount applied.
	 * 
	 */
	public void rewardsAmountApplied(String amount, String rewardsPoints) {
		try {
			waitForElementToDisplay(rewardsDiscount, 5);
			if (isElementDisplayed(rewardsDiscount)) {
				String amountDisplayed = getText(rewardsDiscount);
				if (amountDisplayed.contains(amount)) {
					scrollToViewElement(rewardsDiscount);
					highLighterMethod(rewardsDiscount);
					testStepInfo("Selecting the reward" + rewardsPoints + "points from the drop down is applying the $"
							+ amount + " discount");
				} else {
					testStepFailed("Selecting the reward" + rewardsPoints
							+ "points from the drop down is not applying the $" + amount + " discount");
				}
			}
		} catch (Exception e) {
			testStepFailed("Selecting the reward" + rewardsPoints + "points from the drop down is not applying the $"
					+ amount + " discount");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify if the rewards amount is applied to the
	 * total amount.
	 * 
	 */
	public void checkTotalAmount() {
		try {
			if (isElementDisplayed(rewardsDiscount) && isElementDisplayed(subTotal) && isElementDisplayed(total)) {
				float discount = Float.parseFloat(getText(rewardsDiscount).substring(1));
				float subtotal = Float.parseFloat(getText(subTotal).substring(1));
				float total = Float.parseFloat(getText(MyCart.total).substring(1));
				if (total == subtotal - discount) {
					scrollToViewElement(updateCart);
					highLighterMethod(rewardsDiscount);
					highLighterMethod(subTotal);
					highLighterMethod(MyCart.total);
					testStepInfo("The subtotal is getting updated respectively based on the rewards discount applied");
				} else {
					testStepFailed(
							"The subtotal is not getting updated respectively based on the rewards discount applied");
				}
			}
		} catch (Exception e) {
			testStepFailed("The subtotal is not getting updated respectively based on the rewards discount applied");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on remove Rewards Applied button.
	 * 
	 */
	public void clickRemove_Rewards() {
		try {
			if (isElementDisplayed(removeRewardsApplied)) {
				highLighterMethod(removeRewardsApplied);
				clickOn(removeRewardsApplied);
			} else {
				testStepFailed("Could not click on remove button for rewards applied",
						"Remove button for rewards applied not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on remove button for rewards applied");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify the not enough points message and
	 * absence of redemption drop down.
	 * 
	 */
	public void verifyNotEnoughPoints() {
		try {
			if (isElementDisplayed(notEnoughPointsMessage)) {
				scrollToViewElement(notEnoughPointsMessage);
				highLighterMethod(notEnoughPointsMessage);
				if (findWebElements(dropdownButton_RedemptionOptions).size() == 0
						&& findWebElements(redeemButton).size() == 0) {
					testStepInfo("The dropdown for points redemptions is not displayed due to lack of points");
				} else {
					testStepFailed("The dropdown for points redemptions is displayed inspite of lack of points");
				}
			} else {
				testStepFailed("The feature for lack of points to redeem is not verified",
						"The relevant message is not displayed for lack of points");
			}
		} catch (Exception e) {
			testStepFailed("The feature for lack of points to redeem is not verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify subscription per month radio button is
	 * selected
	 */
	public boolean verifySubscriptionSelectedIsPerMonth() {
		try {
			if (isElementDisplayed(subscriptionPerMonthRadioButton)) {
				String checked = getAttributeValue(subscriptionPerMonthRadioButton, "checked");
				if (checked.contains("true")) {
					highLighterMethod(subscriptionPerMonthRadioButton + "/..");
					testStepInfo("The Subscription per month option is selected");
					return true;
				}
			} else {
				testStepFailed("The Subscription per day month is not displayed");
				return false;
			}
		} catch (Exception e) {
			testStepFailed("Could not verify whether the Subscription per month option is selected");
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 * Description: Method is used to click on desired subscription option in the
	 * cart page and return the price for the subscription.
	 * 
	 */
	public String selectSubscription(String option) {
		try {
			String price = null;
			switch (option) {
			case "month": {
				if (isElementDisplayed(subscriptionPerMonthRadioButton)) {
					highLighterMethod(subscriptionPerMonthRadioButton);
					clickOn(subscriptionPerMonthRadioButton);
					price = getText(subscriptionPerMonthAmount).substring(1);
					return price;
				} else {
					testStepFailed("Could not click on radio button for monthly subscription",
							"Radio button for monthly subscription not displayed");
				}
				break;
			}

			case "2Months": {
				if (isElementDisplayed(subscriptionPer2MonthsRadioButton)) {
					highLighterMethod(subscriptionPer2MonthsRadioButton);
					clickOn(subscriptionPer2MonthsRadioButton);
					price = getText(subscriptionPer2MonthsAmount).substring(1);
					return price;
				} else {
					testStepFailed("Could not click on radio button for 2 month subscription",
							"Radio button for 2 month subscription not displayed");
				}
				break;
			}

			case "3Months": {
				if (isElementDisplayed(subscriptionPer3MonthsRadioButton)) {
					highLighterMethod(subscriptionPer3MonthsRadioButton);
					clickOn(subscriptionPer3MonthsRadioButton);
					price = getText(subscriptionPer3MonthsAmount).substring(1);
					return price;
				} else {
					testStepFailed("Could not click on radio button for 3 months subscription",
							"Radio button for 3 months subscription not displayed");
				}
				break;
			}

			default:
				break;
			}

		} catch (Exception e) {
			testStepFailed("Could not select the desired subscription option");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method is used to verify the sub total
	 * 
	 */
	public void verifySubtotal(String amount) {
		try {
			waitTime(3);
			if (isElementDisplayed(subTotal)) {
				String subtotal = getText(subTotal).substring(1);
				if (Float.parseFloat(subtotal) == Float.parseFloat(amount)) {
					scrollToViewElement(subTotal);
					highLighterMethod(subTotal);
					testStepInfo("The percentage offer of the subscription is applied to the product");
				} else {
					testStepFailed("The percentage offer of the subscription is not applied to the product");
				}
			} else {
				testStepFailed("Could not verify the subtotal", "Subtotal is not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify the subtotal");
			e.printStackTrace();
		}
	}
}
