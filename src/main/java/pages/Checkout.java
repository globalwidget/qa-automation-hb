package pages;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.openqa.selenium.Keys;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Checkout extends ApplicationKeywords {

	private static final String clickToLogin = "Click To Login #xpath=//a[contains(text(),'Returning customer? Click here to login')]";
	private static final String inputUsername_Login = "Login Username #id=username";
	private static final String inputPassword_Login = "Login Password #id=password";
	private static final String loginButton = "Login Button #xpath=//button[contains(text(),'Login')]";
	private static final String firstName_Billing = "First Name Billing #id=billing_first_name";
	private static final String lastName_Billing = "Last Name Billing #id=billing_last_name";
	private static final String company_Billing = "Company Name Billing #id=billing_company";
	private static final String phoneNumber_Billing = "Phone Number Billing #id=billing_phone";
	private static final String shipDifferentAddress_checkBox = "Ship Different Address Checkbox #id=ship-to-different-address-checkbox";
	private static final String placeOrder = "Place Order #id=place_order";
	private static final String countrySelect_Billing = "Country Billing #id=billing_country";
	private static final String country_Billing = "Country Billing #id=select2-billing_country-container";
	private static final String streetAddress_Billing = "Street Address Billing #id=billing_address_1";
	private static final String streetAddressNextLine_Billing = "Street Address Next Line Billing #id=billing_address_2";
	private static final String postCode_Billing = "Post Code Billing #id=billing_postcode";
	private static final String city_Billing = "City Billing #id=billing_city";
	private static final String stateSelect_Billing = "State Billing #id=billing_state";
	private static final String state_Billing = "State Billing #id=select2-billing_state-container";
	private static final String email_Billing = "Email Billing #id=billing_email";
	private static final String signUpForEmails_checkBox = "Sign Up For Emails CheckBox #id=kl_newsletter_checkbox";
	private static final String firstName_Shipping = "First Name Shipping #id=shipping_first_name";
	private static final String lastName_Shipping = "Last Name Shipping #id=shipping_last_name";
	private static final String company_Shipping = "Company Name Shipping #id=shipping_company";
	private static final String countrySelect_Shipping = "Country Shipping #id=shipping_country";
	private static final String country_Shipping = "Country Shipping #id=select2-shipping_country-container";
	private static final String streetAddress_Shipping = "Street Address Shipping #id=shipping_address_1";
	private static final String streetAddressNextLine_Shipping = "Street Address Next Line Shipping #id=shipping_address_2";
	private static final String postCode_Shipping = "Post Code Shipping #id=shipping_postcode";
	private static final String city_Shipping = "City Shipping #id=shipping_city";
	private static final String stateSelect_Shipping = "State Shipping #id=shipping_state";
	private static final String state_Shipping = "State Shipping #id=select2-shipping_state-container";
	private static final String error_InvalidEmailAddress = "Invalid Email Error #xpath=//li[contains(text(),'Invalid billing email address')]";
	private static final String orderNotes = "Order Notes Shipping #id=order_comments";
	private static final String cardNumber = "Card Number #xpath=//input[contains(@id,'wc-authorize-net-cim-credit-card-account-number')]";
	private static final String expiration = "Expiration #xpath=//input[contains(@id,'wc-authorize-net-cim-credit-card-expiry')]";
	private static final String cardSecurityCode = "Card Security Code #xpath=//input[contains(@id,'wc-authorize-net-cim-credit-card-csc')]";
	private static final String successCheckout = "Checkout Success Message #xpath=//p[contains(text(),'Your order has been received')]";
	private static final String shippinngAddressBlock = "Shipping Address Block #xpath=//div[contains(@class,'shipping_address')]";
	private static final String termsAndConditions_checkbox = "Terms and Conditions #id=terms";
	private static final String error_cardNumberWrongLength = "Card Number Wrong Length Message #xpath=//li[contains(text(),'Card number is invalid (wrong length)')]";
	private static final String error_cardNumberInvalid = "Card Number Invalid Message #xpath=//li[contains(text(),'Card number is invalid')][2]";
	private static final String error_expirationDataInvalid = "Expiration Data Invalid Message #xpath=//li[contains(text(),'Card expiration date is invalid')]";
	private static final String error_SecurityCodeWrongLength = "Security Code Wrong Length  Message #xpath=//li[contains(text(),'Card security code is invalid (must be 3 or 4 digi')]";
	private static final String error_billingPostcode = "Billing PostCode Error #xpath=//li[@data-id='billing_postcode']";
	private static final String error_shippingPostcode = "Shipping PostCode Error #xpath=//li[@data-id='shipping_postcode']";

	private static final String error_CardSecurityCodeMissing = "Shipping PostCode Error #xpath=//li[contains(text(),'Card security code is missing')]";
	private static final String error_CardNumberMissing = "Card Number Missing Error #xpath=//li[contains(text(),'Card number is missing')]";
	private static final String error_BillingFirstNameMissing = "Billing First Name Missing #xpath=//li[@data-id='billing_first_name']";
	private static final String error_BillingLastNameMissing = "Billing Last Name Missing #xpath=//li[@data-id='billing_last_name']";
	private static final String error_BillingStreetAddressMissing = "Billing Street Address Missing #xpath=//li[@data-id='billing_address_1']";
	private static final String error_BillingCityMissing = "Billing City Missing #xpath=//li[@data-id='billing_city']";
//	private static final String error_BillingCountryInvalid = "Billing Country Invalid #xpath=//li[contains(text(),'is not a valid country code')][1]";
	private static final String error_BillingCountryMissing = "Billing Country Missing #xpath=//li[@data-id='billing_country']";
	private static final String error_BillingStateMissing = "Billing Country Missing #xpath=//li[@data-id='billing_state']";
	private static final String error_BillingPostCodeMissing = "Billing Post Code Missing #xpath=//li[@data-id='billing_postcode']";
	private static final String error_BillingPhoneMissing = "Billing Phone Number Missing #xpath=//li[@data-id='billing_phone']";
	private static final String error_BillingEmailMissing = "Billing Email Missing #xpath=//li[@data-id='billing_email']";

	private static final String error_ShippingFirstNameMissing = "Shipping First Name Missing #xpath=//li[@data-id='shipping_first_name']";
	private static final String error_ShippingLastNameMissing = "Shipping Last Name Missing #xpath=//li[@data-id='shipping_last_name']";
	private static final String error_ShippingStreetAddressMissing = "Shipping Street Address Missing #xpath=//li[@data-id='shipping_address_1']";
	private static final String error_ShippingCityMissing = "Shipping City Missing #xpath=//li[@data-id='shipping_city']";
//	private static final String error_ShippingCountryInvalid = "Shipping Country Invalid #xpath=//li[contains(text(),'is not a valid country code')][2]";
	private static final String error_ShippingCountryMissing = "Shipping Country Missing #xpath=//li[@data-id='shipping_country']";
	private static final String error_ShippingStateMissing = "Shipping Country Missing #xpath=//li[@data-id='shipping_state']";
	private static final String error_ShippingPostCodeMissing = "Shipping Post Code Missing #xpath=//li[@data-id='shipping_postcode']";
	private static final String error_termsAndConditionsUnchekced = "Terms and Conditions Unchecked #xpath=//li[contains(text(),'Please read and accept the terms and conditions to')]";
	private static final String enterAnAddress = "Do not ship to default Error #xpath=//li[contains(text(),'Please enter an address to continue')]";

	private static final String salesTax = "Sales Tax #xpath=//tr[contains(@class,'sales-tax') and not(contains(@class,'recurring'))]";
	private static final String salesTax_RecurringTotal = "Sales tax - Recurring total #xpath=//tr[contains(@class,'recurring') and contains(@class,'tax')]";
	private static final String subtotal_RecurringTotal = "Subtotal - Recurring total #xpath=//tr[contains(@class,'subtotal') and contains(@class,'recurring')]";
	private static final String heading_RecurringTotal = "Heading - Recurring total #xpath=//th[contains(text(),'Recurring totals')]";
	private static final String shipping_RecurringTotal = "Shipping - Recurring total #xpath=//tr[contains(@class,'shipping ')]";
	private static final String total_RecurringTotal = "Total - Recurring total #xpath=//tr[contains(@class,'order-total') and contains(@class,'recurring')]";
	private static final String createAccountPassword = "Create Account Password #xpath=//input[@id='account_password']";
	private static final String error_createAccountPassword = "Create Account Password - Empty Error #xpath=//li[@data-id='account_password']";
	private static final String subscriptionStatusMessage = "Subscription status Message #xpath=//a[contains(@href,'my-account')]/../../p[contains(text(),'View the status')]";
	private static final String orderNumberMainOrder = "Order Number #xpath=//li[contains(text(),'Order')]/strong";
	private static final String orderNumberSubscriptionOrder = "Order Number #xpath=//td[@data-title='ID']/a";
	private static final String newCreditCard_radioButton = "Order Number #xpath=//input[contains(@id,'new-payment')]";

	public Checkout(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to fill the details required for billing
	 */
	public void fillDetails(HashMap<String, String> billingData, HashMap<String, String> shippingData) {
		try {
			if (isElementDisplayed(firstName_Billing)) {
				Map<String, String> data_Details_Billing = new LinkedHashMap<String, String>(14);
				data_Details_Billing.put(firstName_Billing, billingData.get("firstName_data"));
				data_Details_Billing.put(lastName_Billing, billingData.get("lastName_data"));
				data_Details_Billing.put(company_Billing, billingData.get("companyName_data"));
				data_Details_Billing.put(phoneNumber_Billing, billingData.get("phoneNumber_data"));
				data_Details_Billing.put(countrySelect_Billing, billingData.get("billingCountry_data"));
				data_Details_Billing.put(streetAddress_Billing, billingData.get("streetAddress_data"));
				data_Details_Billing.put(streetAddressNextLine_Billing, billingData.get("streetAddressNextLine_data"));
				data_Details_Billing.put(postCode_Billing, billingData.get("postCode_data"));
				data_Details_Billing.put(city_Billing, billingData.get("city_data"));
				data_Details_Billing.put(stateSelect_Billing, billingData.get("state_data"));
				data_Details_Billing.put(email_Billing, billingData.get("email_data"));
				for (Map.Entry<String, String> entry : data_Details_Billing.entrySet()) {
					if (isElementDisplayed(entry.getKey())) {
						if (entry.getValue() == null || entry.getValue().isEmpty())
							continue;
						else {
							if (entry.getKey().contains("Country") || entry.getKey().contains("State")) {
								{
									highLighterMethod(entry.getKey());
									selectFromDropdown(entry.getKey(), entry.getValue());
								}
							} else {
								highLighterMethod(entry.getKey());
								clearEditBox(entry.getKey());
								typeIn(entry.getKey(), entry.getValue());
							}
						}

					} else {
						testStepFailed(entry.getKey().split("#")[0] + " field was not found");
					}
				}
				if (isElementDisplayed(signUpForEmails_checkBox)) {
					highLighterMethod(signUpForEmails_checkBox);
					testStepInfo("The Sign Up for Emails checkbox was displayed");
				} else {
					testStepFailed("The Sign Up for Emails checkbox was not displayed");
				}
				if (isElementDisplayed(shipDifferentAddress_checkBox)) {
					highLighterMethod(shipDifferentAddress_checkBox);
					testStepInfo("The ship for different address checkbox was displayed");
					if (GOR.shipDifferentAddress == true) {
						if (getAttributeValue(shippinngAddressBlock, "style").contains("none"))
							clickOn(shipDifferentAddress_checkBox);
						waitTime(2);
						if (!(getAttributeValue(shippinngAddressBlock, "style").contains("none"))) {
							Map<String, String> data_Details_shipping = new LinkedHashMap<String, String>(14);
							data_Details_shipping.put(firstName_Shipping, shippingData.get("firstName_data"));
							data_Details_shipping.put(lastName_Shipping, shippingData.get("lastName_data"));
							data_Details_shipping.put(company_Shipping, shippingData.get("companyName_data"));
							data_Details_shipping.put(countrySelect_Shipping, shippingData.get("billingCountry_data"));
							data_Details_shipping.put(streetAddress_Shipping, shippingData.get("streetAddress_data"));
							data_Details_shipping.put(streetAddressNextLine_Shipping,
									billingData.get("streetAddressNextLine_data"));
							data_Details_shipping.put(postCode_Shipping, shippingData.get("postCode_data"));
							data_Details_shipping.put(city_Shipping, shippingData.get("city_data"));
							data_Details_shipping.put(stateSelect_Shipping, shippingData.get("state_data"));
							data_Details_shipping.put(orderNotes, shippingData.get("orderNotes_data"));
							for (Map.Entry<String, String> entry : data_Details_shipping.entrySet()) {
								if (isElementDisplayed(entry.getKey())) {
									if (entry.getValue() == null || entry.getValue().isEmpty())
										continue;
									else {
										if (entry.getKey().contains("Country") || entry.getKey().contains("State")) {
											{
												highLighterMethod(entry.getKey());
												selectFromDropdown(entry.getKey(), entry.getValue());
											}
										} else {
											highLighterMethod(entry.getKey());
											clearEditBox(entry.getKey());
											typeIn(entry.getKey(), entry.getValue());
										}
									}

								} else {
									testStepFailed(entry.getKey().split("#")[0] + " field was not found");
								}
							}
						} else {
							testStepFailed("The Shipping Address block was not displayed");
						}
					}
				} else {
					testStepFailed("The ship for different address checkbox was not displayed");
				}

			} else
				testStepFailed("First Name field was not found");
		} catch (Exception e) {
			testStepFailedDB("Could fill details successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to clear billing and shipping Details.
	 */
	public void ClearDetails() {
		try {
			String[] fields_Billing = { firstName_Billing, lastName_Billing, company_Billing, phoneNumber_Billing,
					countrySelect_Billing, streetAddress_Billing, streetAddressNextLine_Billing, postCode_Billing,
					city_Billing, stateSelect_Billing, email_Billing };
			if (isElementDisplayed(firstName_Billing)) {
				for (String field_Billing : fields_Billing) {
					if (isElementDisplayed(field_Billing)) {
						if (field_Billing.contains("Country")) {
							highLighterMethod(field_Billing);
							selectFromDropdown(field_Billing, 0);
						} else if (field_Billing.contains("State")) {
							selectFromDropdown(countrySelect_Billing, "United States (US)");
							waitForElementToDisplay(field_Billing, 20);
							if (isElementDisplayed(field_Billing)) {
								highLighterMethod(field_Billing);
								selectFromDropdown(field_Billing, 0);
								waitTime(3);
								selectFromDropdown(countrySelect_Billing, 0);
							}
						} else {
							highLighterMethod(field_Billing);
							clearEditBox(field_Billing);
						}
					} else {
						testStepFailed(field_Billing.split("#")[0] + " field was not found");
					}
				}
				if (GOR.shipDifferentAddress == true) {
					waitTime(2);
					if (!(getAttributeValue(shippinngAddressBlock, "style").contains("none"))) {
						String[] fields_Shipping = { firstName_Shipping, lastName_Shipping, company_Shipping,
								countrySelect_Shipping, streetAddress_Shipping, streetAddressNextLine_Shipping,
								postCode_Shipping, city_Shipping, stateSelect_Shipping, orderNotes };
						if (isElementDisplayed(firstName_Shipping)) {
							for (String field_Shipping : fields_Shipping) {
								if (isElementDisplayed(field_Shipping)) {
									if (field_Shipping.contains("Country")) {
										highLighterMethod(field_Shipping);
										selectFromDropdown(field_Shipping, 0);
									} else if (field_Shipping.contains("State")) {
										selectFromDropdown(countrySelect_Shipping, "United States (US)");
										waitForElementToDisplay(field_Shipping, 20);
										if (isElementDisplayed(field_Shipping)) {
											highLighterMethod(field_Shipping);
											selectFromDropdown(field_Shipping, 0);
											waitTime(3);
											selectFromDropdown(countrySelect_Shipping, 0);
										}
									} else {
										highLighterMethod(field_Shipping);
										clearEditBox(field_Shipping);
									}
								} else {
									testStepFailed(field_Shipping.split("#")[0] + " field was not found");
								}
							}
						}
					} else {
						testStepFailed("The Shipping Address block was not displayed");
					}
				}
			}
			if (GOR.loggedIn == true && GOR.securityLogin == false && GOR.completeCheckoutWithSavedCard == true) {
				if (findWebElements(newCreditCard_radioButton).size() > 0) {
					highLighterMethod(newCreditCard_radioButton);
					clickOn(newCreditCard_radioButton);
				}
			}
			if (isElementDisplayed(cardNumber)) {
				highLighterMethod(cardNumber);
				clearEditBox(cardNumber);
				highLighterMethod(expiration);
				clearEditBox(expiration);
				highLighterMethod(cardSecurityCode);
				clearEditBox(cardSecurityCode);

			} else
				testStepFailed("First Name field was not found");
		} catch (Exception e) {
			testStepFailed("The fields could not be cleared successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to fill the details for credit card payment
	 */
	public void fillDetailsforCreditCard(String cardNumber_data, String expirationMonth_data,
			String expirationYear_data, String cardSecurityCode_data) {
		try {
			waitForElementToDisplay(cardNumber, 15);
			if (isElementDisplayed(cardNumber)) {
				highLighterMethod(cardNumber);
				clearEditBox(cardNumber);
				typeNumbersFromKeyboard(cardNumber, cardNumber_data);
				testStepInfo("Card number data was entered in the relevant field");
				highLighterMethod(expiration);
				clearEditBox(expiration);
				typeNumbersFromKeyboard(expiration, expirationMonth_data);
				typeNumbersFromKeyboard(expiration, expirationYear_data);
				testStepInfo("Expiration data was entered in the relevant field");
				highLighterMethod(cardSecurityCode);
				clearEditBox(cardSecurityCode);
				typeIn(cardSecurityCode, cardSecurityCode_data);
			} else {
				testStepFailed("The Card Number field was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Details could not be filled successfully for credit card");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to tick terms and conditions check box
	 */
	public void clickTermsAndConditions() {
		try {
			if (isElementDisplayed(termsAndConditions_checkbox)) {
				highLighterMethod(termsAndConditions_checkbox);
				clickOn(termsAndConditions_checkbox);
			} else {
				testStepFailed("Terms and conditions check box was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on terms and conditions");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on place order
	 */
	public void clickPlaceOrder() {
		try {
			if (isElementDisplayed(placeOrder)) {
				highLighterMethod(placeOrder);
				clickOn(placeOrder);
			} else {
				testStepFailed("Place Order button was not displayed");
			}
		} catch (Exception e) {
			testStepFailedDB("Place Order could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the success message after order is placed.
	 */
	public String verifySuccessMessage() {
		try {
			waitForElementToDisplay(successCheckout, 50);
			if (isElementDisplayed(successCheckout)) {
				GOR.productAdded = false;
				testStepInfo("The Success Message for checkout was displayed.");
				highLighterMethod(successCheckout);
				manualScreenshot("Success Message");
				highLighterMethod(orderNumberMainOrder);
				return getText(orderNumberMainOrder);
			} else {
				testStepFailed("The Success Message for checkout was not displayed.");
			}
		} catch (Exception e) {
			testStepFailed("The Success Message for checkout was not displayed.");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to verify the display of relevant message.
	 */
	public String verifyRelevantMessage(String error) {
		try {
			switch (error) {
			case "Wrong Length Card Number": {
				if (isElementDisplayed(error_cardNumberWrongLength)) {
					testStepInfo("The Error Message for Wrong length of Card Number was displayed.");
					highLighterMethod(error_cardNumberWrongLength);
				} else {
					testStepFailed("The Error Message for Wrong length of Card Number was not displayed.");
				}
				break;
			}
			case "Invalid Card Number": {
				if (isElementDisplayed(error_cardNumberInvalid)) {
					testStepInfo("The Error Message for Invalid Card Number was displayed");
					highLighterMethod(error_cardNumberInvalid);
				} else {
					testStepFailed("The Error Message for Invalid Card Number was not displayed.");
				}
				break;
			}
			case "Invalid Expiration Data": {
				if (isElementDisplayed(error_expirationDataInvalid)) {
					testStepInfo("The Error Message for Invalid Expiration Data was displayed.");
					highLighterMethod(error_expirationDataInvalid);
				} else {
					testStepFailed("The Error Message for Invalid Expiration Data was not displayed.");
				}
				break;
			}
			case "Invalid Security Code": {
				if (isElementDisplayed(error_SecurityCodeWrongLength)) {
					testStepInfo("The Error Message for Wrong Length of Security Code was displayed.");
					highLighterMethod(error_SecurityCodeWrongLength);
				} else {
					testStepFailed("The Error Message for Wrong Length of Security Code was displayed.");
				}
				break;
			}
			case "Invalid Billing Post Code": {
				waitForElementToDisplay(error_billingPostcode, 20);
				if (isElementDisplayed(error_billingPostcode)) {
					testStepInfo("The Error Message for Invalid Billing Post Code was displayed.");
					highLighterMethod(error_billingPostcode);
				} else {
					testStepFailed("The Error Message for Invalid Billing Post Code was not displayed.");
				}
				break;
			}
			case "Invalid Shipping Post Code": {
				waitForElementToDisplay(error_shippingPostcode, 20);
				if (isElementDisplayed(error_shippingPostcode)) {
					testStepInfo("The Error Message for Invalid Shipping Post Code was displayed.");
					highLighterMethod(error_shippingPostcode);
				} else {
					testStepFailed("The Error Message for Invalid Shipping Post Code was not displayed.");
				}
				break;
			}
			case "Invalid Email": {
				if (isElementDisplayed(error_InvalidEmailAddress)) {
					testStepInfo("The Error Message for Invalid Email was displayed.");
					highLighterMethod(error_InvalidEmailAddress);
				} else {
					testStepFailed("The Error Message for Invalid Email was not displayed.");
				}
				break;
			}

			case "Create Account Password": {
				if (isElementDisplayed(error_createAccountPassword)) {
					testStepInfo("The Error Message for Create Account Password was displayed.");
					highLighterMethod(error_createAccountPassword);
				} else {
					testStepFailed("The Error Message for Create Account Password was not displayed.");
				}
				break;
			}

			case "Subscription Product Purchase Success": {
				if (isElementDisplayed(subscriptionStatusMessage)) {
					testStepInfo(
							"The relevant Messages for successful purchase of a subscription for a product was displayed.");
					highLighterMethod(subscriptionStatusMessage);
					waitTime(2);
					scrollToViewElement(orderNumberSubscriptionOrder);
					highLighterMethod(orderNumberSubscriptionOrder);
					return getText(orderNumberSubscriptionOrder);
				} else {
					testStepFailed(
							"The relevant Messages for successful purchase of a subscription for a product was not displayed.");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Relevant Error Meesage verification could not be done");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to click on Ship Different Address.
	 */
	public void clickShipDifferentAddress(boolean enable) {
		try {
			if (isElementDisplayed(shipDifferentAddress_checkBox)) {
				highLighterMethod(shipDifferentAddress_checkBox);
				if (enable == true) {
					if (getAttributeValue(shippinngAddressBlock, "style").contains("none"))
						clickOn(shipDifferentAddress_checkBox);
					else {
						testStepInfo("The Shipping Block was already present");
					}
				} else {
					if (!(getAttributeValue(shippinngAddressBlock, "style").contains("none")))
						clickOn(shipDifferentAddress_checkBox);
					else {
						testStepInfo("The Shipping Block was already absent");
					}
				}
			} else {
				testStepFailed("The Ship Different Address checkbox was not displayed.");
			}
		} catch (Exception e) {
			testStepFailed("Requested function could not be performed on Ship Different address checkbox");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the success message after order is placed.
	 */
	public void validateSavedDetails() {
		try {
			if (isElementDisplayed(successCheckout)) {
				testStepInfo("The Success Message for checkout was displayed.");
				highLighterMethod(successCheckout);
			} else {
				testStepFailed("The Success Message for checkout was not displayed.");
			}
		} catch (Exception e) {
			testStepFailed("The Success Message for checkout could not be validated");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to fill the details required for billing
	 */
	public void checkSavedDetails(HashMap<String, String> billingData_Saved,
			HashMap<String, String> shippingData_Saved) {
		try {
			if (isElementDisplayed(firstName_Billing)) {
				Map<String, String> data_Details_Billing = new LinkedHashMap<String, String>(14);
				data_Details_Billing.put(firstName_Billing, billingData_Saved.get(firstName_Billing));
				data_Details_Billing.put(lastName_Billing, billingData_Saved.get(lastName_Billing));
				data_Details_Billing.put(company_Billing, billingData_Saved.get(company_Billing));
				data_Details_Billing.put(country_Billing, billingData_Saved.get(country_Billing));
				data_Details_Billing.put(streetAddress_Billing, billingData_Saved.get(streetAddress_Billing));
				data_Details_Billing.put(streetAddressNextLine_Billing,
						billingData_Saved.get(streetAddressNextLine_Billing));
				data_Details_Billing.put(postCode_Billing, billingData_Saved.get(postCode_Billing));
				data_Details_Billing.put(city_Billing, billingData_Saved.get(city_Billing));
				data_Details_Billing.put(state_Billing, billingData_Saved.get(state_Billing));
				data_Details_Billing.put(email_Billing, billingData_Saved.get(email_Billing));

				if (CompareData_SavedDetails(data_Details_Billing) == true)
					testStepInfo("The details pre-populated for Billing were same as that saved in the account");
				else
					testStepFailed("The details pre-populated for Billing were not same as that saved in the account");
				testStepInfo("The details pre-populated for Billing were same as that saved in the account");

				if (isElementDisplayed(shipDifferentAddress_checkBox)) {
					highLighterMethod(shipDifferentAddress_checkBox);
					testStepInfo("The ship for different address checkbox was displayed");
					if (GOR.shipDifferentAddress == true) {
						if (getAttributeValue(shippinngAddressBlock, "style").contains("none"))
							clickOn(shipDifferentAddress_checkBox);
						waitTime(2);
						if (!(getAttributeValue(shippinngAddressBlock, "style").contains("none"))) {
							Map<String, String> data_Details_shipping = new LinkedHashMap<String, String>(14);
							data_Details_shipping.put(firstName_Shipping, shippingData_Saved.get(firstName_Shipping));
							data_Details_shipping.put(lastName_Shipping, shippingData_Saved.get(lastName_Shipping));
							data_Details_shipping.put(company_Shipping, shippingData_Saved.get(company_Shipping));
							data_Details_shipping.put(country_Shipping, shippingData_Saved.get(country_Shipping));
							data_Details_shipping.put(streetAddress_Shipping,
									shippingData_Saved.get(streetAddress_Shipping));
							data_Details_shipping.put(streetAddressNextLine_Shipping,
									shippingData_Saved.get(streetAddressNextLine_Shipping));
							data_Details_shipping.put(postCode_Shipping, shippingData_Saved.get(postCode_Shipping));
							data_Details_shipping.put(city_Shipping, shippingData_Saved.get(city_Shipping));
							data_Details_shipping.put(state_Shipping, shippingData_Saved.get(state_Shipping));
							if (CompareData_SavedDetails(data_Details_shipping) == true)
								testStepInfo(
										"The details pre-populated for Shipping were same as that saved in the account");
							else
								testStepFailed(
										"The details pre-populated for Shipping were not same as that saved in the account");
							testStepInfo(
									"The details pre-populated for Shipping were same as that saved in the account");

						} else
							testStepFailed("The Shipping Block was not displayed");
					}
				} else {
					testStepFailed("The ship for different address checkbox was not displayed");
				}

			} else
				testStepFailed("First Name field was not found");
		} catch (Exception e) {
			testStepFailed("Verification of pre-populated data could not be done successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Common Method to compare details for saved and existing billing
	 * and shipping details
	 */
	public boolean CompareData_SavedDetails(Map<String, String> Data) {
		for (Map.Entry<String, String> entry : Data.entrySet()) {
			if (isElementDisplayed(entry.getKey())) {
				if (entry.getValue().isEmpty())
					testStepInfo(entry.getKey().split("#")[0] + "field had empty data saved in the account");
				else {
					if (entry.getKey().contains("Country") || entry.getKey().contains("State")) {
						if (getText(entry.getKey()).contains(entry.getValue())) {
							highLighterMethod(entry.getKey());
							continue;
						} else {
							scrollToViewElement(entry.getKey());
							testStepFailed(entry.getKey().split("#")[0]
									+ " field did not have data same as that saved in the account");
							return false;
						}
					} else {
						if (getAttributeValue(entry.getKey(), "value").contains(entry.getValue())) {
							highLighterMethod(entry.getKey());
							continue;
						} else {
							scrollToViewElement(entry.getKey());
							testStepFailed(entry.getKey().split("#")[0]
									+ " field did not have data same as that saved in the account");
							return false;
						}
					}
				}
			} else {
				testStepFailed(entry.getKey().split("#")[0] + " field was not found");
				return false;
			}
		}
		return true;
	}

	/**
	 * Description: Method to perform login in checkout page
	 */
	public void Login(String username, String password) {
		try {
			if (isElementDisplayed(clickToLogin)) {
				clickOn(clickToLogin);
				waitForElementToDisplay(inputUsername_Login, 10);
				if (isElementDisplayed(inputUsername_Login)) {
					highLighterMethod(inputUsername_Login);
					typeIn(inputUsername_Login, username);
					highLighterMethod(inputPassword_Login);
					typeIn(inputPassword_Login, password);
					highLighterMethod(loginButton);
					clickOn(loginButton);
					GOR.loggedIn = true;
				}
			}

		} catch (Exception e) {
			testStepFailed("Could not be login in the checkout page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check the presence of all the errors for empty fields
	 */
	public void VerifyEmptyFieldErrors() {
		try {
			{
				waitForElementToDisplay(error_BillingFirstNameMissing, 40);
				String[] emptyCardDetailsErrors = { error_BillingFirstNameMissing, error_BillingLastNameMissing,
						error_BillingStreetAddressMissing, error_BillingCityMissing, error_BillingCountryMissing,
						error_BillingStateMissing, error_BillingPostCodeMissing, error_BillingPhoneMissing,
						error_BillingEmailMissing, error_ShippingFirstNameMissing, error_ShippingLastNameMissing,
						error_ShippingStreetAddressMissing, error_ShippingCityMissing, error_ShippingCountryMissing,
						error_ShippingStateMissing, error_ShippingPostCodeMissing, error_termsAndConditionsUnchekced,
						enterAnAddress };
				for (String error : emptyCardDetailsErrors) {
					if (isElementDisplayed(error)) {
						highLighterMethod(error);
					} else {
						testStepFailed(error.split("#")[0] + " error was not displayed");
					}
				}
			}

		} catch (Exception e) {
			testStepFailed("Validation of empty field errors was not successful");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check the presence of the errors for empty card
	 * details
	 */
	public void VerifyEmptyCardDetailsErrors() {
		try {
			String[] emptyCardDetailsErrors = { error_CardSecurityCodeMissing, error_CardNumberMissing,
					error_expirationDataInvalid };
			for (String error : emptyCardDetailsErrors) {
				if (isElementDisplayed(error)) {
					highLighterMethod(error);
				} else {
					testStepFailed(error.split("#")[0] + " error for empty field was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Validation of errors for empty card details could not be done");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to type the numbers from keyboard according the the input
	 * passed
	 */
	public void typeNumbersFromKeyboard(String element, String numberData) {
		try {
			char input[] = numberData.toCharArray();
			for (char number : input) {
				switch (Integer.parseInt(String.valueOf(number))) {
				case 1:
					findWebElement(element).sendKeys(Keys.NUMPAD1);
					break;
				case 2:
					findWebElement(element).sendKeys(Keys.NUMPAD2);
					break;
				case 3:
					findWebElement(element).sendKeys(Keys.NUMPAD3);
					break;
				case 4:
					findWebElement(element).sendKeys(Keys.NUMPAD4);
					break;
				case 5:
					findWebElement(element).sendKeys(Keys.NUMPAD5);
					break;
				case 6:
					findWebElement(element).sendKeys(Keys.NUMPAD6);
					break;
				case 7:
					findWebElement(element).sendKeys(Keys.NUMPAD7);
					break;
				case 8:
					findWebElement(element).sendKeys(Keys.NUMPAD8);
					break;
				case 9:
					findWebElement(element).sendKeys(Keys.NUMPAD9);
					break;
				case 0:
					findWebElement(element).sendKeys(Keys.NUMPAD0);
					break;
				}

			}
		} catch (Exception e) {
			testStepFailed("Could not type numbers from Keyboard successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check whether sales tax is displayed for the
	 * subscription product.
	 */
	public void verifySalesTax_SubscriptionProduct() {
		try {
			waitForElement(salesTax);
			scrollToViewElement(salesTax);
			if (isElementDisplayed(salesTax)) {
				testStepInfo("The sales tax for the product was displayed in the checkout page.");
				highLighterMethod(salesTax);
			} else {
				testStepFailed("The sales tax for the product was not displayed in the checkout page.");
			}

			if (isElementDisplayed(salesTax_RecurringTotal)) {
				testStepInfo("The sales tax was displayed under recurring totals in the checkout page");
				highLighterMethod(salesTax_RecurringTotal);
			} else {
				testStepFailed("The sales tax under recurring totals not displayed in the checkout page.");
			}
		} catch (Exception e) {
			testStepFailed(
					"Could not verify whether sales tax for the subscription product is displayed in the checkout page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check whether the recurring totals section is
	 * displayed.
	 */
	public void verifyRecurringTotals() {
		try {
			waitForElement(heading_RecurringTotal);
			scrollToViewElement(heading_RecurringTotal);
			if (isElementDisplayed(heading_RecurringTotal) && isElementDisplayed(subtotal_RecurringTotal)
					&& isElementDisplayed(shipping_RecurringTotal) && isElementDisplayed(salesTax_RecurringTotal)
					&& isElementDisplayed(total_RecurringTotal)) {
				testStepInfo("The Recurring totals section was displayed.");
				highLighterMethod(heading_RecurringTotal);
				highLighterMethod(subtotal_RecurringTotal);
				highLighterMethod(shipping_RecurringTotal);
				highLighterMethod(salesTax_RecurringTotal);
				highLighterMethod(total_RecurringTotal);
			} else {
				testStepFailed("The Recurring totals section was not displayed.");
			}
		} catch (Exception e) {
			testStepFailed("Could not verify whether Recurring totals section was displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify whether create account password field is
	 * displayed and clear the field.
	 */
	public void verifyCreateAccountPasswordAndClearField() {
		try {
			if (isElementDisplayed(createAccountPassword)) {
				scrollToViewElement(createAccountPassword);
				testStepInfo("The Create Account Password field was displayed.");
				highLighterMethod(createAccountPassword);
				clearEditBox(createAccountPassword);
			} else {
				testStepFailed("The Create Account Password field was not displayed.");
			}
		} catch (Exception e) {
			testStepFailed("The Create Account Password field could not be cleared");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Use New Card under Credit Card payment.
	 */
	public void clickOnUseNewCardIfPresent() {
		try {
			if (GOR.loggedIn == true && GOR.completeCheckoutWithSavedCard == true) {
				if (findWebElements(newCreditCard_radioButton).size() > 0) {
					scrollToViewElement(newCreditCard_radioButton);
					if (isElementDisplayed(newCreditCard_radioButton)) {
						highLighterMethod(newCreditCard_radioButton);
						clickOn(newCreditCard_radioButton);
					}
				}
			} else {
				testStepInfo("Use New Card was not clicked");
			}
		} catch (Exception e) {
			testStepFailed("Details could not be filled successfully for credit card");
			e.printStackTrace();
		}
	}
}
